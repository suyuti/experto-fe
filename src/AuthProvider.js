// bilot.group/articles/authentication-with-azure-ad-using-msal-react-app

import React, { Component } from 'react';
import {
  msalApp,
  requiresInteraction,
  GRAPH_REQUESTS
} from './utils/authUtils';
import { config } from './services/graph.config';
import axiosService from './services/axios.service';
var graph = require('@microsoft/microsoft-graph-client');


var client = null;

const getAuthenticatedClient = accessToken => {
  client = graph.Client.init({
    authProvider: done => {
      done(null, accessToken);
    }
  });
  return client;
};



export default AuthComp =>
  class AuthProvider extends Component {
    that = this;
    constructor(props) {
      super(props);
      this.state = {
        account: null,
        error: null
      };
      this.expertoLogin = this.expertoLogin.bind(this);
    }

    async acquireToken(request, redirect) {
      return msalApp.acquireTokenSilent(request).catch(error => {
        if (requiresInteraction(error.errorCode)) {
          return redirect
            ? msalApp.acquireTokenRedirect(request)
            : msalApp.acquireTokenPopup(request);
        }
      });
    }

    async onSignIn() {
      return msalApp.loginRedirect(GRAPH_REQUESTS.LOGIN);
    }

    onSignOut() {
      msalApp.logout();
    }

    expertoLogin = (me) => {
      //debugger;
      axiosService.get(`/auth/signin`, {
        params: { uniqueId: me.id }
      }).then(resp => {
          //debugger;
          console.log(resp);
        });
    };

    async componentDidMount() {
      /*      msalApp.handleRedirectCallback((error, response) => {
          alert('de')
      });
      msalApp.handleRedirectCallback(error => {
        if (error) {
          const errorMessage = error.errorMessage
            ? error.errorMessage
            : 'unable to acquire access token';
          this.setState({
            error: errorMessage
          });
        }
      });
*/
      //msalApp.loginRedirect({scopes: config.scopes})

      msalApp
        .acquireTokenSilent({ scopes: config.scopes })
        .then(accessTokenResponse => {
          //debugger;
          let accessToken = accessTokenResponse.accessToken;
            getAuthenticatedClient(accessTokenResponse.accessToken)
            client.api(`/me`).get().then(me => 
                {
                    //debugger
                    this.expertoLogin(me)
                }
            )



          let account = msalApp.getAccount();
          //debugger
          if (account) {
            this.setState({
              account
            });
          } else {
            this.onSignIn();
          }
          ////debugger;
          //this.expertoLogin();
          //console.log(account);
        })
        .catch(error => {
          //debugger;
          if (error.errorCode.indexOf('user_login_error') !== -1) {
            msalApp
              .loginPopup({ scopes: config.scopes })
              .then(function(accessTokenResponse) {
                let account = msalApp.getAccount();
                if (account) {
                  this.setState({
                    account
                  });
                } else {
                  this.onSignIn();
                }
                // Acquire token interactive success
                //debugger;
              })
              .catch(function(error) {
                // Acquire token interactive failure
                //debugger;
                console.log(error);
              });
          }
          console.log(error);
        });
    }

    render() {
      const { account, error } = this.state;
      console.log(account);
      return (
        <AuthComp
          {...this.props}
          accountInfo={account}
          error={error}
          onSignIn={() => this.onSignIn()}
          onSignOut={() => this.onSignOut()}
        />
      );
    }
  };
