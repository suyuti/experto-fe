import BarChartIcon from '@material-ui/icons/BarChart';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ChatIcon from '@material-ui/icons/ChatOutlined';
import CodeIcon from '@material-ui/icons/Code';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import FolderIcon from '@material-ui/icons/FolderOutlined';
import DashboardTwoToneIcon from '@material-ui/icons/DashboardTwoTone';
import GradeTwoTone from '@material-ui/icons/GradeTwoTone';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LockOpenIcon from '@material-ui/icons/LockOpenOutlined';
import MailIcon from '@material-ui/icons/MailOutlined';
import PresentToAllIcon from '@material-ui/icons/PresentToAll';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import PersonIcon from '@material-ui/icons/PersonOutlined';
import ReceiptIcon from '@material-ui/icons/ReceiptOutlined';
import SettingsIcon from '@material-ui/icons/SettingsOutlined';
import ViewModuleIcon from '@material-ui/icons/ViewModule';

var iconsMap = {
  BarChartIcon: BarChartIcon,
  CalendarTodayIcon: CalendarTodayIcon,
  ChatIcon: ChatIcon,
  CodeIcon: CodeIcon,
  DashboardIcon: DashboardIcon,
  ErrorIcon: ErrorIcon,
  FolderIcon: FolderIcon,
  DashboardTwoToneIcon: DashboardTwoToneIcon,
  GradeTwoTone: GradeTwoTone,
  ListAltIcon: ListAltIcon,
  LockOpenIcon: LockOpenIcon,
  MailIcon: MailIcon,
  PresentToAllIcon: PresentToAllIcon,
  PeopleIcon: PeopleIcon,
  PersonIcon: PersonIcon,
  ReceiptIcon: ReceiptIcon,
  SettingsIcon: SettingsIcon,
  ViewModuleIcon: ViewModuleIcon
};

export const getNavItems = (yetkiler, yoneticisiOldugumDepartmanlar) => {
  if (!yetkiler) {
    return [];
  }

  var menu = [
    {
      label: 'Ana Ekran',
      icon: 'DashboardTwoToneIcon',
      content: [
        {
          label: 'Ana Ekran',
          description: 'Satis Masasi',
          to: '/dashboardsatis'
        },
      ]
    }
  ];

  var firmalar = {
    label: 'Firmalar',
    icon: 'PeopleIcon',
    content: []
  };

  if (yetkiler.aktifmusteri && yetkiler.aktifmusteri.includes('list')) {
    firmalar.content.push({
      label: 'Aktif Müşteriller',
      description: '',
      to: '/musteriler?durum=aktif'
    });
  }
  if (yetkiler.pasifmusteri && yetkiler.pasifmusteri.includes('list')) {
    firmalar.content.push({
      label: 'Pasif Müşteriller',
      description: '',
      to: '/musteriler?durum=pasif'
    });
  }
  if (
    yetkiler.potansiyelmusteri &&
    yetkiler.potansiyelmusteri.includes('list')
  ) {
    firmalar.content.push({
      label: 'Potansiyel Müşteriller',
      description: '',
      to: '/musteriler?durum=potansiyel'
    });
  }
  if (yetkiler.sorunlumusteri && yetkiler.sorunlumusteri.includes('list')) {
    firmalar.content.push({
      label: 'Sorunlu Müşteriller',
      description: '',
      to: '/musteriler?durum=sorunlu'
    });
  }

  if (yetkiler.partnerler && yetkiler.partnerler.includes('list')) {
    firmalar.content.push({
      label: 'Partneler',
      description: '',
      to: '/partnerler'
    });
  }

  if (yetkiler.tedarikciler && yetkiler.tedarikciler.includes('list')) {
    firmalar.content.push({
      label: 'Tedarikçiler',
      description: '',
      to: '/tedarikciler'
    });
  }

  menu.push(firmalar);

  if (yetkiler.urun && yetkiler.urun.includes('list')) {
    menu.push({
      label: 'Ürünler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Ürünler',
          description: '',
          to: '/urunler'
        }
      ]
    });
  }
  if (yetkiler.kisi && yetkiler.kisi.includes('list')) {
    menu.push({
      label: 'Kişiler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Kontak Kişiler',
          description: '',
          to: '/kisiler'
        }
      ]
    });
  }
  if (yetkiler.personel && yetkiler.personel.includes('list')) {
    menu.push({
      label: 'Personel',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Personel',
          description: '',
          to: '/personeller'
        }
      ]
    });
  }

  if (yetkiler.gorev && yetkiler.gorev.includes('list')) {
    var gorevler = {
      label: 'Görevler',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Açık Görevlerim',
          description: '',
          to: '/gorevler?t=acik'
        },
        {
          label: 'Başarılı Görevlerim',
          description: '',
          to: '/gorevler?t=basarili'
        },
        {
          label: 'Başarısız Görevlerim',
          description: '',
          to: '/gorevler?t=basarisiz'
        },
        {
          label: 'Oluşturduğum Görevler',
          description: '',
          to: '/gorevler?t=olusturdugum'
        }
      ]
    };

    if (yoneticisiOldugumDepartmanlar) {
      gorevler.content.push({
        label: 'Ekibimin açık görevleri',
        description: '',
        to: '/gorevler?t=ekipacik'
      });
      gorevler.content.push({
        label: 'Ekibimin başarılı görevleri',
        description: '',
        to: '/gorevler?t=ekipbasarili'
      });
      gorevler.content.push({
        label: 'Ekibimin geciken görevleri',
        description: '',
        to: '/gorevler?t=ekipgeciken'
      });
    }
    // TODO
    menu.push(gorevler);
  }
  if (yetkiler.toplanti && yetkiler.toplanti.includes('list')) {
    // TODO
    var toplantilar = {
      label: 'Toplantılar',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Gelecek Toplantılar',
          description: '',
          to: '/toplantilar?s=f'
        },
        {
          label: 'Geçmiş Toplantılar',
          description: '',
          to: '/toplantilar?s=p'
        }
      ]
    }

    if (yoneticisiOldugumDepartmanlar) {
      toplantilar.content.push({
        label: 'Ekibimin Gelecek Toplantıları',
        description: '',
        to: '/toplantilar?t=ekipgelecek'
      });
      toplantilar.content.push({
        label: 'Ekibimin Geçmiş Toplantıları',
        description: '',
        to: '/toplantilar?t=ekipgecmis'
      });
    }

    menu.push(toplantilar)

  }
  if (yetkiler.gorusme && yetkiler.gorusme.includes('list')) {
    // TODO
    var gorusme = {
      label: 'Müşteri Görüşmeleri',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Görüşmelerim',
          description: '',
          to: '/gorusmeler?t=me'
        }
      ]
    }
    if (yoneticisiOldugumDepartmanlar) {
      gorusme.content.push({
        label: 'Ekibimin Görüşmeleri',
        description: '',
        to: '/gorusmeler?t=ekip'
      })
    }
    menu.push(gorusme);
  }

  if (yetkiler.admin && yetkiler.admin.includes('list')) {
    // TODO Admin yetkisi
    menu.push({
      label: 'Yönetim',
      icon: 'PeopleIcon',
      content: [
        {
          label: 'Departmanlar',
          description: '',
          to: '/departmanlar'
        },
        {
          label: 'Yetkilendirme',
          description: '',
          to: '/yetki'
        }
      ]
    });
  }

  var navItems = [
    {
      label: 'İşlem Menusu',
      content: menu
    }
  ];
  return navItems;
};

export default [
  {
    label: 'Navigation menu',
    content: JSON.parse(
      `[
  {
    "label": "Ana Ekran",
    "icon": "DashboardTwoToneIcon",
    "content": [
      {
        "label": "Satış",
        "description": "Satış masası",
        "to": "/dashboardsatis"
      },
      {
        "label": "Teknik",
        "description": "This is a dashboard page example built using this template.",
        "to": "/DashboardDefault"
      },
      {
        "label": "Mali",
        "description": "This is a dashboard page example built using this template.",
        "to": "/DashboardDefault"
      }
    ]
  },
  {
    "label":"Müşteriler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Aktif Müşteriller",
        "description": "",
        "to": "/musteriler?durum=aktif"
      },
      {
        "label": "Pasif Müşteriller",
        "description": "",
        "to": "/musteriler?durum=pasif"
      },
      {
        "label": "Potansiyel Müşteriller",
        "description": "",
        "to": "/musteriler?durum=potansiyel"
      },
      {
        "label": "Sorunlu Müşteriller",
        "description": "",
        "to": "/musteriler?durum=sorunlu"
      }
    ]
  },
  {
    "label":"Ürünler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Ürünler",
        "description": "",
        "to": "/urunler"
      }
    ]
  },
  {
    "label":"Kontak Kişiler",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Kontak Kişiler",
        "description": "",
        "to": "/kisiler"
      }
    ]
  },
  {
    "label":"Mail",
    "icon":"PeopleIcon",
    "content": [
      {
        "label": "Mail",
        "description": "",
        "to": "/toplantilar"
      }
    ]
  }
]`,
      (key, value) => {
        if (key === 'icon') {
          return iconsMap[value];
        } else {
          return value;
        }
      }
    )
  }
];
