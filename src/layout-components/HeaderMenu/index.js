import React, { Fragment } from 'react';

import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  Grid,
  Box,
  Typography,
  Popover,
  Menu,
  Button,
  List,
  ListItem,
  Divider,
  ButtonBase
} from '@material-ui/core';

import { Settings, Briefcase, Users, Layers } from 'react-feather';

const HeaderMenu = props => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'mega-menu-popover' : undefined;

  const [anchorElMenu, setAnchorElMenu] = React.useState(null);

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorElMenu(null);
  };

  return (
    <Fragment>
      <div className="app-header-menu">
        <Button
          onClick={handleClickMenu}
          color="inherit"
          size="medium"
          className="btn-inverse font-size-xs mr-3">
          Eylemler
        </Button>
        <Menu
          anchorEl={anchorElMenu}
          keepMounted
          open={Boolean(anchorElMenu)}
          onClose={handleCloseMenu}
          classes={{ list: 'p-0' }}
          getContentAnchorEl={null}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}>
          <Box className="overflow-hidden border-0 bg-skim-blue p-3 dropdown-mega-menu-md">
            <div className="text-center">
              <div className="font-weight-bold font-size-lg mb-1 text-white">
                İşlemler
              </div>
            </div>
            <div className="d-flex flex-wrap">
              <div className="w-50 p-2">
                <ButtonBase
                  onClick={() => {
                    handleCloseMenu();
                    props.history.push(`/gorev/yeni`);
                  }}
                  className="btn card card-box text-left d-flex bg-white rounded justify-content-center px-3 py-2 w-100 border-0">
                  <div>
                    <Briefcase className="h1 d-block my-2 text-success" />
                    <div className="font-weight-bold font-size-lg text-black">
                      Görev
                    </div>
                    <div className="font-size-sm mb-1 text-black-50">
                      Yeni görev oluştur.
                    </div>
                  </div>
                </ButtonBase>
              </div>
              <div className="w-50 p-2">
                <ButtonBase
                  onClick={() => {
                    handleCloseMenu();
                    props.history.push(`/toplanti/yeni`);
                  }}
                  className="btn card card-box bg-white rounded text-left d-flex justify-content-center px-3 py-2 w-100 border-0">
                  <div>
                    <Users className="h1 d-block my-2 text-danger" />
                    <div className="font-weight-bold font-size-lg text-black">
                      Toplantı
                    </div>
                    <div className="font-size-sm mb-1 text-black-50">
                      Yeni toplantı oluştur
                    </div>
                  </div>
                </ButtonBase>
              </div>
              <div className="w-50 p-2">
                <ButtonBase
                  onClick={() => {
                    handleCloseMenu();
                    props.history.push(`/gorusme/yeni`);
                  }}
                  className="btn card card-box text-left d-flex bg-white rounded justify-content-center px-3 py-2 w-100 border-0">
                  <div>
                    <Settings className="h1 d-block my-2 text-warning" />
                    <div className="font-weight-bold font-size-lg text-black">
                      Görüşme Notu
                    </div>
                    <div className="font-size-sm mb-1 text-black-50">
                      Yeni görüşme notu oluştur
                    </div>
                  </div>
                </ButtonBase>
              </div>
            </div>
          </Box>
        </Menu>
      </div>
    </Fragment>
  );
};

export default withRouter(HeaderMenu);
