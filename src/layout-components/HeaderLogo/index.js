import React, { Fragment } from 'react';

import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { IconButton, Box, makeStyles, Avatar } from '@material-ui/core';

import { connect } from 'react-redux';

import projectLogo from '../../assets/images/react.svg';

const useStyles = makeStyles(theme => ({
  logo: {
    fontSize: '1.8rem'
  }
}))

const HeaderLogo = props => {
  const { sidebarToggle, sidebarHover } = props;
  const classes = useStyles()
  return (
    <Fragment>
      <div
        className={clsx('app-header-logo', {
          'app-header-logo-close': sidebarToggle,
          'app-header-logo-open': sidebarHover
        })}>
        <Box
          className="header-logo-wrapper"
          title="Experto">
          <Box className={clsx( classes.logo, "__header-logo-text text-white")} component={Link} to='/dashboardsatis' >Experto</Box>
        </Box>
      </div>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  sidebarToggle: state.ThemeOptions.sidebarToggle,
  sidebarHover: state.ThemeOptions.sidebarHover
});

export default connect(mapStateToProps)(HeaderLogo);
