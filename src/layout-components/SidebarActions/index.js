import React, { Fragment } from "react"
import { Button, Box} from "@material-ui/core"
import { withRouter } from "react-router"
import { useDispatch } from "react-redux"
import * as Actions from '../../store/actions'

const SidebarActions = props => {
    const dispatch = useDispatch()

    return (
        <Fragment>
            <Box>
                <div className="p-2">
                    <Button 
                        variant='contained' 
                        color="primary" 
                        fullWidth
                        onClick={() => 
                            props.history.push('/gorev/yeni')
                            //</div>dispatch(Actions.showGorevDialog())
                        }
                    >Yeni Görev Oluştur</Button>
                </div>
            </Box>
            <Box>
                <div className="p-2">
                    <Button 
                        variant='contained' 
                        color="primary" 
                        fullWidth
                        onClick={() => 
                            props.history.push('/toplanti/yeni')
                            //</div>dispatch(Actions.showGorevDialog())
                        }
                    >Yeni Toplantı Oluştur</Button>
                </div>
            </Box>
        </Fragment>
    )
}

export default withRouter(SidebarActions)