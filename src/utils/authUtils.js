import {UserAgentApplication} from 'msal'
import {config} from '../services/graph.config'

const hostname = window && window.location && window.location.hostname
let clientId = config.appId
let scopes = config.scopes

/*if (hostname==='YOUR QA') {
    clientId= 'XXXX'
    scopes='XXXX'
}
else {

}
*/
export const GRAPH_REQUESTS = {
    LOGIN: {
        scopes: config.scopes
    }
}

export const msalApp = new UserAgentApplication({
    auth: {
        clientId: config.appId,
        //authority: 'https://login.microsoftonline.com/common',
        //validateAuthority: true,
        redirectUri: 'https://localhost:3000/',
        //navigateToLoginRequestUrl: false
        postLogoutRedirectUri: 'https://localhost:3000/welcome'
    },
    cache: {
        cacheLocation: 'sessionStorage'
    }
})

export const requiresInteraction = () => {

}