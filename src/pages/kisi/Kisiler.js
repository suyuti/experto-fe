import React, { Fragment, useEffect, useState } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, LinearProgress } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { withRouter } from 'react-router';

const Kisiler = props => {
  const dispatch = useDispatch();
  const _kisiler = useSelector(state => state.kisi.kisiler);
  const yetkiler = useSelector(state => state.experto.yetkiler);
  const [enableEdit, setEnableEdit] = useState(true);
  const [kisiler, setKisiler] = useState(null)
  const [newLoad, setNewLoad] = useState(true)
  /*
    Kisiler listesi daha önceden doldurulmuş ise problem oluyor.
    Aslında getKisiler() reducerdaki kişiler listesini temizliyor.
    Sayfa sorunsuz olarak çalışmalı.
    Ama temizlemeden sonra sorun olduğu gözlendi.

    _kisiler : reducerdaki liste
    load() da getKisiler ile alınıyor.

    _kisiler de değişiklik olmuşsa kisiler statei ne alınıyor. Tablo onu gösteriyor.

    Burada sayfa yüklendiğinde reducerdaki dolu olan liste etkilemesin diye bir bayrak konuldu. newLoad
    Sayfa yeni yüklendiğinde newLoad True. getKisiler() çağırımının hemen ardından False yapılıyor.
    Daha sonra liste geldiyse ve newLoad False ise kisiler listesi state'e alınıyor.
    
    Daha iyi bir çözümü vardır mutlaka ama acilen böyle çözdüm. 
    suyuti / 18.09.2020
  */

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getKisiler());

      if (yetkiler.kisi.includes('create')) {
        dispatch(Actions.enableKaydetBtn());
      } else {
        dispatch(Actions.disableKaydetBtn());
      }
      setEnableEdit(yetkiler.kisi.includes('view'));

      setNewLoad(false)
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (_kisiler && !newLoad) {
      setKisiler(_kisiler)
    }
  }, [_kisiler])
//  debugger

  return (
    <Fragment>
      <PageTitle
        titleHeading="Kontak Kişiler"
        titleDescription="..."
        buttonTitle="Yeni"
        onSave={() => {
          props.history.push('/kisi/yeni');
        }}
      />
      {!kisiler && <LinearProgress />}

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MaterialTable
            title="Kontak Kişiler"
            data={kisiler || []}
            columns={[
              {
                title: 'Adı',
                field: 'adi'
                //render: rowData => `${rowData.adi} ${rowData.soyadi}`
              },
              {
                title: 'Soyadı',
                field: 'soyadi'
                //render: rowData => `${rowData.adi} ${rowData.soyadi}`
              },
              { title: 'Ünvanı', field: 'unvani' },
              { title: 'Telefon', field: 'cepTelefon' },
              { title: 'Mail', field: 'mail' },
              { title: 'Firma', field: 'musteri.marka' },
              {
                title: 'Durum',
                render: rowData => {
                  if (rowData.aktif) {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-info">
                        Aktif
                      </div>
                    );
                  } else {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-warning">
                        Pasif
                      </div>
                    );
                  }
                }
              }
            ]}
            actions={[
              {
                icon: 'edit',
                tooltip: 'Detail',
                onClick: (event, rowData) =>
                  props.history.push(`/kisi/${rowData._id}`),
                disabled: !enableEdit
              }
            ]}
            options={{
              grouping: true,
              actionsColumnIndex: -1,
              pageSize: 20
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(Kisiler);
