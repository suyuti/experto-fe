import React, { Fragment, useState, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, Tabs, Tab, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import KisiInfo from 'components/kisi/KisiInfo';
import KisiNot from 'components/kisi/KisiNot';
import KisiGorusmeler from 'components/kisi/KisiGorusmeler';
import { withRouter } from 'react-router';
import { validate } from 'components/kisi/KisiValidator';
import AttachPanel from 'components/attachment/AttachPanel';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}));

const Kisi = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const kisi = useSelector(state => state.kisi.kisi);
  const yetkiler = useSelector(state => state.experto.yetkiler);

  useEffect(() => {
    const load = async () => {
      debugger
      if (props.match.params.id === 'yeni') {
        dispatch(Actions.yeniKisi());
      } else {
        dispatch(Actions.getKisi(props.match.params.id));
      }

      if (yetkiler.kisi.includes('edit')) {
        dispatch(Actions.enableKaydetBtn());
      } else {
        dispatch(Actions.disableKaydetBtn());
      }
    };
    load();
  }, [dispatch]);

  if (!kisi) {
    return <></>;
  }

  const save = () => {
    const err = validate(kisi);
    if (err) {
      dispatch(Actions.setError(err));
    } else {
      dispatch(Actions.saveKisi(kisi));
      dispatch(Actions.resetError());
      //props.history.push('/kisiler');
    }
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Kontak Kişi"
        titleDescription="..."
        buttonTitle="Kaydet"
        onSave={() => {
          save();
        }}
      />
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <KisiInfo />
        </Grid>
        {!kisi.yeni && (
          <Fragment>
            <Grid item xs={6}>
              <KisiGorusmeler />
            </Grid>
            <Grid item xs={6}>
              <KisiNot />
            </Grid>
            <Grid item xs={6}>
            <AttachPanel
              files={kisi.attachments || []}
              onFileUploaded={fileId => {
                var k = {...kisi}
                k.attachments.push(fileId)
                dispatch(Actions.setKisi(k))
              }}
            />
            </Grid>
          </Fragment>
        )}
      </Grid>
    </Fragment>
  );
};

export default withRouter(Kisi);
