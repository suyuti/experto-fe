import React, { Fragment, useEffect, useState } from 'react';
import GorevList from 'components/gorev/GorevList';
import { PageTitle } from 'layout-components';
import GorevlerToolbar from 'components/gorev/GorevlerToolbar';
import GorevlerChart from 'components/gorev/GorevlerChart';
import { Tabs, Tab } from '@material-ui/core';
import TabPanel from 'components/TabPanel'

const qs = require('query-string');

const Gorevler = props => {
  const [subtitle, setSubtitle] = useState('');
  const [filter, setFilter] = useState(null);
  const [count, setCount] = useState(0);
  const [selectedTab, setSelectedTab] = useState(0);

  useEffect(() => {
    const gorevType = qs.parse(props.location.search);
    if (gorevType.t === 'acik') {
      setSubtitle('Açık görevlerim');
    } else if (gorevType.t === 'basarili') {
      setSubtitle('Başarılı tamamlanmış görevlerim');
    } else if (gorevType.t === 'basarisiz') {
      setSubtitle('Başarısız tamamlanmış görevlerim');
    } else if (gorevType.t === 'olusturdugum') {
      setSubtitle('Benim oluşturduğum görevler');
    } else if (gorevType.t === 'geciken') {
      setSubtitle('Geciken görevlerim');
    } else if (gorevType.t === 'ekipacik') {
      setSubtitle('Ekibimin açık görevleri');
    } else if (gorevType.t === 'ekipgeciken') {
      setSubtitle('Ekibimin geciken görevleri');
    } else if (gorevType.t === 'ekipbasarili') {
      setSubtitle('Ekibimin başarılı görevleri');
    }
  }, [props.location.search]);

  return (
    <Fragment>
      <PageTitle
        titleHeading="Görevler"
        titleDescription={subtitle}
        buttonTitle="Yeni"
        onSave={() => {
          props.history.push('/gorev/yeni');
        }}
      />
      <GorevlerToolbar
        onFilterChange={_filter => {
          setFilter(_filter);
        }}
        count={count}
      />
      <Tabs
        value={selectedTab}
        onChange={(e, n) => setSelectedTab(n)}
        indicatorColor="primary"
        textColor="primary"
        variant="fullWidth"
        aria-label="simple tabs example">
        <Tab label="Liste" />
        <Tab label="Grafik" />
      </Tabs>
      <TabPanel value={selectedTab} index={0}>
        <GorevList filter={filter} {...props} setCount={setCount} />
      </TabPanel>
      <TabPanel value={selectedTab} index={1}>
      </TabPanel>
    </Fragment>
  );
};

export default Gorevler;
