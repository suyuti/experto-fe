import React, { Fragment, useEffect, useState } from 'react';
import GorevPanel from '../../components/gorev/GorevPanel';
import PageTitle from '../../layout-components/PageTitle';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { withRouter } from 'react-router';
import Toolbar from 'components/Toolbar';
import { validate } from 'components/gorev/GorevValidator';

const Gorev = props => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.app.error);
  const gorev = useSelector(state => state.experto.gorev);
  const me = useSelector(state => state.graph.me);

  useEffect(() => {
    if (props.match.params.id === 'yeni') {
      dispatch(Actions.yeniGorev(me));
    } else {
      dispatch(Actions.getGorev(props.match.params.id));
      dispatch(Actions.getHistory(props.match.params.id));
    }
  }, [dispatch]);

  if (props.match.params.id === 'yeni') {
    if (!gorev) {
      return <></>;
    }
    if (!gorev.yeni) {
      return <></>;
    }
  }

  const save = () => {
    const err = validate(gorev);
    if (err) {
      dispatch(Actions.setError(err));
    } else {
      dispatch(Actions.saveGorev(gorev));
      dispatch(Actions.resetError());
      props.history.goBack();
    }
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Görev"
        buttonTitle="Kaydet"
        //titleDescription={gorev && gorev.kayit ? 'kaydedildi' : 'kaydedilmedi'}
        onSave={() => {
          if (gorev.sonuc && gorev.sonuc === 'acik') {
            save();
          }
          //dispatch(Actions.resetError());
          //dispatch(Actions.saveGorev(gorev));
          //props.history.goBack();
        }}
      />
      <GorevPanel />
    </Fragment>
  );
};

export default withRouter(Gorev);
