import React, { Fragment, useState, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import {
  Typography,
  Checkbox,
  Table,
  TableBody,
  TableRow,
  TableCell,
  MenuItem,
  TableHead,
  Button,
  TextField
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from 'store/actions';

const aksiyonlar = [
  {
    title: 'Aktif Müşteriler',
    field: 'aktifmusteri',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Pasif Müşteriler',
    field: 'pasifmusteri',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Potansiyel Müşteriler',
    field: 'potansiyelmusteri',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Sorunlu Müşteriler',
    field: 'sorunlumusteri',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Partnerler',
    field: 'partnerler',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Tedarilçiler',
    field: 'tedarikciler',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Kontak Kişiler',
    field: 'kisi',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Personeller',
    field: 'personel',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Görüşmeler',
    field: 'gorusme',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Ürünler',
    field: 'urun',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Satış Fırsatları',
    field: 'satisfirsati',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Randevular',
    field: 'randevu',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Yetkiler',
    field: 'yetki',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Teklifler',
    field: 'teklif',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Sözleşmeler',
    field: 'sozlesme',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Müşteri Kaynağı',
    field: 'musterikaynak',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Satış Performans İzleme',
    field: 'satisperformans',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Görev',
    field: 'gorev',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Not ve yorum',
    field: 'not',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Toplantı',
    field: 'toplanti',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Departmanlar',
    field: 'departman',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  },
  {
    title: 'Admin',
    field: 'admin',
    list: false,
    create: false,
    view: false,
    edit: false,
    delete: false
  }
];

const roller = [
  { rolAdi: 'admin', key: 'admin' },
  { rolAdi: 'Yönetici', key: 'Yönetici' },
  { rolAdi: 'Satış Yöneticisi', key: 'Satış Yöneticisi' },
  { rolAdi: 'Satış Personel', key: 'Satış Personel' },
  { rolAdi: 'Müşteri', key: 'Müşteri' },
  { rolAdi: 'Ziyaretçi', key: 'Ziyaretçi' },
  { rolAdi: 'Proje Yönetim Grup Md.', key: 'Proje Yönetim Grup Md.' },
  { rolAdi: 'Proje Yönetim Uzm.', key: 'Proje Yönetim Uzm.' },
  { rolAdi: 'Proje Yönetim Uzm. Yrd.', key: 'Proje Yönetim Uzm. Yrd.' },
  { rolAdi: 'ArGe Muhasebesi Direktörü', key: 'ArGe Muhasebesi Direktörü' },
  { rolAdi: 'ArGe Muhasebesi Uzm.', key: 'ArGe Muhasebesi Uzm.' },
  { rolAdi: 'Teknik Grup Md. (Metatek)', key: 'Teknik Grup Md. (Metatek)' },
  { rolAdi: 'Teknik Uzm. (Metatek)', key: 'Teknik Uzm. (Metatek)' },
  { rolAdi: 'Teknik Grup Md. (MakiTek)', key: 'Teknik Grup Md. (MakiTek)' },
  { rolAdi: 'Teknik Uzm. (Makitek)', key: 'Teknik Uzm. (Makitek)' },
  { rolAdi: 'IK ve İdari İşler Md.', key: 'IK ve İdari İşler Md.' },
  { rolAdi: 'Muhasebe Uzm.', key: 'Muhasebe Uzm.' },
  { rolAdi: 'Asistan', key: 'Asistan' },
  { rolAdi: 'İdari İşler', key: 'İdari İşler' },
  { rolAdi: 'Kurumsal İletişim Direktörü', key: 'Kurumsal İletişim Direktörü' },
  { rolAdi: 'SGK Operasyon Uzm.', key: 'SGK Operasyon Uzm.' },
  { rolAdi: 'Genel Müdür Yard.', key: 'Genel Müdür Yard.' },
  { rolAdi: 'CEO', key: 'CEO' },
  { rolAdi: 'Yönetim Kurulu Başkanı', key: 'Yönetim Kurulu Başkanı' }
];

const Yetki = props => {
  const dispatch = useDispatch();
  const departmanlar = useSelector(state => state.departman.departmanlar);
  const yetkiler = useSelector(state => state.yetki.yetkiler);
  const [selectedDep, setSelectedDep] = useState(null);

  const { yetkilerData, token } = props;
  const [tableData, setTableData] = useState(aksiyonlar);
  const [selectedRole, setSelectedRole] = useState(null);
  const [selectedRoleAdi, setSelectedRoleAdi] = useState('');
  const [yeni, setYeni] = useState(false);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getDepartmanlarAsList());
      dispatch(Actions.getYetkiler());
      dispatch(Actions.enableKaydetBtn())
    };
    load();
    clear();
  }, [dispatch]);

  useEffect(() => {
    if (departmanlar && yetkiler) {
      //clear()
    }
  }, [departmanlar, yetkiler]);

  const clear = () => {
    var acts = [...aksiyonlar];
    acts.map(a => {
      a.list = false;
      a.create = false;
      a.view = false;
      a.edit = false;
      a.delete = false;
    });
    setTableData(acts);
  };

  const handleRolChanged = e => {
    debugger;
    clear();
    setSelectedDep(e.target.value);
    let y = yetkiler.find(y => y.departman === e.target.value);
    if (y) {
      var acts = [...aksiyonlar];
      acts.map(a => {
        if (y.yetkiler[`${a.field}`]) {
          a.list = y.yetkiler[`${a.field}`].includes('list');
          a.create = y.yetkiler[`${a.field}`].includes('create');
          a.view = y.yetkiler[`${a.field}`].includes('view');
          a.edit = y.yetkiler[`${a.field}`].includes('edit');
          a.delete = y.yetkiler[`${a.field}`].includes('delete');
        }
      });
      setTableData(acts);
    }
    console.log(y);

    /*
    const y = yetkilerData.find(yd => yd.rol === e.target.value);
    clear();
    var acts = [...aksiyonlar];
    if (y) {
      setYeni(false);
      setSelectedRole(y._id);
      setSelectedRoleAdi(y.rol);
      acts.map(a => {
        if (y.yetkiler[`${a.field}`]) {
          a.list = y.yetkiler[`${a.field}`].includes('list');
          a.create = y.yetkiler[`${a.field}`].includes('create');
          a.view = y.yetkiler[`${a.field}`].includes('view');
          a.edit = y.yetkiler[`${a.field}`].includes('edit');
          a.delete = y.yetkiler[`${a.field}`].includes('delete');
        }
      });
    } else {
      setYeni(true);
      setSelectedRoleAdi(e.target.value);
    }
    setTableData(acts);
    */
  };

  const handleCheckbox = (field, status, t) => {
    var td = [...tableData];
    var a = td.find(t => t.field == field);
    a[`${t}`] = status;
    setTableData(td);
  };

  const handleSubmit = () => {
    //if (yeni) {
    //axios.post(`${baseUrl}/api/yetkiler`, {rolAdi: selectedRoleAdi, yetkiler: tableData},
    //{headers: {authorization: token}})
    //.then(resp => console.log(resp))
    //} else {
    //axios.put(`${baseUrl}/api/yetkiler/${selectedRole}`, {yetkiler: tableData},
    //{headers: {authorization: token}})
    //.then(resp => console.log(resp))
    //}
  };

  const canSubmit = () => {
    return selectedRole || yeni;
  };

  const onSave = () => {
    dispatch(Actions.saveYetkiler(selectedDep, tableData));
    //dispatch(Actions.saveYetki())
    //alert('sve')
    //dispatch(Actions.saveMusteri(musteri));
    //props.history.push('/musteriler');
  };

  if (!departmanlar) {
    return <></>;
  }

  return (
    <Fragment>
      <PageTitle
        titleHeading="Yetkilendirme"
        titleDescription="..."
        buttonTitle="Kaydet"
        onSave={onSave}
      />
      <div className="flex-col p-8">
        <Typography variant="h6">Yetki Yönetimi</Typography>
        <TextField
          label="Departman"
          variant="outlined"
          fullWidth
          onChange={handleRolChanged}
          select>
          {departmanlar.map(d => {
            return (
              <MenuItem key={d._id} value={d._id}>
                {d.adi}
              </MenuItem>
            );
          })}
          {/*roller
            .sort((a, b) => {
              if (b.rolAdi > a.rolAdi) return -1;
            })
            .map(r => (
              <MenuItem key={r.rolAdi} value={r.rolAdi}>
                {r.rolAdi}
              </MenuItem>
            ))}

          {/*yetkilerData &&
              yetkilerData.map((y) => (
                <MenuItem key={y._id} value={y._id}>
                  {y.rol}
                </MenuItem>
              ))*/}
        </TextField>
        <Table size="small" className="m-8">
          <TableHead>
            <TableRow>
              <TableCell key="act">Aksiyon</TableCell>
              <TableCell key="l">Listeleme</TableCell>
              <TableCell key="c">Oluşturma</TableCell>
              <TableCell key="v">Detay görme</TableCell>
              <TableCell key="u">Değiştirme</TableCell>
              <TableCell key="d">Silme</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tableData.map((a, i) => (
              <TableRow key={i}>
                <TableCell>{a.title}</TableCell>
                <TableCell>
                  <Checkbox
                    checked={a.list}
                    onChange={e =>
                      handleCheckbox(a.field, e.target.checked, 'list')
                    }
                  />
                </TableCell>
                <TableCell>
                  <Checkbox
                    checked={a.create}
                    onChange={e =>
                      handleCheckbox(a.field, e.target.checked, 'create')
                    }
                  />
                </TableCell>
                <TableCell>
                  <Checkbox
                    checked={a.view}
                    onChange={e =>
                      handleCheckbox(a.field, e.target.checked, 'view')
                    }
                  />
                </TableCell>
                <TableCell>
                  <Checkbox
                    checked={a.edit}
                    onChange={e =>
                      handleCheckbox(a.field, e.target.checked, 'edit')
                    }
                  />
                </TableCell>
                <TableCell>
                  <Checkbox
                    checked={a.delete}
                    onChange={e =>
                      handleCheckbox(a.field, e.target.checked, 'delete')
                    }
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmit}
          disabled={!canSubmit()}>
          Kaydet
        </Button>
      </div>
    </Fragment>
  );
};
export default Yetki;
