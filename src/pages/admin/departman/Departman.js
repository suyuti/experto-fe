import React, {Fragment} from 'react'
import { PageTitle } from 'layout-components'
import { withRouter } from 'react-router'
import Departman from 'components/departman/Departman'
import {useDispatch, useSelector} from 'react-redux'
import * as Actions from 'store/actions'

const DepartmanPage = prosp => {
    const dispatch = useDispatch()
    const departman = useSelector(state => state.departman.departman)
    if (!departman) {
        return <></>
    }
    return <Fragment>
      <PageTitle
        titleHeading="Departman"
        //titleDescription={subtitle}
        buttonTitle="Kaydet"
        onSave={() => {
            dispatch(Actions.saveDepartman(departman))
            prosp.history.push(`/departmanlar`)
          //props.history.goB;
        }}
      />
      <Departman />
    </Fragment>
}

export default withRouter(DepartmanPage)