import React, {Fragment, useEffect} from 'react'
import { PageTitle } from 'layout-components'
import { withRouter } from 'react-router'
import { Grid } from '@material-ui/core'
import DepartmanlarTree from 'components/departman/DepartmanTree'
import { useDispatch } from 'react-redux'
import * as Actions from 'store/actions'

const Departmanlar = props => {
    const dispatch = useDispatch()

    useEffect(() => {
        const load = async () => {
            dispatch(Actions.getDepartmanlar())
        }
        load()
    }, [dispatch])

    return <Fragment>
      <PageTitle
        titleHeading="Departmanlar"
        //titleDescription={subtitle}
        buttonTitle="Yeni"
        onSave={() => {
            dispatch(Actions.getDepartmanlarAsList());
            dispatch(Actions.yeniDepartman())
            props.history.push('/departmanlar/yeni');
        }}
      />
      <Grid container spacing={3}>
          <Grid item xs={12}>
              <DepartmanlarTree />
          </Grid>
      </Grid>
    </Fragment>
}

export default withRouter(Departmanlar)