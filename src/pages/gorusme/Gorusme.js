import React, { Fragment, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import GorusmeInfo from 'components/gorusme/GorusmeInfo';
import * as Actions from 'store/actions';
import PageTitle from 'layout-components/PageTitle';
import { validate } from 'components/gorusme/GorusmeValidator';

const GorusmePage = props => {
  const dispatch = useDispatch();
  const gorusme = useSelector(state => state.gorusme.gorusme);
  const yetkiler = useSelector(state => state.experto.yetkiler);
  const me = useSelector(state => state.graph.me);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.resetError());
      if (props.match.params.id === 'yeni') {
        dispatch(Actions.yeniGorusme({gorusmeYapanMsId: me.id}));
      } else {
        dispatch(Actions.getGorusme(props.match.params.id));
      }

      if (yetkiler.gorusme.includes('edit')) {
        dispatch(Actions.enableKaydetBtn());
      } else {
        dispatch(Actions.disableKaydetBtn());
      }
    };
    load();
  }, [dispatch]);

  if (!gorusme) {
    return <></>;
  }

  const save = () => {
    const err = validate(gorusme);
    if (err) {
      dispatch(Actions.setError(err));
    } else {
      dispatch(Actions.saveGorusme(gorusme));
      dispatch(Actions.resetError());
      props.history.goBack();
    }
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Görüşme Kaydı"
        titleDescription="..."
        buttonTitle="Kaydet"
        onSave={() => {
          save();
        }}
      />
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <GorusmeInfo />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default GorusmePage;
