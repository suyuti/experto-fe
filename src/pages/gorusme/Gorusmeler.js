import React, { Fragment, useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import { Grid, Avatar } from '@material-ui/core';
import MaterialTable from 'material-table';
import PageTitle from 'layout-components/PageTitle';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from 'store/actions';
import moment from 'moment';
const qs = require('query-string');

const Gorusmeler = props => {
  const dispatch = useDispatch();
  const gorusmeler = useSelector(state => state.gorusme.gorusmeler);
  const personeller = useSelector(state => state.graph.personeller);
  const kisiler = useSelector(state => state.kisi.kisiler);
  const [filteredGorusmeler, setFilteredGorusmeler] = useState([]);
  const me = useSelector(state => state.graph.me);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorusmeler());
      dispatch(Actions.getKisiler());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (gorusmeler) {
      debugger;
      const type = qs.parse(props.location.search);
      if (type.t === 'ekip') {
        setFilteredGorusmeler(
          gorusmeler.filter(g => g.gorusmeYapanMsId !== me.id)
        );
      } else if (type.t === 'me') {
        setFilteredGorusmeler(
          gorusmeler.filter(g => g.gorusmeYapanMsId === me.id)
        );
      } else {
        setFilteredGorusmeler(gorusmeler);
      }
    }
  }, [gorusmeler, props.location.search]);

  if (!kisiler) {
    return <></>;
  }

  return (
    <Fragment>
      <PageTitle
        titleHeading="Görüşmeler"
        //titleDescription={subtitle}
        buttonTitle="Yeni"
        onSave={() => {
          props.history.push('/gorusme/yeni');
        }}
      />
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MaterialTable
            title="Görüşmeler"
            data={filteredGorusmeler || []}
            columns={[
              { title: 'Firma', field: 'musteri.marka' },
              {
                title: 'Kişi',
                field: 'kisi._id',
                //customSort: (a, b) => {
                //  return a.kisi.adi > b.kisi.adi ? 1 : -1;
                //},
                render: (rowData, type) => {
                  if (type === 'row') {
                    return (
                      <div className="">
                        <div className="text-500">{`${rowData.kisi.adi} ${rowData.kisi.soyadi}`}</div>
                        <div className="">{`${rowData.kisi.unvani}`}</div>
                      </div>
                    );
                  } else {
                    debugger;
                    let k = kisiler.find(k => k._id === rowData);
                    if (k) {
                      return `${k.adi} ${k.soyadi} ${k.unvani}`;
                    } else {
                      return '';
                    }
                  }
                }
              },
              { title: 'Konu', field: 'konu' },
              {
                title: 'Zaman',
                customSort: (a, b) => {
                  return moment(a.tarih) - moment(b.tarih);
                },

                render: rowData =>
                  `${moment(rowData.tarih).format('DD.MM.yyyy')} ${
                    rowData.saat
                  }`
              },
              { title: 'Kanal', field: 'kanal' },
              {
                title: 'Görüşme yapan',
                field: 'gorusmeYapanMsId',
                render: (rowData, type) => {
                  console.log(type);
                  if (type === 'row') {
                    if (rowData.gorusmeYapanMsId) {
                      let p = personeller.find(
                        p => p.id === rowData.gorusmeYapanMsId
                      );
                      if (p) {
                        return (
                          <div className="d-flex align-items-center">
                            <Avatar src={p.photo} />
                            <div className="ml-2">{p.displayName}</div>
                          </div>
                        );
                      }
                    }
                  } else {
                    if (rowData) {
                      let p = personeller.find(p => p.id === rowData);
                      if (p) {
                        return (
                          <div className="d-flex align-items-center">
                            <Avatar src={p.photo} />
                            <div className="ml-2">{p.displayName}</div>
                          </div>
                        );
                      }
                    }
                  }
                }
              }
            ]}
            //actions={[
            //  {
            //    icon: 'edit',
            //    tooltip: 'Detail',
            //    onClick: (event, rowData) =>
            //      props.history.push(`/musteriler/${rowData._id}`),
            //    disabled: !enableEdit // TODO Yetkisi yoksa goremeyecek
            //  }
            //]}
            options={{
              grouping: true,
              actionsColumnIndex: -1,
              pageSize: 20
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(Gorusmeler);
