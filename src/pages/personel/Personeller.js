
import React, {Fragment, useEffect, useState} from 'react'
import MaterialTable from 'material-table'
import { useSelector, useDispatch } from 'react-redux'
import * as Actions from "store/actions";

const Personeller = props => {
    const dispatch = useDispatch()
    const personeller = useSelector(state => state.graph.personeller)
    const expertoPersoneller = useSelector(state => state.experto.expertoPersoneller)
    const [personelList, setPersonelList] = useState([])

    useEffect(() => {
        dispatch(Actions.getExpertoPersoneller())
    }, [dispatch])

    useEffect(() => {
        if (expertoPersoneller && personeller) {
            debugger
            for (var p of expertoPersoneller) {
                personelList.push({adi: 'c'})
            }
        }
    }, [expertoPersoneller, personeller])

    if (!personelList) {
        return <></>
    }

    return <Fragment>
        <MaterialTable 
            title='Personeller'
            data={personelList}
            columns={[
                { title: 'Adi Soyadi', field: 'adi'},
                { title: 'Departman', field: 'displayName'},
                { title: 'Yetki', field: 'displayName'}
            ]}
        />
    </Fragment>
}
export default Personeller