import React, { Fragment, useEffect, useState } from 'react';
import GorevList from '../../components/gorev/GorevList';
import MailList from '../../components/mail/MailList';
import { Grid, Tabs, Tab } from '@material-ui/core';
import BildirimPanel from '../../components/dashboard/BildirimPanel';
import { useDispatch } from 'react-redux';
import * as Actions from 'store/actions';
import TabPanel from 'components/TabPanel';
import GorevlerPanel from 'components/dashboard/GorevlerPanel';
import TakvimPanel from 'components/dashboard/TakvimPanel';
import ToplantilarPanel from 'components/dashboard/ToplantilarPanel';

const DashboardSatis = props => {
  const dispatch = useDispatch();
  const [selectedTab, setSelectedTab] = useState(0);

  useEffect(() => {
    dispatch(Actions.getGorevler());
  }, [dispatch]);

  return (
    <Fragment>
      <Tabs
        value={selectedTab}
        onChange={(event, newValue) => setSelectedTab(newValue)}>
        <Tab label="Görevler" />
        <Tab label="Toplantılar" />
        <Tab label="Takvim" />
      </Tabs>
      <TabPanel value={selectedTab} index={0}>
        <div className="p-4">
          <GorevlerPanel />
        </div>
      </TabPanel>
      <TabPanel value={selectedTab} index={1}>
        <div className="p-4">
          <ToplantilarPanel />
        </div>
      </TabPanel>
      <TabPanel value={selectedTab} index={2}>
        <div className="p-4">
          <TakvimPanel />
        </div>
      </TabPanel>
    </Fragment>
  );
};
export default DashboardSatis;

/*
<GorevList gorevTipi="acik" />
        <Grid item xs={6}>
          <MailList />
        </Grid>

*/
