import React, { Fragment, useState, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, Tabs, Tab, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TabPanel from 'components/TabPanel';
import MusteriInfo from 'components/musteri/MusteriInfo';
import MusteriIletisim from 'components/musteri/MusteriIletisim';
import MusteriKisiler from 'components/musteri/MusteriKisiler';
import MusteriMaliBilgiler from 'components/musteri/MusteriMaliBilgiler';
import MusteriTeklifler from 'components/musteri/MusteriTeklifler';
import MusteriSozlesmeler from 'components/musteri/MusteriSozlesmeler';
import MusteriYazismalar from 'components/musteri/MusteriYazismalar';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import Toolbar from 'components/Toolbar';
import MusteriKaynagi from 'components/musteri/MusteriKaynagi';
import MusteriPatentler from 'components/musteri/MusteriPatentler';
import MusteriUrunler from 'components/musteri/MusteriUrunler';
import MusteriToplantilar from 'components/musteri/MusteriToplantilar';
import MusteriGorevler from 'components/musteri/MusteriGorevler';
import { withRouter } from 'react-router';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}));

const Musteri = props => {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = useState('genelbilgiler');
  const dispatch = useDispatch();
  const musteri = useSelector(state => state.experto.musteri);
  const yetkiler = useSelector(state => state.experto.yetkiler);

  useEffect(() => {
    const load = async () => {
      if (props.match.params.id === 'yeni') {
        dispatch(Actions.yeniMusteri());
      } else {
        dispatch(Actions.getMusteri(props.match.params.id));
      }
    };
    load();
    return () => {
      dispatch(Actions.enableKaydetBtn());
    };
  }, [dispatch]);

  useEffect(() => {
    if (musteri) {
      if (musteri.durum === 'aktif') {
        if (yetkiler.aktifmusteri.includes('edit')) {
          dispatch(Actions.enableKaydetBtn());
        } else {
          dispatch(Actions.disableKaydetBtn());
        }
      }
      if (musteri.durum === 'pasif') {
        if (yetkiler.pasifmusteri.includes('edit')) {
          dispatch(Actions.enableKaydetBtn());
        } else {
          dispatch(Actions.disableKaydetBtn());
        }
      }
      if (musteri.durum === 'potansiyel') {
        if (yetkiler.potansiyelmusteri.includes('edit')) {
          dispatch(Actions.enableKaydetBtn());
        } else {
          dispatch(Actions.disableKaydetBtn());
        }
      }
      if (musteri.durum === 'sorunlu') {
        if (yetkiler.sorunlumusteri.includes('edit')) {
          dispatch(Actions.enableKaydetBtn());
        } else {
          dispatch(Actions.disableKaydetBtn());
        }
      }
    }
  }, [musteri]);

  if (!musteri) {
    return <></>;
  }

  const onSave = () => {
    dispatch(Actions.saveMusteri(musteri));
    props.history.push('/musteriler');
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Müşteriler"
        titleDescription="..."
        buttonTitle="Kaydet"
        onSave={onSave}
      />
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Tabs
            orientation="vertical"
            variant="scrollable"
            value={selectedTab}
            onChange={(event, newValue) => {
              switch (newValue) {
                case 'genelbilgiler':
                case 'iletisim':
                case 'kisiler':
                case 'malibilgiler':
                  dispatch(Actions.enableKaydetBtn());
                  break;
                default:
                  dispatch(Actions.disableKaydetBtn());
              }
              setSelectedTab(newValue);
            }}
            className={classes.tabs}>
            <Tab label="Genel bilgiler" value="genelbilgiler" />
            <Tab label="İletişim" value="iletisim" />
            {!musteri.yeni && 
            <Tab label="Kişiler" value="kisiler" />
            }
            <Tab label="Mali Bilgiler" value="malibilgiler" />
            {yetkiler.musterikaynak.includes('list') && (
              <Tab label="Müşteri Kaynağı" value="musterikaynagi" />
            )}
            {!musteri.yeni && yetkiler.teklif.includes('list') && (
              <Tab label="Teklifler" value="teklifler" />
            )}
            {!musteri.yeni && yetkiler.sozlesme.includes('list') && (
              <Tab label="Sözleşmeler" value="sozlesmeler" />
            )}
            {!musteri.yeni && <Tab label="Yazışmalar" value="yazismalar" />}
            {!musteri.yeni && (
              <Tab label="Patent Bilgileri" value="patentler" />
            )}
            {!musteri.yeni && <Tab label="Ürünleri" value="urunler" />}
            {!musteri.yeni && <Tab label="Toplantılar" value="toplantilar" />}
            {!musteri.yeni && <Tab label="İlgili Görevler" value="gorevler" />}
          </Tabs>
        </Grid>
        <Grid container item xs={10}>
          {/* 
          <Grid item xs={12}>
            <Toolbar />
            <Divider />
          </Grid>
*/}
          <Grid item xs={12}>
            <TabPanel value={selectedTab} index="genelbilgiler">
              <MusteriInfo />
            </TabPanel>
            <TabPanel value={selectedTab} index="iletisim">
              <MusteriIletisim />
            </TabPanel>
            {!musteri.yeni && (
              <TabPanel value={selectedTab} index="kisiler">
                <MusteriKisiler />
              </TabPanel>
            )}
            <TabPanel value={selectedTab} index="malibilgiler">
              <MusteriMaliBilgiler />
            </TabPanel>
            <TabPanel value={selectedTab} index="musterikaynagi">
              <MusteriKaynagi />
            </TabPanel>
            {!musteri.yeni && (
              <Fragment>
                <TabPanel value={selectedTab} index="teklifler">
                  <MusteriTeklifler />
                </TabPanel>
                <TabPanel value={selectedTab} index="sozlesmeler">
                  <MusteriSozlesmeler />
                </TabPanel>
                <TabPanel value={selectedTab} index="yazismalar">
                  <MusteriYazismalar />
                </TabPanel>
                <TabPanel value={selectedTab} index="patentler">
                  <MusteriPatentler />
                </TabPanel>
                <TabPanel value={selectedTab} index="urunler">
                  <MusteriUrunler />
                </TabPanel>
                <TabPanel value={selectedTab} index="toplantilar">
                  <MusteriToplantilar />
                </TabPanel>
                <TabPanel value={selectedTab} index="gorevler">
                  <MusteriGorevler />
                </TabPanel>
              </Fragment>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(Musteri);
