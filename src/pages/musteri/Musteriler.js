import React, { Fragment, useEffect, useState } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, Avatar, LinearProgress } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { withRouter } from 'react-router';
const qs = require('query-string');

const Musteriler = props => {
  const dispatch = useDispatch();
  const musteriler = useSelector(state => state.experto.musteriler);
  const personeller = useSelector(state => state.graph.personeller);
  const yetkiler = useSelector(state => state.experto.yetkiler);
  const [subtitle, setSubtitle] = useState('');
  const [enableEdit, setEnableEdit] = useState(true)

  useEffect(() => {
    const load = async () => {
      const musteriTip = qs.parse(props.location.search);
      dispatch(Actions.getMusteriler({ durum: musteriTip.durum }));
      if (musteriTip.durum === 'aktif') {
        setSubtitle('Aktif Müşteriler');
        if (yetkiler.aktifmusteri.includes('create')) {
          dispatch(Actions.enableKaydetBtn())
        }
        else {
          dispatch(Actions.disableKaydetBtn())
        }
        setEnableEdit(yetkiler.aktifmusteri.includes('view'))
      } else if (musteriTip.durum === 'pasif') {
        setSubtitle('Pasif Müşteriler');
        if (yetkiler.pasifmusteri.includes('create')) {
          dispatch(Actions.enableKaydetBtn())
        }
        else {
          dispatch(Actions.disableKaydetBtn())
        }
        setEnableEdit(yetkiler.pasifmusteri.includes('view'))
      } else if (musteriTip.durum === 'potansiyel') {
        if (yetkiler.potansiyelmusteri.includes('create')) {
          dispatch(Actions.enableKaydetBtn())
        }
        else {
          dispatch(Actions.disableKaydetBtn())
        }
        setEnableEdit(yetkiler.potansiyelmusteri.includes('view'))
        setSubtitle('Potansiyel Müşteriler');
      } else if (musteriTip.durum === 'sorunlu') {
        if (yetkiler.sorunlumusteri.includes('create')) {
          dispatch(Actions.enableKaydetBtn())
        }
        else {
          dispatch(Actions.disableKaydetBtn())
        }
        setEnableEdit(yetkiler.sorunlumusteri.includes('view'))
        setSubtitle('Sorunlu Müşteriler');
      }
    };
    load();
  }, [dispatch, props.location.search]);

  return (
    <Fragment>
      <PageTitle
        titleHeading="Müşteriler"
        titleDescription={subtitle}
        buttonTitle="Yeni"
        onSave={() => {
          props.history.push('/musteriler/yeni');
        }}
      />
      {!musteriler && 
      <LinearProgress />
      }
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MaterialTable
            title="Musteriler"
            data={musteriler || []}
            columns={[
              { title: 'Marka', field: 'marka' },
              /*{
                title: 'Müşteri Temsilcisi',
                render: rowData => {
                  if (rowData.musteriTemsilcisi) {
                    let p = personeller.find(
                      p => p.id === rowData.musteriTemsilcisi
                    );
                    if (p) {
                      return (
                        <div className="d-flex align-items-center">
                          <Avatar src={p.photo} />
                          <div className="ml-2">{p.displayName}</div>
                        </div>
                      );
                    }
                  }
                }
              },
              */
              { title: 'Ticari Unvan', field: 'unvan' },
              //{ title: 'Vergi Dairesi', field: 'firmaBilgi.vergiDairesi' },
              //{ title: 'Vergi No', field: 'firmaBilgi.vergiNo' },
              { title: 'İl', field: 'iletisim.il' },
              { title: 'İlçe', field: 'iletisim.ilce' },
              //{ title: 'Web Sitesi', field: 'iletisim.webSitesi' },
              { title: 'Sektor', field: 'sektor.adi' },
              {
                title: 'Kobi/Sanayi',
                render: rowData => {
                  if (rowData.firmaBilgi.kobiMi) {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-info">
                        KOBI
                      </div>
                    );
                  } else {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-warning">
                        Sanayi
                      </div>
                    );
                  }
                }
              }
            ]}
            actions={[
              {
                icon: 'edit',
                tooltip: 'Detail',
                onClick: (event, rowData) =>
                  props.history.push(`/musteriler/${rowData._id}`),
                disabled: !enableEdit // TODO Yetkisi yoksa goremeyecek
              }
            ]}
            options={{
              grouping: true,
              actionsColumnIndex: -1,
              pageSize: 20
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(Musteriler);
