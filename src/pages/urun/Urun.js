import React, { Fragment, useState, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, Tabs, Tab, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TabPanel from 'components/TabPanel';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import Toolbar from 'components/Toolbar';
import UrunInfo from 'components/urun/UrunInfo';
import UrunFiyatlar from 'components/urun/UrunFiyatlar';
import UrunDokumanlar from 'components/urun/UrunDokumanlar';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}));

const Urun = props => {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = useState(0);
  const dispatch = useDispatch();
  const urun = useSelector(state => state.urun.urun);
  const yetkiler = useSelector(state => state.experto.yetkiler);

  useEffect(() => {
    const load = async () => {
      if (yetkiler.urun.includes('edit')) {
        dispatch(Actions.enableKaydetBtn());
      } else {
        dispatch(Actions.disableKaydetBtn());
      }

      if (props.match.params.id === 'yeni') {
        dispatch(Actions.yeniUrun());
      } else {
        dispatch(Actions.getUrun(props.match.params.id));
      }
    };
    load();
  }, [dispatch]);

  if (!urun) {
    return <></>;
  }
  const onSave = () => {
    dispatch(Actions.saveUrun(urun));
    props.history.push('/urunler');
  };

  return (
    <Fragment>
      <PageTitle 
        titleHeading={urun.adi} 
        titleDescription="Ürün detay bilgileri"
        buttonTitle="Kaydet"
        onSave={onSave}
        />
      <Grid container spacing={3}>
        <Grid item xs={8} container spacing={3}>
          <Grid item xs={12}>
            <UrunInfo />
          </Grid>
          <Grid item xs={12}>
            <UrunFiyatlar />
          </Grid>
        </Grid>
        <Grid item xs={4}>
          <UrunDokumanlar />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default Urun;
