import React, { Fragment, useEffect, useState } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, LinearProgress } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { withRouter } from 'react-router';

const Urunler = props => {
  const dispatch = useDispatch();
  const urunler = useSelector(state => state.urun.urunler);
  const yetkiler = useSelector(state => state.experto.yetkiler);
  const [enableEdit, setEnableEdit] = useState(true)

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getUrunler());
      setEnableEdit(yetkiler.urun.includes('view'))

      if (yetkiler.urun.includes('create')) {
        dispatch(Actions.enableKaydetBtn())
      }
      else {
        dispatch(Actions.disableKaydetBtn())
      }
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <PageTitle
        titleHeading="Ürünler"
        titleDescription="..."
        buttonTitle="Yeni"
        onSave={() => {
          props.history.push('/urun/yeni');
        }}
      />
      {!urunler && <LinearProgress />}
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <MaterialTable
            title="Ürünler"
            data={urunler || []}
            columns={[{ title: 'Ürün Adı', field: 'adi' }]}
            actions={[
              {
                icon: 'edit',
                tooltip: 'Detail',
                onClick: (event, rowData) =>
                  props.history.push(`/urun/${rowData._id}`),
                  disabled: !enableEdit // TODO Yetkisi yoksa goremeyecek
                }
            ]}
            options={{
              actionsColumnIndex: -1,
              pageSize: 20
            }}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(Urunler);
