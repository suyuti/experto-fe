import React, { Fragment, useState, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import { Grid, Button, Avatar } from '@material-ui/core';
import ReactDataGrid from 'react-data-grid';
import Handsontable from 'handsontable';
import { HotTable, HotColumn, BaseEditorComponent } from '@handsontable/react';
import 'handsontable/dist/handsontable.full.css';
import IlgiliKisiRenderer from '../../components/toplantiKarar/IlgiliKisiRenderer';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { SET_TOPLANTI } from '../../store/actions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';

class IlgiliKisilerRenderer2 extends BaseEditorComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <Avatar />
      </Fragment>
    );
  }
}

const ToplantiKararPage = props => {
  const dispatch = useDispatch();
  var initialGridSettings = {
    gridSettings: {
      manualColumnResize: true,
      manualRowResize: true,
      rowHeaders: true,
      colHeaders: true,
      rowHeights: 60,
      stretchH: 'all',
      licenseKey: 'non-commercial-and-evaluation',
      minSpareRows: 1
      //afterChange: changes => onChange(changes),
      //beforeCreateRow: (index, amount, source) => {
      //  console.log(index);
      //  console.log(amount);
      //  console.log(source);
      //},
      //afterCreateRow: (index, amount, source) => {
      //  console.log(kararlar);
      //  console.log(index);
      //  console.log(amount);
      //  console.log(source);
      //}
    },

    colIdSettings: {
      title: 'No',
      readOnly: true,
      width: 50,
      className: 'htCenter'
    },

    colKararSettings: {
      title: 'Karar',
      width: 150,
      className: 'htLeft',
      data: 'karar'
    },
    colIlgiliSettings: {
      title: 'İlgili Kişi',
      width: 150,
      className: 'htCenter',
      type: 'autocomplete',
      source: [''],
      strict: true,
      //allowInvalid: false,
      data: 'expertoAtamaDisplayName'
      //renderer: function(instance, td, row, col, prop, value, cellProperties) {
      //  return td;
      //}
      //IlgiliKisiRenderer
    },
    colFirmaIlgiliSettings: {
      title: 'Firma İlgili Kişi',
      width: 150,
      className: 'htCenter',
      type: 'autocomplete',
      source: [''],
      strict: true,
      data: 'firmaIlgiliKisi'
      //allowInvalid: false,
      //data: 'expertoAtamaDisplayName'
      //renderer: function(instance, td, row, col, prop, value, cellProperties) {
      //  return td;
      //}
      //IlgiliKisiRenderer
    },
    colSonTarihSettings: {
      title: 'Son Tarih',
      width: 150,
      type: 'date',
      data: 'sonTarih',
      dateFormat: 'DD.MM.YYYY',
      correctFormat: true,
      className: 'htCenter',
      datePickerConfig: {
        // First day of the week (0: Sunday, 1: Monday, etc)
        firstDay: 1,
        showWeekNumber: true,
        numberOfMonths: 1,
        disableDayFn: function(date) {
          // Disable Sunday and Saturday
          return date.getDay() === 0 || date.getDay() === 6;
        }
      }
    },
    colDurumSettings: {
      title: 'Durum',
      width: 150,
      data: 'durum',
      type: 'autocomplete',
      source: ['Açık', 'Bilgi', 'Tamamlandı', 'İptal'],
      strict: true,
      allowInvalid: false
    }
  };
  const [gridSettings, setGridSettings] = useState(null);

  const me = useSelector(state => state.graph.me);
  const personeller = useSelector(state => state.graph.personeller);
  const _toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const kararlar = useSelector(state => state.expertoToplanti.kararlar);
  const kisiler = useSelector(state => state.kisi.kisiler);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getToplantiKararlari(props.match.params.id));
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    // Experto personel listesi
    if (
      kararlar &&
      personeller &&
      _toplanti &&
      !_toplanti.icToplantiMi
      //(kisiler && ) 
    ) {
      initialGridSettings.colIlgiliSettings.source = personeller.map(
        p => p.displayName
      );
      initialGridSettings.colFirmaIlgiliSettings.source = kisiler ? kisiler.map(
        p => `${p.adi} ${p.soyadi} [${p.unvani}]` 
      ) : []

      setGridSettings(initialGridSettings);

      if (_toplanti.tamamlandi) {
        dispatch(Actions.disableKaydetBtn())
      }
      else {
        dispatch(Actions.enableKaydetBtn())
      }
    }
  }, [kararlar, personeller, kisiler]);

  useEffect(() => {
    if (_toplanti && _toplanti.firma) {
      dispatch(Actions.getMusteriKisiler(_toplanti.firma._id));
    }
    else {
      // kisi listesini temizlemek icin
      dispatch({type: Actions.GET_MUSTERI_KISILER_REQ})
    }
  }, [_toplanti]);

  if (!_toplanti) {
    return <></>;
  }
  if (!kararlar) {
    return <></>;
  }
  if (!gridSettings) {
    return <></>;
  }

  const save = () => {
    var kaydet = false;
    var kaydedilecekKararlar = [];
    for (var i = 0; i < kararlar.length - 1; ++i) {
      var karar = kararlar[i];
      if (
        karar.durum &&
        karar.expertoAtamaDisplayName &&
        karar.karar &&
        karar.sonTarih
      ) {
        kaydet = true;
      } else {
        kaydet = false;
        break;
      }

      if (!karar._id) {
        let p = personeller.find(
          p => p.displayName === karar.expertoAtamaDisplayName
        );
        if (p) {
          karar.expertoAtamaMsId = p.id;
        }
        kaydedilecekKararlar.push(karar);
      }
    }
    if (kaydet) {
      if (kaydedilecekKararlar.length > 0) {
        dispatch(
          Actions.saveToplantiKararlari(_toplanti, kaydedilecekKararlar)
        );
        props.history.push(`/toplanti/${_toplanti._id}`);
      }
    } else {
      dispatch(Actions.showMessage('TOPLANTI KARARI KAYDEDILEMEDI !'));
    }

    //kararlar.map(k => {
    //  debugger;
    //  if (k._id == null) {
    //    let p = personeller.find(
    //      p => p.displayName === k.expertoAtamaDisplayName
    //    );
    //    if (p) {
    //      k.expertoAtamaMsId = p.id;
    //    }
    //  }
    //});
    //dispatch(Actions.saveToplantiKararlari(_toplanti, kararlar));
    //props.history.push(`/toplanti/${_toplanti._id}`);
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Toplantı Kararları"
        titleDescription={`${_toplanti.baslik} - ${
          _toplanti.firma ? _toplanti.firma.marka : 'İç Toplantı'
        }`}
        onSave={() => {
          save();
          //dispatch(Actions.saveToplantiKararlari(_toplanti, kararlar));
          //dispatch(Actions.saveToplanti(_toplanti))
        }}
        buttonTitle="Kaydet"
      />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div className="d-flex justify-items-end">
          <Button
              variant="contained"
              color="primary"
              className="m-2 p-2"
              onClick={() => {
                dispatch(Actions.getToplantiPdf(_toplanti));
              }}>
              <span className="btn-wrapper--icon">
                <FontAwesomeIcon icon={['far', 'file-pdf']} />
              </span>
              <span className="btn-wrapper--label">Toplantı Raporu</span>
            </Button>
            </div>
        </Grid>
        <Grid item xs={12}>
          <HotTable  data={kararlar} settings={gridSettings.gridSettings}>
            <HotColumn settings={gridSettings.colKararSettings} />
            <HotColumn settings={gridSettings.colIlgiliSettings} />
            <HotColumn settings={gridSettings.colFirmaIlgiliSettings} />
            <HotColumn settings={gridSettings.colSonTarihSettings} />
            <HotColumn settings={gridSettings.colDurumSettings} />
          </HotTable>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default ToplantiKararPage;
