import React, { Fragment, useEffect, useState } from 'react';
import { PageTitle } from 'layout-components';
import {
  Grid,
  CardHeader,
  IconButton,
  Card,
  Divider,
  CardContent,
  Button,
  Avatar,
  Tooltip,
  LinearProgress
} from '@material-ui/core';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import AddIcon from '@material-ui/icons/Add';
import ToplantiList from '../../components/toplanti/ToplantiList';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import ToplantiTakvim from 'components/toplanti/ToplantiTakvim';
import endOfDayfrom from 'date-fns/endOfDay';
import startOfDay from 'date-fns/startOfDay';
import moment from 'moment';
import MaterialTable from 'material-table';
import AvatarGroup from '@material-ui/lab/AvatarGroup';

const qs = require('query-string');

const Toplantilar = props => {
  const dispatch = useDispatch();
  const [subtitle, setSubtitle] = useState('');
  const toplantilar = useSelector(state => state.expertoToplanti.toplantilar);
  const me = useSelector(state => state.graph.me);
  const [filteredToplantilar, setFilteredToplantilar] = useState([]);
  const personeller = useSelector(state => state.graph.personeller);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getToplantilar());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (toplantilar) {
      const type = qs.parse(props.location.search);
      const today = new Date().setHours(0, 0, 0, 0);
      if (type.s === 'f') {
        // gelecek
        setFilteredToplantilar(
          toplantilar.filter(
            t =>
              t.expertoKatilimcilar.some(s => s === me.id) &&
              today <= new Date(t.tarih).setHours(0, 0, 0, 0)
          )
        );
      } else if (type.s === 'p') {
        // gecmis
        setFilteredToplantilar(
          toplantilar.filter(
            t =>
              t.expertoKatilimcilar.some(s => s === me.id) &&
              today > new Date(t.tarih).setHours(0, 0, 0, 0)
          )
        );
      } else if (type.t === 'ekipgelecek') {
        // gecmis
        setFilteredToplantilar(
          toplantilar.filter(
            t =>
              t.expertoKatilimcilar.some(s => s !== me.id) &&
              today <= new Date(t.tarih).setHours(0, 0, 0, 0)
          )
        );
      } else if (type.t === 'ekipgecmis') {
        // gecmis
        setFilteredToplantilar(
          toplantilar.filter(
            t =>
              t.expertoKatilimcilar.some(s => s !== me.id) &&
              today > new Date(t.tarih).setHours(0, 0, 0, 0)
          )
        );
      }
    }
  }, [toplantilar, props.location.search]);

  /*
  useEffect(() => {
    const load = async () => {
      let param = qs.parse(props.location.search);
      if (param.s === 'f') {
        setSubtitle('Gelecek toplantılar')
        dispatch(
          Actions.getToplantilar({expertoKatilimcilar: me.id, tarih: { $gte: startOfDay(new Date()) } })
        );
      } else if (param.s === 'p') {
        setSubtitle('Geçmiş toplantılar')
        dispatch(
          Actions.getToplantilar({expertoKatilimcilar: me.id,  tarih: { $lt: startOfDay(new Date()) } })
        );
      }
    };
    load();
  }, [dispatch, props.location.search]);
  */
  const [showCalendar, setShowCalendar] = useState(false);

  return (
    <Fragment>
      <PageTitle
        titleHeading="Toplantılar"
        titleDescription={subtitle}
        buttonTitle="Yeni"
        onSave={() => props.history.push('/toplanti/yeni')}
      />
      {!toplantilar && <LinearProgress />}

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Toplantilar"
              avatar={<InsertInvitationIcon fontSize="large" />}
              action={
                <IconButton
                  onClick={() => {
                    props.history.push('/toplanti/yeni');
                  }}>
                  <AddIcon fontSize="large" />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <MaterialTable
                title="Toplantılar"
                data={filteredToplantilar || []}
                columns={[
                  { title: 'Konu', field: 'baslik' },
                  { title: 'Firma', field: 'firma.marka' },
                  {
                    title: 'Zaman',
                    field: 'tarih',
                    render: rowData =>
                      moment(rowData.tarih).format('DD.MM.yyyy')
                  },
                  { title: 'Yer', field: 'yer' },
                  { title: 'Kategori', field: 'kategori' },
                  {
                    title: 'Kararlar',
                    render: rowData => {
                      const today = new Date().setHours(0,0,0,0);
                      const topTar = new Date(rowData.tarih).setHours(0,0,0,0);

                      return (
                        <Button
                          disabled={today < topTar}
                          variant="contained"
                          color="primary"
                          onClick={() => {
                            dispatch(Actions.getToplanti(rowData._id));
                            props.history.push(
                              `/toplanti/${rowData._id}/karar`
                            );
                          }}>
                          Kararlar
                        </Button>
                      );
                    }
                  },
                  {
                    title: 'Katılımcılar',
                    render: rowData => {
                      {
                        return (
                          <div>
                            {rowData.expertoKatilimcilar.map(ek => {
                              let p = personeller.find(p => p.id == ek);
                              if (!p) {
                                return <></>;
                              }
                              return (
                                <div className="d-flex align-items-center mb-2">
                                  <Avatar src={p.photo} />
                                  <div className="ml-2">{p.displayName}</div>
                                </div>
                              );
                            })}
                          </div>
                        );
                      }
                    }
                  },
                  {
                    title: 'Durum',
                    field: 'tamamlandi',
                    render: row => {
                      if (row.tamamlandi) {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-success">
                            Tamamlandı
                          </div>
                        );
                      } else {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-info">
                            Tamamlanmadı
                          </div>
                        );
                      }
                    }
                  }
                ]}
                actions={[
                  {
                    icon: 'edit',
                    tooltip: 'Detail',
                    onClick: (event, rowData) =>
                      props.history.push(`/toplanti/${rowData._id}`),
                    disabled: false // TODO Yetkisi yoksa goremeyecek
                  }
                ]}
                options={{
                  grouping: true,
                  actionsColumnIndex: -1,
                  pageSize: 20
                }}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default Toplantilar;
