import React, { Fragment, useEffect } from 'react';
import { PageTitle } from 'layout-components';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import ToplantiPanel from 'components/toplanti/ToplantiPanel';
import { validate } from 'components/toplanti/ToplantiValidator';

const Toplanti = props => {
  const dispatch = useDispatch();
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const me = useSelector(state => state.graph.me);

  useEffect(() => {
    const load = async () => {
      if (props.match.params.id === 'yeni') {
        dispatch(Actions.yeniToplanti(me));
      } else {
        dispatch(Actions.getToplanti(props.match.params.id));
      }
      dispatch(Actions.enableKaydetBtn());
    };
    load();
  }, [dispatch]);

  if (props.match.params.id === 'yeni') {
    if (!toplanti) {
      return <></>;
    }
    if (!toplanti.yeni) {
      return <></>;
    }
  }
  if (!toplanti) {
    return <></>;
  }

  const save = () => {
    const err = validate(toplanti);
    if (err) {
      dispatch(Actions.setError(err));
    } else {
      dispatch(Actions.saveToplanti(toplanti));
      dispatch(Actions.resetError());
      props.history.goBack();
    }
  };

  return (
    <Fragment>
      <PageTitle
        titleHeading="Toplantı"
        titleDescription={
          toplanti && toplanti.kayit ? 'kaydedildi' : 'kaydedilmedi'
        }
        buttonTitle="Kaydet"
        onSave={() => {
          save();
          //dispatch(Actions.saveToplanti(toplanti));
        }}
      />
      <ToplantiPanel />
    </Fragment>
  );
};

export default Toplanti;
