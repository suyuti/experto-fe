import React, { Fragment } from 'react';
import {Avatar} from '@material-ui/core'

const PersonelCard = props => {
  const { user } = props;
  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <Avatar alt="..." src={user && user.photo} className="mr-2" />
        <div>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="font-weight-bold text-black"
            title="...">
            {user && user.displayName}
          </a>
          <span className="text-black-50 d-block">{user && user.jobTitle}</span>
        </div>
      </div>
    </Fragment>
  );
};

export default PersonelCard;
