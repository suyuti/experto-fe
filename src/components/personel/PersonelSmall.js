import React, { Fragment } from 'react';
import { Avatar } from '@material-ui/core';

const PersonelSmall = props => {
  const { user } = props;
  return (
    <Fragment>
      <div className="d-flex align-items-center">
        <Avatar className="mr-4" src={user.photo} />
        {user.displayName}
        <span className="text-black">{user.jobTitle}</span>
      </div>
    </Fragment>
  );
};

export default PersonelSmall;
