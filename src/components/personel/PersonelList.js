import React, { Fragment } from 'react';
import { Menu, List, ListItem, Divider } from '@material-ui/core';
import PersonelSmall from '../personel/PersonelSmall'

const PersonelList = props => {
  const { title, onSelect, users } = props;
  return (
    <Fragment>
      <div className="overflow-hidden dropdown-menu-xl p-0">
        <div className="bg-composed-wrapper bg-light mt-0">
          <div className="bg-composed-wrapper--content text-black px-4 py-3">
            <h5 className="mb-1">{title}</h5>
            <p className="mb-0 opacity-7"></p>
          </div>
        </div>
        <List>
          {users.map(p => {
            return (
              <Fragment>
                <ListItem
                  button
                  className="align-box-row"
                  onClick={() => onSelect(p.id)}>
                  <PersonelSmall user={p} />
                </ListItem>
                <Divider />
              </Fragment>
            );
          })}
        </List>
      </div>
    </Fragment>
  );
};

export default PersonelList;
