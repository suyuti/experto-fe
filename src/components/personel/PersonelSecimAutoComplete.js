import React, { Fragment } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { Chip, Avatar } from '@material-ui/core';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;
const PersonelSecimAutoComplete = props => {
  const { personeller, onSelected } = props;
  return (
    <Fragment>
      <Autocomplete
        multiple
        options={personeller || []}
        disableCloseOnSelect
        getOptionLabel={option => option.displayName}
        renderTags={(tagValue, getTagProps) =>
          tagValue.map((opt, index) => (
            <Chip
              avatar={<Avatar src={opt.photo} />}
              label={opt.displayName}
              variant="outlined"
              {...getTagProps({ index })}
            />
          ))
        }
        renderOption={(option, { selected }) => (
          <React.Fragment>
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              style={{ marginRight: 3 }}
              checked={selected}
            />
            <div className="d-flex align-items-center">
              <Avatar src={option.photo} sizes="  " />
              {option.displayName}
            </div>
          </React.Fragment>
        )}
        onChange={(event, newValue) => {
            onSelected(newValue.map(v => v.id))
        }}
        renderInput={params => (
          <TextField
            {...params}
            fullWidth
            variant="outlined"
            label="Personeller"
            placeholder="Personeller"
          />
        )}
      />{' '}
    </Fragment>
  );
};

export default PersonelSecimAutoComplete;
