import React, { Fragment, useEffect, useState } from 'react';
import {
  Grid,
  Paper,
  ButtonBase,
  Avatar,
  Tooltip,
  IconButton
} from '@material-ui/core';
import GorevList from 'components/gorev/GorevList';
import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Chart from 'react-apexcharts';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { PulseLoader } from 'react-spinners';
import MaterialTable from 'material-table';
import moment from 'moment';

const GorevlerPanel = props => {
  const dispatch = useDispatch();
  const gorevStats = useSelector(state => state.experto.gorevStats);
  const gorevler = useSelector(state => state.experto.gorevler);
  const me = useSelector(state => state.graph.me);
  const yoneticisiOldugumDepartmanlar = useSelector(
    state => state.experto.yoneticisiOldugumDepartmanlar
  );
  const personeller = useSelector(state => state.graph.personeller);

  const [acikGorevler, setAcikGorevler] = useState(0);
  const [gecikenGorevler, setGecikenGorevler] = useState(0);
  const [ekipAcikGorevler, setEkipAcikGorevler] = useState(0);
  const [ekipGecikenGorevler, setEkipGecikenGorevler] = useState(0);

  const [sonAcik10Gorevim, setSonAcik10Gorevim] = useState([]);
  const [sonAcik10EkibiminGorevi, setSonAcik10EkibiminGorevi] = useState([]);

  const options = {
    chart: {
      height: 150,
      type: 'area'
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth'
    },
    xaxis: {
      type: 'datetime',
      categories: [
        '2018-09-19T00:00:00.000Z',
        '2018-09-19T01:30:00.000Z',
        '2018-09-19T02:30:00.000Z',
        '2018-09-19T03:30:00.000Z',
        '2018-09-19T04:30:00.000Z',
        '2018-09-19T05:30:00.000Z',
        '2018-09-19T06:30:00.000Z'
      ]
    },
    tooltip: {
      x: {
        format: 'dd/MM/yy HH:mm'
      }
    }
  };
  const series = [
    {
      name: 'series-1',
      data: [30, 40, 25, 50, 49, 21, 70, 51]
    },
    {
      name: 'series-2',
      data: [23, 12, 54, 61, 32, 56, 81, 19]
    }
  ];

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorevler());
      //dispatch(Actions.getGorevStats());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (gorevler) {
      setAcikGorevler(
        gorevler.filter(g => g.atananMsId === me.id && g.sonuc === 'acik')
          .length
      );
      setGecikenGorevler(
        gorevler.filter(
          g =>
            g.atananMsId === me.id &&
            g.sonuc === 'acik' &&
            new Date(g.sonTarih).setHours(0, 0, 0, 0) <
              new Date().setHours(0, 0, 0, 0)
        ).length
      );
      setEkipAcikGorevler(
        gorevler.filter(g => g.atananMsId !== me.id && g.sonuc === 'acik')
          .length
      );
      setEkipGecikenGorevler(
        gorevler.filter(
          g =>
            g.atananMsId !== me.id &&
            g.sonuc === 'acik' &&
            new Date(g.sonTarih).setHours(0, 0, 0, 0) <
              new Date().setHours(0, 0, 0, 0)
        ).length
      );

      let _gorevler = gorevler.filter(
        g => g.atananMsId === me.id && g.sonuc === 'acik'
      );
      let sortedGorevler = _gorevler.sort(
        (a, b) => new Date(a.sonTarih) - new Date(b.sonTarih)
      );
      setSonAcik10Gorevim(sortedGorevler.slice(0, 10));

      let egorevler = gorevler.filter(
        g => g.atananMsId !== me.id && g.sonuc === 'acik'
      );
      let sortedEGorevler = egorevler.sort(
        (a, b) => new Date(a.sonTarih) - new Date(b.sonTarih)
      );
      setSonAcik10EkibiminGorevi(sortedEGorevler.slice(0, 10));
    }
  }, [gorevler]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <div className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
            {gorevler ? (
              <Fragment>
                <div>
                  <div className="display-3 text-success">{acikGorevler}</div>
                  <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-success" />
                  <div className="font-weight-light font-size-lg">
                    Açık Görevlerim
                  </div>
                </div>
                <div className="ml-auto card-hover-indicator align-self-center">
                  <IconButton
                    onClick={e => {
                      e.preventDefault();
                      props.history.push(`/gorevler?t=acik`);
                    }}>
                    <FontAwesomeIcon
                      icon={['fas', 'chevron-right']}
                      className="font-size-xl"
                    />
                  </IconButton>
                </div>
              </Fragment>
            ) : (
              <PulseLoader color={'var(--primary)'} loading={true} />
            )}
          </div>
        </Grid>
        <Grid item xs={3}>
          <div className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
            {gorevler ? (
              <Fragment>
                <div>
                  <div className="display-3 text-danger">{gecikenGorevler}</div>
                  <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-danger" />
                  <div className="font-weight-light font-size-lg">
                    Geciken Görevlerim
                  </div>
                </div>
                <div className="ml-auto card-hover-indicator align-self-center">
                  <IconButton
                    onClick={e => {
                      e.preventDefault();
                      props.history.push(`/gorevler?t=geciken`);
                    }}>
                    <FontAwesomeIcon
                      icon={['fas', 'chevron-right']}
                      className="font-size-xl"
                    />
                  </IconButton>
                </div>
              </Fragment>
            ) : (
              <PulseLoader color={'var(--primary)'} loading={true} />
            )}
          </div>
        </Grid>
        <Grid item xs={3}>
          {yoneticisiOldugumDepartmanlar ? (
            <div className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
              <div>
                <div className="display-3  text-info">{ekipAcikGorevler}</div>
                <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-info" />
                <div className="font-weight-light font-size-lg">
                  Ekibimin Açık Görevleri
                </div>
              </div>
              <div className="ml-auto card-hover-indicator align-self-center">
                <IconButton
                  onClick={e => {
                    e.preventDefault();
                    props.history.push(`/gorevler?t=ekipacik`);
                  }}>
                  <FontAwesomeIcon
                    icon={['fas', 'chevron-right']}
                    className="font-size-xl"
                  />
                </IconButton>
              </div>
            </div>
          ) : (
            <Fragment></Fragment>
          )}
        </Grid>
        <Grid item xs={3}>
          {yoneticisiOldugumDepartmanlar ? (
            <div className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
              <div>
                <div className="display-3 text-warning">
                  {ekipGecikenGorevler}
                </div>
                <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-warning" />
                <div className="font-weight-light font-size-lg ">
                  Ekibimin Geciken Görevleri
                </div>
              </div>
              <div className="ml-auto card-hover-indicator align-self-center">
                <IconButton
                  onClick={e => {
                    e.preventDefault();
                    props.history.push(`/gorevler?t=ekipgeciken`);
                  }}>
                  <FontAwesomeIcon
                    icon={['fas', 'chevron-right']}
                    className="font-size-xl"
                  />
                </IconButton>
              </div>
            </div>
          ) : (
            <Fragment></Fragment>
          )}
        </Grid>
        <Grid item xs={12}>
          <Chart options={options} series={series} type="area" height={400} />
        </Grid>
        <Grid item xs={6}>
          {personeller && (
            <MaterialTable
              title="Son Açık 10 Görevim"
              data={sonAcik10Gorevim}
              columns={[
                { title: 'Görev', field: 'baslik' },
                {
                  title: 'Son Tarih',
                  customSort: (a, b) => {
                    return moment(a.sonTarih) - moment(b.sonTarih);
                  },
                  render: rowData =>
                    moment(rowData.sonTarih).format('DD.MM.yyyy')
                },
                { title: 'Müşteri', field: 'musteri.marka' },
                {
                  title: 'Atama',
                  sorting: false,
                  render: rowData => {
                    let p = personeller.find(p => p.id == rowData.atananMsId);
                    if (!p) {
                      return <></>;
                    }
                    return (
                      <Tooltip title={p.displayName}>
                        <Avatar src={p.photo} />
                      </Tooltip>
                    );
                  }
                },
                //{ title: 'Öncelik', field: 'oncelik' },
                /*{
                  title: 'Durum',
                  render: rowData => {
                    if (rowData.sonuc === 'acik') {
                      //const today = new Date();
                      //const sonTarih = new Date(rowData.sonTarih);

                      //if (today > sonTarih) {
                      //  return (
                      //    <div className="h-auto py-0 px-3 badge badge-danger">
                      //      Gecikmiş
                      //    </div>
                      //  );
                      //} else {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-info">
                            Açık
                          </div>
                        );
                      //}
                    } else if (rowData.sonuc === 'basarili') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-success">
                          Başarılı
                        </div>
                      );
                    } else if (rowData.sonuc === 'basarisiz') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-first">
                          Başarısız
                        </div>
                      );
                    } else if (rowData.sonuc === 'iptal') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-warning">
                          İptal
                        </div>
                      );
                    }
                    return <div></div>;
                  }
                },*/
                {
                  title: 'Gecikme',
                  render: rowData => {
                    if (rowData.sonuc === 'acik') {
                      const today = new Date();
                      const sonTarih = new Date(rowData.sonTarih);
                      today.setHours(0, 0, 0, 0);
                      sonTarih.setHours(0, 0, 0, 0);

                      if (today > sonTarih) {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-danger">
                            Gecikmiş
                          </div>
                        );
                      } else {
                        let kacGun = moment(sonTarih).diff(today, 'days');
                        return (
                          <>{kacGun === 0 ? 'Bugün son' : `${kacGun} gün`}</>
                        );
                      }
                    }
                  }
                }
              ]}
              actions={[
                {
                  icon: 'edit',
                  tooltip: 'Detay',
                  onClick: (event, rowData) =>
                    props.history.push(`/gorev/${rowData._id}`) //alert('You saved ' + rowData.name)
                }
              ]}
              options={{
                pageSizeOptions: [5, 10, 20, 50, 100],
                pageSize: 20,
                actionsColumnIndex: -1
              }}
            />
          )}
        </Grid>
        <Grid item xs={6}>
          {personeller && (
            <MaterialTable
              title="Ekibimin Son Açık 10 Görevi"
              data={sonAcik10EkibiminGorevi}
              columns={[
                { title: 'Görev', field: 'baslik' },
                {
                  title: 'Son Tarih',
                  customSort: (a, b) => {
                    return moment(a.sonTarih) - moment(b.sonTarih);
                  },
                  render: rowData =>
                    moment(rowData.sonTarih).format('DD.MM.yyyy')
                },
                { title: 'Müşteri', field: 'musteri.marka' },
                {
                  title: 'Atama',
                  sorting: false,
                  render: rowData => {
                    let p = personeller.find(p => p.id == rowData.atananMsId);
                    if (!p) {
                      return <></>;
                    }
                    return (
                      <Tooltip title={p.displayName}>
                        <Avatar src={p.photo} />
                      </Tooltip>
                    );
                  }
                },
                /*
                { title: 'Öncelik', field: 'oncelik' },
                {
                  title: 'Durum',
                  render: rowData => {
                    if (rowData.sonuc === 'acik') {
                      const today = new Date();
                      const sonTarih = new Date(rowData.sonTarih);

                      if (today > sonTarih) {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-danger">
                            Gecikmiş
                          </div>
                        );
                      } else {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-info">
                            Açık
                          </div>
                        );
                      }
                    } else if (rowData.sonuc === 'basarili') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-success">
                          Başarılı
                        </div>
                      );
                    } else if (rowData.sonuc === 'basarisiz') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-first">
                          Başarısız
                        </div>
                      );
                    } else if (rowData.sonuc === 'iptal') {
                      return (
                        <div className="h-auto py-0 px-3 badge badge-warning">
                          İptal
                        </div>
                      );
                    }
                    return <div></div>;
                  }
                }
                */
                {
                  title: 'Gecikme',
                  render: rowData => {
                    if (rowData.sonuc === 'acik') {
                      const today = new Date();
                      const sonTarih = new Date(rowData.sonTarih);
                      today.setHours(0, 0, 0, 0);
                      sonTarih.setHours(0, 0, 0, 0);

                      if (today > sonTarih) {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-danger">
                            Gecikmiş
                          </div>
                        );
                      } else {
                        let kacGun = moment(sonTarih).diff(today, 'days');
                        return (
                          <>{kacGun === 0 ? 'Bugün son' : `${kacGun} gün`}</>
                        );
                      }
                    }
                  }
                }
              ]}
              actions={[
                {
                  icon: 'edit',
                  tooltip: 'Detay',
                  onClick: (event, rowData) =>
                    props.history.push(`/gorev/${rowData._id}`) //alert('You saved ' + rowData.name)
                }
              ]}
              options={{
                pageSizeOptions: [5, 10, 20, 50, 100],
                pageSize: 20,
                actionsColumnIndex: -1
              }}
            />
          )}
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(GorevlerPanel);

/*
https://apexcharts.com/react-chart-demos/column-charts/stacked/
*/
