import React, { Fragment, useEffect } from 'react';
import { Grid, Paper } from '@material-ui/core';
import GorevList from 'components/gorev/GorevList';
import {withRouter} from 'react-router'
import ToplantiTakvim from 'components/toplanti/ToplantiTakvim';
import { useDispatch, useSelector } from 'react-redux';
import { Loader } from 'react-feather';
import * as Actions from 'store/actions'

const TakvimPanel = props => {
  const dispatch = useDispatch()
  const me = useSelector(state => state.graph.me)

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorevler({atananMsId: me.id}))
      dispatch(Actions.getToplantilar({expertoKatilimcilar: me.id}))
    }
    load()
  }, [dispatch])

  return (
    <Fragment>
      <Grid container spacing={3}>
          <Grid item xs={12}>
            <ToplantiTakvim />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(TakvimPanel);
