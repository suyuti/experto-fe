import React, { Fragment } from 'react';
import { Grid, Paper } from '@material-ui/core';
import GorevList from 'components/gorev/GorevList';
import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Chart from 'react-apexcharts';

const ToplantilarPanel = props => {
  const options = {
      chart: {
        type: 'bar',
        height: 350,
        toolbar: {show:false},
        stacked: true,
        toolbar: {
          show: true
        },
        zoom: {
          enabled: true
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom',
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      xaxis: {
        type: 'datetime',
        categories: [
          '01/01/2011 GMT',
          '01/02/2011 GMT',
          '01/03/2011 GMT',
          '01/04/2011 GMT',
          '01/05/2011 GMT',
          '01/06/2011 GMT'
        ]
      },
      legend: {
        position: 'right',
        offsetY: 40
      },
      fill: {
        opacity: 1
      }
  };
  const series = [{
    name: 'Mehmet',
    data: [44, 55, 41, 67, 22, 43]
  }, {
    name: 'PRODUCT B',
    data: [13, 23, 20, 8, 13, 27]
  }, {
    name: 'PRODUCT C',
    data: [11, 17, 15, 15, 21, 14]
  }, {
    name: 'PRODUCT D',
    data: [21, 7, 25, 13, 22, 8]
  }]

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
            <div>
              <div className="display-3 text-success">56</div>
              <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-success" />
              <div className="font-weight-light font-size-lg">
                Gelecek Toplantılar
              </div>
            </div>
            <div className="ml-auto card-hover-indicator align-self-center">
              <FontAwesomeIcon
                icon={['fas', 'chevron-right']}
                className="font-size-xl"
              />
            </div>
          </a>
        </Grid>
        <Grid item xs={3}>
          <a
            href="#/"
            onClick={e => e.preventDefault()}
            className="card card-box card-box-hover-rise card-box-hover text-black align-box-row align-items-start mb-4 p-4">
            <div>
              <div className="display-3 text-danger">56</div>
              <div className="divider mt-2 mb-3 border-2 w-25 bg-first rounded border-danger" />
              <div className="font-weight-light font-size-lg">
                Ekibimin Gelecek Toplantıları
              </div>
            </div>
            <div className="ml-auto card-hover-indicator align-self-center">
              <FontAwesomeIcon
                icon={['fas', 'chevron-right']}
                className="font-size-xl"
              />
            </div>
          </a>
        </Grid>
        <Grid item xs={3}></Grid>
        <Grid item xs={3}></Grid>
        <Grid item xs={6}>
          <Chart options={options} series={series} type="bar" stacked height={400} />
        </Grid>
        <Grid item xs={6}>
          <Chart options={options} series={series} type="donut" height={250} />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(ToplantilarPanel);

/*
https://apexcharts.com/react-chart-demos/column-charts/stacked/
*/
