import React, { Fragment, useEffect } from 'react';
import {
  Grid,
  Card,
  CardHeader,
  Typography,
  Divider,
  CardContent,
  Tooltip,
  IconButton,
  makeStyles
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { withRouter } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { PulseLoader } from 'react-spinners';
import * as Actions from '../../store/actions';
import { useStyles } from '@material-ui/pickers/views/Calendar/SlideTransition';

const styles = makeStyles(theme => ({
  infoCard: {}
}));

const BildirimPanel = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const gorevStats = useSelector(state => state.experto.gorevStats);
  const toplantiStats = useSelector(
    state => state.expertoToplanti.toplantiStats
  );

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorevStats());
      dispatch(Actions.getToplantiStats());
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Card className="card-box card-shadow-first  mb-4">
            <CardHeader
              title={<Typography variant="h4">Görevlerim</Typography>}
              avatar={<FormatListBulletedIcon />}
              action={
                <Tooltip title="Görevlere git">
                  <IconButton
                    onClick={() => props.history.push('/gorevler?t=acik')}>
                    <ChevronRightIcon />
                  </IconButton>
                </Tooltip>
              }
            />
            <CardContent>
              <div className="d-flex justify-content-around">
                {!gorevStats ? (
                  <PulseLoader color={'var(--primary)'} loading={true} />
                ) : (
                  <Fragment>
                    <div className="text-center">
                      <Tooltip title="Bana atanmış açık görevler">
                        <div className="display-2">{gorevStats.acik}</div>
                      </Tooltip>
                      <div className="display-5 text-second text-w">Açık</div>
                    </div>
                    <div className="text-center">
                      <Tooltip title="Benim oluşturduğum görevler">
                        <div className="display-2">
                          {gorevStats.olusturduklarim}
                        </div>
                      </Tooltip>
                      <div>Takip</div>
                    </div>
                    {gorevStats.ekipAcikIsler !== undefined}{' '}
                    {
                      <div className="text-center">
                        <Tooltip title="Ekibimdeki açık görevler">
                          <div className="display-2">
                            {gorevStats.ekipAcikIsler}
                          </div>
                        </Tooltip>
                        <div>Ekip Açık</div>
                      </div>
                    }
                  </Fragment>
                )}
              </div>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={3}>
          <Card className="card-box card-shadow-first  mb-4">
            <CardHeader
              title={<Typography variant="h4">Toplantılarım</Typography>}
              avatar={<InsertInvitationIcon />}
              action={
                <Tooltip title="Toplantılara git">
                  <IconButton
                    onClick={() => props.history.push('/toplantilar')}>
                    <ChevronRightIcon />
                  </IconButton>
                </Tooltip>
              }
            />
            <CardContent>
              <div className="display-2 text-center line-height-sm text-second text-center d-flex align-items-center pt-3 justify-content-center">
                <div>
                  {toplantiStats ? (
                    toplantiStats.acik
                  ) : (
                    <PulseLoader color={'var(--primary)'} loading={true} />
                  )}
                </div>
              </div>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={3}>
          <Card className="card-box card-shadow-first  mb-4">
            <CardHeader
              title={<Typography variant="h4">Satış Hedefi</Typography>}
              avatar={<InsertInvitationIcon />}
              action={
                <IconButton>
                  <MoreVertIcon />
                </IconButton>
              }
            />
            <CardContent>
              <div className="display-2 text-center line-height-sm text-second text-center d-flex align-items-center pt-3 justify-content-center">
                <div>.</div>
              </div>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={3}>
          <Card className="card-box card-shadow-first  mb-4">
            <CardHeader
              title={<Typography variant="h4">Görüşme Hedefi</Typography>}
              avatar={<InsertInvitationIcon />}
              action={
                <IconButton>
                  <MoreVertIcon />
                </IconButton>
              }
            />
            <CardContent>
              <div className="display-2 text-center line-height-sm text-second text-center d-flex align-items-center pt-3 justify-content-center">
                <div>.</div>
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(BildirimPanel);
