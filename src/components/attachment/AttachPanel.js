import React, { Fragment, useState, useRef } from 'react';
import {
  CardHeader,
  Divider,
  CardContent,
  Card,
  Typography,
  IconButton,
  makeStyles,
  ListItem,
  Grid,
  Box,
  List,
  LinearProgress,
  Tooltip
} from '@material-ui/core';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import AddIcon from '@material-ui/icons/Add';
import Dropzone from 'react-dropzone';
import GetAppIcon from '@material-ui/icons/GetApp';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import ClearIcon from '@material-ui/icons/Clear';
import PublishIcon from '@material-ui/icons/Publish';
import UploadService from 'services/fileupload.service';
import DoneIcon from '@material-ui/icons/Done';
import { useDispatch } from 'react-redux';
import * as Actions from 'store/actions'
import {saveAs} from 'file-saver'
//var FileSaver = require('file-saver');

const useStyles = makeStyles(theme => ({
  fileItem: {
    borderColor: 'black',
    margin: '5px'
  }
}));

const AttachmentItem = props => {
  const {
    file,
    yeni,
    uploading,
    uploaded,
    onProgress,
    onRemove,
    onDownload,
    onDelete
  } = props;

  var icon = 'file-alt';
  if (file) {
    if (file.endsWith('.png')) {
      icon = 'file-image';
    } else if (file.endsWith('.doc')) {
      icon = 'file-word';
    } else if (file.endsWith('.docx')) {
      icon = 'file-word';
    } else if (file.endsWith('.xls')) {
      icon = 'file-excel';
    } else if (file.endsWith('.xlsx')) {
      icon = 'file-excel';
    } else if (file.endsWith('.pdf')) {
      icon = 'file-pdf';
    }
  }

  //debugger;
  return (
    <Fragment>
      <ListItem key={file} className="d-flex justify-content-between align-items-center">
        <div className="d-flex align-items-center">
          <FontAwesomeIcon icon={['far', icon]} className="font-size-xxl" />
          <div className="ml-4">{file}</div>
        </div>
        <span>
          {yeni && !uploaded && !uploading && (
            <IconButton onClick={onRemove}>
              <ClearIcon />
            </IconButton>
          )}
          {yeni && !uploaded && uploading && <LinearProgress />}
          {!yeni && !uploaded && !uploading && (
            <>
              <IconButton onClick={onDownload} >
                <GetAppIcon />
              </IconButton>
              <IconButton onClick={onDelete}>
                <DeleteOutlineIcon />
              </IconButton>
            </>
          )}
          {yeni && uploaded && !uploading && <DoneIcon />}
        </span>
      </ListItem>
    </Fragment>
  );
};

const AttachPanel = props => {
  const { onAttached, onFileUploaded, files } = props;
  const classes = useStyles();
  const [newFiles, setNewFiles] = useState([]);
  const [existFiles, setExistFiles] = useState(files);
  const [uploadMode, setUploadMode] = useState(false);
  const [uploading, setUploading] = useState(false);
  const [progressVal, setProgressVal] = useState(0);
  const [progressMax, setProgressMax] = useState(0);
  const [showProgress, setShowProgress] = useState(false)
  const dispatch = useDispatch()

  const uploadFiles = async (files) => {
    var _files = [...files];
    
    for (var file of _files) {
      await UploadService.upload(file.file, e => {
        setProgressVal(Math.round((100 * e.loaded) / e.total));
        debugger
        if (e.loaded === e.total) {
          file.uploaded = true;
          file.uploading = false;
        } else {
          file.uploaded = false;
          file.uploading = true;
        }
      })
      .then(e => {
        if (!e.err) {
          onFileUploaded({fileId: e.data, yeni: true});
        }
      });
    }
    setNewFiles(_files);
    setShowProgress(false)
  };

  //console.log(newFiles);
  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Ekler"
          avatar={<AttachFileIcon />}
          action={
            <>
              {newFiles.length > 0 && (
                <Tooltip title="Dosyaları gönder">
                  <IconButton
                    disabled={uploading}
                    onClick={() => {
                      //uploadFiles();
                    }}>
                    <PublishIcon />
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip title="Yeni dosya ekle">
                <IconButton onClick={() => setUploadMode(true)}>
                  <AddIcon />
                </IconButton>
              </Tooltip>
            </>
          }
        />
        <Divider />
        <CardContent>
          <>
          {showProgress && 
                      <LinearProgress variant="determinate" value={progressVal} />
                    }
            {uploadMode ? (
              <Dropzone
                onDrop={files => {
                  setShowProgress(true)
                  var _files = [];
                  var total = 0;
                  for (var file of files) {
                    _files.push({
                      file: file,
                      uploaded: false,
                      yeni: true,
                      uploading: false
                    });
                    total += file.size;
                  }
                  setProgressMax(total);
                  setNewFiles(_files);
                  setUploadMode(false);
                  uploadFiles(_files)
                  //onAttached(files);
                }}
                onFileDialogCancel={() => setUploadMode(false)}>
                {({ getRootProps, getInputProps }) => (
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <div className="dz-message">
                      <div className="dx-text">
                        Yüklemek istediğiniz dosyaları buraya sürükleyin. Veya
                        tıklayarak seçin.
                      </div>
                    </div>
                  </div>
                )}
              </Dropzone>
            ) : (
              <List className="py-0">
                {existFiles.map(f => {
                  if (!f.yeni) {
                    return <AttachmentItem file={f.dosyaAdi} yeni={false} onDownload={() => {
                      UploadService.download(f._id).then(resp => {
                        debugger
                        saveAs(resp, f.dosyaAdi)
                      })
                      //dispatch(Actions.)
                      //alert(f.dosyaAdi)
                    }} />;
                  }
                })}
                {newFiles.map(f => {
                  return (
                    <AttachmentItem
                      file={f.file.path}
                      yeni={f.yeni}
                      uploaded={f.uploaded}
                      uploading={f.uploading}
                      onRemove={() => {
                        setNewFiles(
                          newFiles.filter(
                            file => file.file.path !== f.file.path
                          )
                        );
                      }}
                    />
                  );
                })}
              </List>
            )}
          </>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default AttachPanel;
