//import moment from 'moment'
const moment = require('moment');

exports.validate = kisi => {
  var error = null;
  if (kisi.adi === null || kisi.adi.trim().length === 0) {
    error = error || {};
    error.adi = 'Ad yazmalısınız';
  }
  if (kisi.soyadi === null || kisi.soyadi.trim().length === 0) {
    error = error || {};
    error.soyadi = 'Soyad yazmalısınız';
  }
  if (!kisi.musteri) {
    error = error || {};
    error.musteri = 'Firma seçmelisiniz';
  }

  return error;
};
