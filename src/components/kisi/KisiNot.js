import React, { Fragment, useEffect, useState } from 'react';
import {
  Card,
  Divider,
  CardContent,
  CardHeader,
  Paper,
  IconButton,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions'
import NoteIcon from '@material-ui/icons/Note';
import { withRouter } from 'react-router';

const KisiNot = props => {
  const dispatch = useDispatch();
  const kisi = useSelector(state => state.kisi.kisi);
  const notlar = useSelector(state => state.kisi.notlar);
  const [showDlg, setShowDlg] = useState(false);
  const [not, setNot] = useState({});

  useEffect(() => {
    const load = async () => {
        dispatch(Actions.getKisiNotlar(props.match.params.id))
    }
    load()
  }, [dispatch])

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Notlar"
          avatar={<NoteIcon />}
          action={
            <div className="d-flex flex-row">
              <IconButton onClick={() => {setShowDlg(true)}}>
                <AddIcon />
              </IconButton>
            </div>
          }
        />
        <Divider />
        <CardContent>
          {notlar &&
            notlar.map(n => {
              return <Paper className="p-4">{n.not}</Paper>;
            })}
        </CardContent>
      </Card>
      {showDlg && (
        <Dialog open={showDlg}
          fullWidth
          maxWidth='lg'
        >
          <DialogTitle>Yeni Not</DialogTitle>
          <Divider />
          <DialogContent className="p-4">
            <Grid container spacing={3}>
              <Grid item xs={12} className='m-2'>
                <TextField
                  label="Not"
                  fullWidth
                  multiline
                  autoFocus
                  variant='outlined'
                  rows={8}
                  value={not.not}
                  onChange={e => {
                    var n = {...not}
                    n.not = e.target.value
                    setNot(n)
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <Divider />
          <DialogActions>
            <Button onClick={() => setShowDlg(false)} color="primary">
              İptal
            </Button>
            <Button
              onClick={() => {
                dispatch(Actions.addKisiNot(kisi._id, not)); // tODO
                setShowDlg(false);
              }}
              color="primary">
              Tamam
            </Button>
          </DialogActions>{' '}
        </Dialog>
      )}
    </Fragment>
  );
};

export default withRouter(KisiNot)
