import React, { Fragment, useState, useEffect } from 'react';
import {
  Card,
  CardHeader,
  Divider,
  LinearProgress,
  IconButton,
  CardContent,
  DialogTitle,
  AppBar,
  Button,
  Typography,
  Toolbar,
  Grid,
  Dialog,
  DialogContent,
  DialogActions
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AddIcon from '@material-ui/icons/Add';
import MaterialTable from 'material-table';
import GorusmeForm from 'components/gorusme/GorusmeForm';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from 'store/actions'
import { withRouter } from 'react-router';

const KisiGorusmeler = props => {
  const dispatch = useDispatch()
  const [showDlg, setShowDlg] = useState(false);
  const kisi = useSelector(state => state.kisi.kisi)
  const gorusmeler = useSelector(state => state.gorusme.gorusmeler)

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorusmeler({kisi: kisi._id}))
    }
    load()
  }, [dispatch])


  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Görüşmeler"
          subheader=""
          avatar={<FontAwesomeIcon icon={['fas', 'phone-alt']} size="2x" />}
          action={
            <IconButton onClick={() => props.history.push(`/gorusme/yeni?k=${kisi._id}&f=${kisi.musteri._id}`)}>
              <AddIcon />
            </IconButton>
          }
        />
        <Divider />
        <CardContent>
          <MaterialTable 
          title='Kişi ile yapılan görüşmeler'
            data={gorusmeler || []} 
            columns={[
              {title: 'Konu', field: 'konu'},
              {title: 'Tarih', field: 'tarih'},
              {title: 'Kanal', field: 'kanal'},
              {title: 'Görüşmeyi yapan', field: 'gorusmeYapan'}
            ]}
            />
        </CardContent>
      </Card>
      {showDlg && (
        <Dialog open={showDlg} maxWidth='md'>
          <DialogTitle>
            <AppBar position='absolute' color='primary'>
              <Toolbar >
                <Typography variant='h4' color='inherit'>Görüşme Kaydı Oluşturma</Typography>
              </Toolbar>
            </AppBar>
          </DialogTitle>
          <DialogContent className='p-4'>
            <GorusmeForm source='kisi' kisi={kisi} firmaId=''/>
          </DialogContent>
          <Divider />
          <DialogActions>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowDlg(false);
              }}>
              Kapat
            </Button>
            <Button variant="contained" color="primary" onClick={() => {}}>
              Kaydet
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </Fragment>
  );
};

export default withRouter(KisiGorusmeler);
