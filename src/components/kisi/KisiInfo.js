import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import IOSSwitch from '../IosSwitch';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import {
  Grid,
  TextField,
  Card,
  CardHeader,
  Divider,
  Tooltip,
  CardContent,
  IconButton,
  FormKontrol,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  FormControl,
  Avatar,
  Menu
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
//import IOSSwitch from 'components/IOSSwitch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import CloseIcon from '@material-ui/icons/Close';
import PersonelCard from 'components/personel/PersonelCard';
import PersonelList from 'components/personel/PersonelList';
import MaskedInput from 'react-text-mask';
import PhoneInput from 'components/PhoneInput';
import { withRouter } from 'react-router';

const qs = require('query-string');

const KisiInfo = props => {
  const dispatch = useDispatch();
  const kisi = useSelector(state => state.kisi.kisi);
  const musteriler = useSelector(state => state.experto.musteriler);
  const yetkiler = useSelector(state => state.experto.yetkiler);
  const error = useSelector(state => state.app.error);
  const [musteriSecili, setMusteriSecili] = useState(null);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getMusteriler());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (musteriler) {
      const params = qs.parse(props.location.search);
      if (params.musteri) {
        let m = musteriler.find(m => m._id === params.musteri)
        if (m) {
          setMusteriSecili(m);
          dispatch(Actions.setKisi({ musteri: musteriSecili }));
        }
      }
    }
  }, musteriler);

  //if (!kisi) {
  //  return <></>;
  //}

  if (!musteriler) {
    return <></>;
  }

 
  console.log(kisi)
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Genel Bilgiler"
              avatar={<AccountBalanceIcon />}
              action={
                <div className="d-flex flex-row">
                  <IconButton>
                    <MoreVertIcon />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <IOSSwitch
                        checked={kisi.aktif}
                        onChange={e => {
                          let checked = e.target.checked;
                          var k = { ...kisi };
                          k.aktif = checked;
                          dispatch(Actions.setKisi(k));
                        }}
                        name="checkedB"
                      />
                    }
                    label={kisi.aktif ? 'Aktif' : 'Pasif'}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Autocomplete
                    disabled={musteriSecili !== null}
                    options={musteriler}
                    getOptionLabel={option => option.marka}
                    value={kisi.musteri}
                    InputProps={
                      {
                        //readOnly: gorev.createdByMsId !== me.id
                      }
                    }
                    onChange={(e, v, r) => {
                      if (v) {
                        dispatch(Actions.setKisi({ musteri: v }));
                      }
                    }}
                    renderInput={params => (
                      <TextField
                        error={error && error.musteri}
                        helperText={error && error.musteri}
                        {...params}
                        label="Firma"
                        variant="outlined"
                        fullWidth
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    error={error && error.adi}
                    helperText={error && error.adi}
                    label="Adı"
                    fullWidth
                    variant="outlined"
                    //InputProps={{ readOnly: !editMode }}
                    value={kisi.adi}
                    onChange={e => {
                      var k = { ...kisi };
                      k.adi = e.target.value;
                      dispatch(Actions.setKisi(k));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    error={error && error.soyadi}
                    helperText={error && error.soyadi}
                    label="Soyadı"
                    fullWidth
                    variant="outlined"
                    value={kisi.soyadi}
                    onChange={e => {
                      var k = { ...kisi };
                      k.soyadi = e.target.value;
                      dispatch(Actions.setKisi(k));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Ünvanı"
                    fullWidth
                    variant="outlined"
                    value={kisi.unvani}
                    onChange={e => {
                      var k = { ...kisi };
                      k.unvani = e.target.value;
                      dispatch(Actions.setKisi(k));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Departmanı"
                    fullWidth
                    variant="outlined"
                    value={kisi.departman}
                    onChange={e => {
                      dispatch(Actions.setKisi({ departman: e.target.value }));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Cinsiyet</FormLabel>
                    <RadioGroup row
                      aria-label="gender"
                      name="gender1"
                      value={kisi.cinsiyet }
                      onChange={(e) => dispatch(Actions.setKisi({cinsiyet: e.target.value}))}
                    >
                      <FormControlLabel
                        value="kadin"
                        control={<Radio />}
                        label="Kadın"
                      />
                      <FormControlLabel
                        value="erkek"
                        control={<Radio />}
                        label="Erkek"
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                      error={error && error.tarih}
                      helperText={error && error.tarih}
                      autoOk
                      fullWidth
                      variant="inline"
                      inputVariant="outlined"
                      label="Doğum Tarihi"
                      format="dd/MM/yyyy"
                      InputAdornmentProps={{ position: 'start' }}
                      value={kisi.dogumTarihi}
                      onChange={e => {
                        dispatch(Actions.setKisi({ dogumTarihi: e }));
                      }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="Mail"
                    fullWidth
                    variant="outlined"
                    value={kisi.mail}
                    onChange={e => {
                      dispatch(Actions.setKisi({ mail: e.target.value }));
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    variant="outlined"
                    label="Cep Telefonu"
                    value={kisi.cepTelefon}
                    onChange={e => {
                      dispatch(Actions.setKisi({ cepTelefon: e.target.value }));
                    }}
                    InputProps={{
                      inputComponent: PhoneInput
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    variant="outlined"
                    label="İş Telefonu"
                    value={kisi.isTelefon}
                    onChange={e => {
                      dispatch(Actions.setKisi({ isTelefon: e.target.value }));
                    }}
                    //id="formatted-numberformat-input"
                    InputProps={{
                      inputComponent: PhoneInput
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="TCKN"
                    fullWidth
                    variant="outlined"
                    value={kisi.tckn}
                    onChange={e => {
                      dispatch(Actions.setKisi({ tckn: e.target.value }));
                    }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(KisiInfo);
