import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Grid,
  TextField,
  Card,
  CardHeader,
  Divider,
  Tooltip,
  CardContent,
  IconButton,
  Avatar,
  Menu
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
//import IOSSwitch from 'components/IOSSwitch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import CloseIcon from '@material-ui/icons/Close';
import PersonelCard from 'components/personel/PersonelCard';
import PersonelList from 'components/personel/PersonelList';

const UrunInfo = props => {
  const dispatch = useDispatch();
  const urun = useSelector(state => state.urun.urun);

  useEffect(() => {
    const load = async () => {
      //dispatch(Actions.getUrun());
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Ürün Bilgileri"
              avatar={<AccountBalanceIcon  />}
              action={
                <div className="d-flex flex-row">
                  <IconButton >
                    <MoreVertIcon  />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    label="Ürün Adı"
                    fullWidth
                    autoFocus
                    variant='outlined'
                    value={urun.adi}
                    onChange={e => {
                      var u = { ...urun };
                      u.adi = e.target.value;
                      dispatch(Actions.setUrun(u));
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="Açıklama"
                    fullWidth
                    multiline
                    rows={3}
                    variant='outlined'
                    //InputProps={{ readOnly: !editMode }}
                    value={urun.aciklama}
                    onChange={e => {
                      var u = { ...urun };
                      u.aciklama = e.target.value;
                      dispatch(Actions.setUrun(u));
                    }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default UrunInfo;
