import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Grid,
  TextField,
  Card,
  CardHeader,
  Divider,
  Tooltip,
  CardContent,
  IconButton,
  Avatar,
  Menu
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
//import IOSSwitch from 'components/IOSSwitch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import CloseIcon from '@material-ui/icons/Close';
import PersonelCard from 'components/personel/PersonelCard';
import PersonelList from 'components/personel/PersonelList';

const UrunDokumanlar = props => {
  const dispatch = useDispatch();
  const urun = useSelector(state => state.urun.urun);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    const load = async () => {
      //dispatch(Actions.getUrun());
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Ürün Dokümanları"
              avatar={<AccountBalanceIcon fontSize="large" />}
              action={
                <div className="d-flex flex-row">
                  {editMode && (
                    <div className="d-flex flex-row">
                      <Tooltip title="Kaydet">
                        <IconButton>
                          <SaveAltIcon fontSize="large" />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="İptal">
                        <IconButton onClick={() => setEditMode(false)}>
                          <CloseIcon fontSize="large" />
                        </IconButton>
                      </Tooltip>
                    </div>
                  )}
                  <IconButton onClick={() => setEditMode(!editMode)}>
                    <MoreVertIcon fontSize="large" />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default UrunDokumanlar;
