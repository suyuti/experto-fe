import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Actions from '../../store/actions';
import {
  Grid,
  TextField,
  Card,
  CardHeader,
  Divider,
  Tooltip,
  CardContent,
  IconButton,
  Avatar,
  Menu,
  Checkbox
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
//import IOSSwitch from 'components/IOSSwitch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import CloseIcon from '@material-ui/icons/Close';
import UrunFiyat from './UrunFiyat';
import MoneyInput from 'components/MoneyInput'

const UrunFiyatlar = props => {
  const dispatch = useDispatch();
  const urun = useSelector(state => state.urun.urun);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    const load = async () => {
      //dispatch(Actions.getUrun());
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Ürün Fiyatları"
              avatar={<FontAwesomeIcon icon={['fas', 'lira-sign']} size="1x" />}
              action={
                <div className="d-flex flex-row">
                  <IconButton>
                    <MoreVertIcon />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={0}>
                <Grid item xs={12}>
                  <div className="d-flex mb-2">
                    <Checkbox
                      disableRipple
                      checked={urun.onOdemeTutariVar}
                      onChange={e => {
                        var u = { ...urun };
                        u.onOdemeTutariVar = e.target.checked;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                    <TextField
                      fullWidth
                      disabled={!urun.onOdemeTutariVar}
                      variant="outlined"
                      label="Ön Ödeme Tutarı"
                      value={urun.onOdemeTutari}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        inputComponent: MoneyInput
                      }}    
                      onChange={e => {
                        var u = { ...urun };
                        u.onOdemeTutari = e.target.value;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className="d-flex mb-2">
                    <Checkbox
                      disableRipple
                      checked={urun.basariTutariVar}
                      onChange={e => {
                        var u = { ...urun };
                        u.basariTutariVar = e.target.checked;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Başarı Tutarı"
                      value={urun.basariTutari}
                      disabled={!urun.basariTutariVar}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        inputComponent: MoneyInput
                      }}    
                      onChange={e => {
                        var u = { ...urun };
                        u.basariTutari = e.target.value;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className="d-flex mb-2">
                    <Checkbox
                      disableRipple
                      checked={urun.sabitTutarVar}
                      onChange={e => {
                        var u = { ...urun };
                        u.sabitTutarVar = e.target.checked;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Sabit Tutarı"
                      value={urun.sabitTutar}
                      disabled={!urun.sabitTutarVar}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        inputComponent: MoneyInput
                      }}    
                      onChange={e => {
                        var u = { ...urun };
                        u.sabitTutar = e.target.value;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className="d-flex mb-2">
                    <Checkbox
                      disableRipple
                      checked={urun.raporBasiOdemeTutariVar}
                      onChange={e => {
                        var u = { ...urun };
                        u.raporBasiOdemeTutariVar = e.target.checked;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Rapor Başı Ödeme Tutarı"
                      value={urun.raporBasiOdemeTutari}
                      disabled={!urun.raporBasiOdemeTutariVar}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        inputComponent: MoneyInput
                      }}    
                      onChange={e => {
                        var u = { ...urun };
                        u.raporBasiOdemeTutari = e.target.value;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                  </div>
                </Grid>
                <Grid item xs={12}>
                  <div className="d-flex mb-2">
                    <Checkbox
                      disableRipple
                      checked={urun.yuzdeTutariVar}
                      onChange={e => {
                        var u = { ...urun };
                        u.yuzdeTutariVar = e.target.checked;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                    <TextField
                      fullWidth
                      variant="outlined"
                      label="Yüzde"
                      value={urun.yuzdeTutari}
                      disabled={!urun.yuzdeTutariVar}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        inputComponent: MoneyInput
                      }}    
                      onChange={e => {
                        var u = { ...urun };
                        u.yuzdeTutari = e.target.value;
                        dispatch(Actions.setUrun(u));
                      }}
                    />
                  </div>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default UrunFiyatlar;
