import React, { Fragment, useState, useEffect } from 'react';
import {
  Checkbox,
  TextField,
  InputAdornment,
  Box
} from '@material-ui/core';
//import NumberFormatCustom from '../../utils/NumberFormat';
//import { useSelector } from 'react-redux';

const UrunFiyat = props => {
  const { label, fiyatVar, fiyat } = props;

  return (
    <div style={{ width: '100%' }}>
      <Box display="flex" p={1}>
        <Box p={1}>
          <Checkbox
            className="mb-24"
            checked={fiyatVar}
            //name="onOdemeTutariVar"
            //onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
            //disabled={!editable}
          />
        </Box>
        <Box p={1} flexGrow={1}>
          <TextField
            className="mb-24"
            label={label}
            //id="onOdemeTutari"
            //name="onOdemeTutari"
            value={fiyat}
            //value={urun.onOdemeTutariVar ? urun.onOdemeTutari : ''}
            onChange={e => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                //      handleChange(e);
              }
            }}
            //type="text"
            variant="outlined"
            fullWidth
            //disabled={!urun.onOdemeTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              )
              //readOnly: !editable,
            }}
          />
        </Box>
      </Box>
    </div>
  );
};

export default UrunFiyat