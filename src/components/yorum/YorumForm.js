import React, { Fragment, useState } from 'react'
import {Grid, TextField, Button} from '@material-ui/core'

const YorumForm = props => {
    const [form, setForm] = useState({})
    return <Fragment>
        <Grid container spacing={3}>
            <Grid item xs= {12}>
                <TextField 
                    label='Yorum'
                    fullWidth
                    onChange={(e) => {
                        var f = {...form}
                        f.yorum = e.target.value
                        setForm(f)
                    }}
                />
            </Grid>
        </Grid>
    </Fragment>
}

export default YorumForm