import React, { Fragment, useEffect, useState } from 'react';
import moment from 'moment';
import { Paper, Grid, Avatar, makeStyles, Tooltip } from '@material-ui/core';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
  tarih: {
    fontWeight: 100,
    fontSize: '0.8rem'
  },
  yazar: {
    fontWeight: 100,
    fontSize: '0.8rem'
  },
  yorum: {
    margin: 10,
    width: '100%'
  }
}));

const YorumItem = props => {
  const classes = useStyles();
  const { yorum } = props;
  const personeller = useSelector(state => state.graph.personeller);
  const [personel, setPersonel] = useState({});

  useEffect(() => {
    if (yorum.olusturanMsID) {
      let p = personeller.filter(p => p.id === yorum.olusturanMsID);
      if (p) {
        setPersonel(p[0]);
      }
    }
  }, []);

  return (
    <Fragment>
      <div className="d-flex p-2 border w-100 radius-3 align-items-center">
        <div className="m-2 p-2">
          <Tooltip title={personel.displayName}>
            <Avatar src={personel.photo} />
          </Tooltip>
        </div>
        <div className="flex-grow-1 m-2 p-2">
          <div className="d-flex justify-content-between">
            <div className={classes.yazar}>{personel.displayName}</div>
            <div className={classes.tarih}>{moment(yorum.olusturmaTarihi).format('HH:mm DD.mm.yyyy')}</div>
          </div>
          <div className={classes.yorum}>{yorum.yorum}</div>
        </div>
      </div>
    </Fragment>
  );
};

export default YorumItem;

/*
      <div className="rounded-0 p-2">
        <div>
          <div className="avatar-icon-wrapper avatar-icon-sm mb-2">
            <span className="badge-circle badge badge-success">Online</span>
            <div className="avatar-icon rounded-circle">
              <img alt="..." src={yorum.photo} />
            </div>
          </div>
          <div>
            <div className="d-flex justify-content-between text-black-50">
              <div>{yorum.displayName}</div>
              <span className="opacity-5">
                {moment(yorum.tarih).format('dd.MM.yyyy hh:mm')}
              </span>
            </div>
            <div className="font-weight-bold my-2"></div>
            <p className="font-size-xs mb-0">{yorum.yorum}</p>
          </div>
        </div>
      </div>

*/
