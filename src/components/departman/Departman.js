import React, { Fragment, useState, useEffect } from 'react';
import {
  Grid,
  ListItemText,
  TextField,
  Avatar,
  Chip,
  ListItem,
  List,
  ListItemAvatar,
  ListItemSecondaryAction,
  Checkbox,
  Paper,
  MenuItem
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from 'store/actions';
import MaterialTable from 'material-table';
import AutoComplete from '@material-ui/lab/Autocomplete';
import { SET_DEPARTMAN } from 'store/actions';

const Departman = props => {
  const dispatch = useDispatch();
  const departman = useSelector(state => state.departman.departman);
  const departmanlar = useSelector(state => state.departman.departmanlar);
  const personeller = useSelector(state => state.graph.personeller);
  const error = useSelector(state => state.app.error);
  const [yonetici, setYonetici] = useState(null);

  useEffect(() => {
    if (departman.yoneticiMsId) {
      let p = personeller.find(p => p.id === departman.yoneticiMsId);
      setYonetici(p);
    } else {
      setYonetici(null);
    }
  }, [departman.yoneticiMsId]);

  if (!departman) {
    return <></>;
  }

  const MenuItemRender = data => {
    const isChildren = data.children !== null;
    if (isChildren) {
      return (
          <Fragment>
              <MenuItem
                key={data.adi}
                value={data.adi}>
                    {data.adi}
              </MenuItem>
                {data.children.map((node, idx) => MenuItemRender(node))}
          </Fragment>
      );
    }
    return (
      <Fragment>
        <MenuItem key={data.adi} value={data.adi}/>
      </Fragment>
    );
  };


  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12} container spacing={2} component={Paper}>
          <Grid item xs={4}>
            <TextField
              label="Departman adı"
              fullWidth
              autoFocus
              variant="outlined"
              value={departman.adi}
              onChange={e => {
                var d = { ...departman };
                d.adi = e.target.value;
                dispatch(Actions.setDepartman(d));
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <AutoComplete
              options={personeller}
              getOptionLabel={opt => opt.displayName}
              selectOnFocus
              clearOnBlur
              handleHomeEndKeys
              value={yonetici}
              onChange={(event, newValue) => {
                dispatch(
                  Actions.setDepartman({
                    yoneticiMsId: newValue ? newValue.id : null
                  })
                );
              }}
              freeSolo
              renderTags={(tagValue, getTagProps) =>
                tagValue.map((opt, index) => (
                  <Chip
                    avatar={<Avatar src={opt.photo} />}
                    label={opt.displayName}
                    variant="outlined"
                    {...getTagProps({ index })}
                  />
                ))
              }
              renderInput={params => (
                <TextField
                  error={error}
                  helperText={error && error.atama}
                  fullWidth
                  {...params}
                  variant="outlined"
                  label="Departman Yöneticisi"
                  placeholder="Departman Yöneticisi"
                />
              )}
            />
          </Grid>
        <Grid item xs={4}>
            <TextField 
                label='Bağlı olduğu departman'
                variant='outlined'
                fullWidth
                select
                value={departman.parent}
                onChange={(e) => {
                    dispatch(Actions.setDepartman({parent: e.target.value}))
                }}
            >
            {departmanlar && departmanlar.map(d => {
                if (d.adi !== departman.adi) {
                    //return MenuItemRender(d)
                    return (<MenuItem key={d._id} value={d._id}>{d.adi}</MenuItem>)
                }
            })}
            </TextField>
        </Grid>
        </Grid>
        <Grid item xs={6}>
          <Paper className="p-4">
            <List dense>
              {personeller.map(p => {
                var checked = false;
                if (departman.personeller.includes(p.id)) {
                  checked = true;
                }
                return (
                  <ListItem key={p.id}>
                    <ListItemAvatar>
                      <Avatar src={p.photo} />
                    </ListItemAvatar>
                    <ListItemText primary={p.displayName} />
                    <ListItemSecondaryAction>
                      <Checkbox
                        edge="end"
                        onChange={() => {
                          var checkeds = [...departman.personeller];
                          const currentIndex = checkeds.indexOf(p.id);
                          if (currentIndex === -1) {
                            checkeds.push(p.id);
                          } else {
                            checkeds.splice(currentIndex, 1);
                          }
                          dispatch(
                            Actions.setDepartman({ personeller: checkeds })
                          );
                        }}
                        checked={checked}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                );
              })}
            </List>
          </Paper>
        </Grid>
      </Grid>
    </Fragment>
  );
};
export default Departman;
