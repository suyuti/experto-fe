import React, { Fragment } from 'react';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Typography, IconButton, Divider } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import * as Actions from 'store/actions';

const DepartmanlarTree = props => {
  const dispatch = useDispatch();
  const departmanlar = useSelector(state => state.departman.departmanlar);
  const personeller = useSelector(state => state.graph.personeller)

  if (!departmanlar) {
    return <></>;
  }

  const TreeRender = data => {
    const isChildren = data.children !== null;
    let yonetici = personeller.find(p => p.id === data.yoneticiMsId)

    if (isChildren) {
      return (
        <TreeItem
          key={data.adi}
          nodeId={data.adi}
          label={
            <div className="p-2 d-flex align-items-center justify-content-between">
              <Typography variant="h4">{data.adi}</Typography>
              <div className='d-flex justify-content-center align-items-center'>
                <div className="text">Yönetici: {yonetici && yonetici.displayName}</div>
                <IconButton
                  onClick={e => {
                    dispatch(Actions.getDepartmanlarAsList());
                    dispatch(Actions.getDepartman(data._id));
                    props.history.push(`/departmanlar/${data._id}`);
                    e.preventDefault();
                  }}>
                  <EditIcon />
                </IconButton>
              </div>
            </div>
          }>
          {data.children.map((node, idx) => TreeRender(node))}
        </TreeItem>
      );
    }
    return (
      <Fragment>
        <TreeItem key={data.adi} nodeId={data.adi} label={data.adi} />
      </Fragment>
    );
  };

  return (
    <Fragment>
      <TreeView
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}>
        {departmanlar.map(d => TreeRender(d))}
      </TreeView>
    </Fragment>
  );
};

export default withRouter(DepartmanlarTree);
