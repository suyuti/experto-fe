import React, { Fragment } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { Chip, Avatar } from '@material-ui/core';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;
const DepartmanSecimAutoComplete = props => {
  const { departmanlar, onSelected } = props;
  return (
    <Fragment>
      <Autocomplete
        multiple
        //id="checkboxes-tags-demo"
        options={departmanlar || []}
        disableCloseOnSelect
        getOptionLabel={option => option.adi}
        renderTags={(tagValue, getTagProps) =>
          tagValue.map((opt, index) => (
            <Chip
              label={opt.adi}
              variant="outlined"
              {...getTagProps({ index })}
            />
          ))
        }
        renderOption={(option, { selected }) => (
          <React.Fragment>
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              style={{ marginRight: 3 }}
              checked={selected}
            />
            <div className="d-flex align-items-center">{option.adi}</div>
          </React.Fragment>
        )}
        onChange={(event, newValue) => {
          onSelected(newValue);
        }}
        renderInput={params => (
          <TextField
            {...params}
            fullWidth
            variant="outlined"
            label="Ekipler"
            placeholder="Ekipler"
          />
        )}
      />{' '}
    </Fragment>
  );
};

export default DepartmanSecimAutoComplete;
