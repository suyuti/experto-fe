//import moment from 'moment'
const moment = require('moment');

exports.validate = gorusme => {
  var error = null;

  if (!gorusme.musteri) {
    error = error || {};
    error.musteri = 'Firma seçmelisiniz';
  }
  if (!gorusme.kisi) {
    error = error || {};
    error.kisi = 'Kişi seçmelisiniz';
  }
  if (!gorusme.tarih) {
    error = error || {};
    error.tarih = 'Tarih seçmelisiniz';
  }
  if (!gorusme.saat) {
    error = error || {};
    error.saat = 'Saat seçmelisiniz';
  }
  if (!gorusme.kanal) {
    error = error || {};
    error.kanal = 'Görüşme kanalı seçmelisiniz';
  }
  if (gorusme.konu !== null && gorusme.konu.trim().length === 0) {
    error = error || {};
    error.konu = 'Konu belirtmelisiniz';
  }

  return error;
};
