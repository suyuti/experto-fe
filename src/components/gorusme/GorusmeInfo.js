import React, { Fragment, useEffect, useState } from 'react';
import {
  Grid,
  Card,
  CardHeader,
  Divider,
  CardContent,
  IconButton,
  TextField,
  MenuItem
} from '@material-ui/core';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from 'store/actions';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { hours, iletisimKanallari } from 'components/utils/constants';
import { DatePicker } from '@material-ui/pickers';
import {withRouter} from 'react-router'

const qs = require('query-string');

const GorusmeInfo = props => {
  const musteriler = useSelector(state => state.experto.musteriler);
  const kisiler = useSelector(state => state.kisi.kisiler);
  const gorusme = useSelector(state => state.gorusme.gorusme);
  const error = useSelector(state => state.app.error);
  const dispatch = useDispatch();
  const [musteriSecili, setMusteriSecili] = useState(null);
  const [kisiSecili, setKisiSecili] = useState(null);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getMusteriler());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (musteriler) {
      const params = qs.parse(props.location.search);
      if (params.f) {
        let m = musteriler.find(m => m._id === params.f)
        if (m) {
          setMusteriSecili(m);
          dispatch(Actions.setGorusme({ musteri: m }));
          dispatch(Actions.getMusteriKisiler(m._id));
        }
      }
    }
  }, [musteriler])

  useEffect(() => {
    if (kisiler) {
      const params = qs.parse(props.location.search);
      if (params.k) {
        let m = kisiler.find(m => m._id === params.k)
        if (m) {
          setKisiSecili(m);
          dispatch(Actions.setGorusme({ kisi: m }));
        }
      }
    }
  }, [kisiler])

  if (!musteriler) {
    return <></>
  }

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Görüşme Bilgileri"
              avatar={<AccountBalanceIcon />}
              action={
                <div className="d-flex flex-row">
                  <IconButton>
                    <MoreVertIcon />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Autocomplete
                    disabled={musteriSecili !== null}
                    options={musteriler || []}
                    getOptionLabel={option => option.marka}
                    value={gorusme.musteri}
                    onChange={(e, v, r) => {
                      if (v) {
                        dispatch(Actions.getMusteriKisiler(v._id));
                        dispatch(Actions.setGorusme({ musteri: v }));
                        //dispatch({
                        //  type: Actions.SET_TOPLANTI,
                        //  toplanti: { firma: v }
                        //});
                      }
                    }}
                    //InputProps={{
                    //  readOnly: gorev.createdByMsId != me.id
                    //}}
                    //onChange={(e, v) => {
                    //  dispatch({
                    //    type: Actıons.SET_GOREV,
                    //    gorev: { musteri: v }
                    //  });
                    //}}
                    renderInput={params => (
                      <TextField
                        {...params}
                        label="Müşteri"
                        variant="outlined"
                        fullWidth
                        error={error && error.musteri}
                        helperText={error && error.musteri}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Autocomplete
                    disabled={kisiSecili !== null}
                    options={kisiler || []}
                    getOptionLabel={option =>
                      `${option.adi} ${option.soyadi} - ${option.unvani}`
                    }
                    value={gorusme.kisi}
                    onChange={(e, v, r) => {
                      if (v) {
                        dispatch(Actions.setGorusme({ kisi: v }));
                        //dispatch(Actions.getMusteriKisiler(v._id));
                        //dispatch({
                        //  type: Actions.SET_TOPLANTI,
                        //  toplanti: { firma: v }
                        //});
                      }
                    }}
                    //InputProps={{
                    //  readOnly: gorev.createdByMsId != me.id
                    //}}

                    //onChange={(e, v) => {
                    //  dispatch({
                    //    type: Actıons.SET_GOREV,
                    //    gorev: { musteri: v }
                    //  });
                    //}}
                    renderInput={params => (
                      <TextField
                        {...params}
                        label="Görüşme yapılan kişi"
                        variant="outlined"
                        fullWidth
                        error={error && error.kisi}
                        helperText={error && error.kisi}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      error={error && error.tarih}
                      helperText={error && error.tarih}
                      autoOk
                      fullWidth
                      variant="inline"
                      inputVariant="outlined"
                      label="Görüşme Tarihi"
                      format="dd/MM/yyyy"
                      value={gorusme.tarih}
                      InputAdornmentProps={{ position: 'start' }}
                      onChange={date =>
                        dispatch(Actions.setGorusme({ tarih: date }))
                      }
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    error={error && error.saat}
                    helperText={error && error.saat}
                    fullWidth
                    label="Saat"
                    variant="outlined"
                    value={gorusme.saat}
                    onChange={e => {
                      dispatch(Actions.setGorusme({ saat: e.target.value }));
                    }}
                    select>
                    {hours.map(h => {
                      return (
                        <MenuItem key={h} value={h}>
                          {h}
                        </MenuItem>
                      );
                    })}
                  </TextField>
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    error={error && error.kanal}
                    helperText={error && error.kanal}
                    fullWidth
                    label="İletişim Kanalı"
                    variant="outlined"
                    value={gorusme.kanal}
                    onChange={e => {
                      dispatch(Actions.setGorusme({ kanal: e.target.value }));
                    }}
                    select>
                    {iletisimKanallari.map(i => {
                      return (
                        <MenuItem key={i} value={i}>
                          {i}
                        </MenuItem>
                      );
                    })}
                  </TextField>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={error && error.konu}
                    helperText={error && error.konu}
                    fullWidth
                    label="Konu"
                    variant="outlined"
                    multiline
                    rows={3}
                    value={gorusme.konu}
                    onChange={e => {
                      dispatch(Actions.setGorusme({ konu: e.target.value }));
                    }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(GorusmeInfo);
