import React, { Fragment, useEffect, useState } from 'react';
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  CardActions,
  Grid,
  TextField,
  MenuItem
} from '@material-ui/core';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { hours, iletisimKanallari } from 'components/utils/constants';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from 'store/actions';
import Autocomplete from '@material-ui/lab/Autocomplete';

const GorusmeForm = props => {
  const { source, kisi, firmaId } = props;
  const dispatch = useDispatch();
  const musteriler = useSelector(state => state.experto.musteriler);
  const kisiler = useSelector(state => state.kisi.kisiler);
  const gorusme = useSelector(state => state.gorusme.gorusme);
  const [musteriSecili, setMusteriSecili] = useState(false);
  const [kisiSecili, setKisiSecili] = useState(false);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getMusteriler());
      dispatch(Actions.yeniGorusme());
      if (source === 'firma') {
        setMusteriSecili(true);
        dispatch(Actions.getMusteriKisiler(kisi.musteri._id));
      } else if (source === 'kisi') {
        setMusteriSecili(true);
        setKisiSecili(true);
        dispatch(Actions.getMusteriKisiler(kisi.musteri._id));
      }
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (source === 'kisi' && musteriler) {
        dispatch(Actions.setGorusme({}))
    }
  }, [musteriler])

  if (!musteriler) {
    return <></>;
  }

  if (!gorusme) {
    return <></>;
  }

  return (
    <Fragment>
      <Grid container spacing={3} className="mt-2 mb-2">
        <Grid item xs={6}>
          <Autocomplete
            disabled={musteriSecili}
            options={musteriler}
            getOptionLabel={option => option.marka}
            value={gorusme.firma}
            onChange={(e, v, r) => {
              if (v) {
                dispatch(Actions.getMusteriKisiler(v._id));
                //dispatch({
                //  type: Actions.SET_TOPLANTI,
                //  toplanti: { firma: v }
                //});
              }
            }}
            //InputProps={{
            //  readOnly: gorev.createdByMsId != me.id
            //}}
            //onChange={(e, v) => {
            //  dispatch({
            //    type: Actıons.SET_GOREV,
            //    gorev: { musteri: v }
            //  });
            //}}
            renderInput={params => (
              <TextField
                {...params}
                label="Müşteri"
                variant="outlined"
                fullWidth
                //error={error && error.musteri}
                //helperText={error && error.musteri}
              />
            )}
          />
        </Grid>
        <Grid item xs={6}>
          <Autocomplete
            disabled={kisiSecili}
            options={kisiler}
            getOptionLabel={kisi =>
              `${kisi.adi} ${kisi.soyadi} [${kisi.unvani}]`
            }
            //value={gorusme.firma}
            onChange={(e, v, r) => {
              if (v) {
                //dispatch(Actions.getMusteriKisiler(v._id));
                //dispatch({
                //  type: Actions.SET_TOPLANTI,
                //  toplanti: { firma: v }
                //});
              }
            }}
            renderInput={params => (
              <TextField
                {...params}
                label="Görüşme yapılan kişi"
                variant="outlined"
                fullWidth
                //error={error && error.musteri}
                //helperText={error && error.musteri}
              />
            )}
          />
        </Grid>
        <Grid item xs={4}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              fullWidth
              //disabled={gorev.yeni}
              variant="filled"
              format="dd/MM/yyyy"
              margin="normal"
              label="Görüşme Tarihi"
              //InputProps={{
              //readOnly: gorev.createdByMsId !== me.id
              //}}
              //value={kisi.dogumTarihi}
              onChange={e => {
                //dispatch(Actions.setKisi({ dogumTarihi: e }));
              }}
              KeyboardButtonProps={{
                'aria-label': 'change date'
              }}
            />
          </MuiPickersUtilsProvider>
        </Grid>
        <Grid item xs={4}>
          <TextField fullWidth label="Saat" variant="outlined" select>
            {hours.map(h => {
              return (
                <MenuItem key={h} value={h}>
                  {h}
                </MenuItem>
              );
            })}
          </TextField>
        </Grid>
        <Grid item xs={4}>
          <TextField
            fullWidth
            label="İletişim Kanalı"
            variant="outlined"
            select>
            {iletisimKanallari.map(i => {
              return (
                <MenuItem key={i} value={i}>
                  {i}
                </MenuItem>
              );
            })}
          </TextField>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Konu"
            variant="outlined"
            multiline
            rows={3}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default GorusmeForm;
