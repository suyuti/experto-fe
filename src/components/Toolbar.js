import React, { Fragment } from 'react';
import {
  Grid,
  ButtonGroup,
  Button,
  makeStyles,
  Paper,
  Typography
} from '@material-ui/core';
import clsx from 'clsx';
import AppsIcon from '@material-ui/icons/Apps';
const useStyles = makeStyles(theme => ({
  root: {
    border: 1,
    margin: '-2rem -2rem 2rem',
    padding: 5,
    display: 'flex',
    justifyContent: 'space-between'
  }
}));

const Toolbar = props => {
  const classes = useStyles();
  return (
    <Fragment>
      <Paper square elevation={2} className={clsx(classes.root)}>
        <div className='d-flex align-items-center justify-content-center'>
          <Paper className="d-flex align-items-center justify-content-center">
            <AppsIcon />
          </Paper>
          <Typography>Baslik</Typography>
          <Typography>Alt Baslik</Typography>
        </div>
        <ButtonGroup>
          <Button>Kaydet</Button>
          <Button>Sil</Button>
        </ButtonGroup>
      </Paper>
    </Fragment>
  );
};

export default Toolbar;
