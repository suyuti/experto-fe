import React, { Fragment, useState, useEffect } from 'react';
import {
  Card,
  CardHeader,
  IconButton,
  Divider,
  CardContent,
  List,
  ListItem,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Grid,
  TextField
} from '@material-ui/core';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import AddIcon from '@material-ui/icons/Add';
import { useSelector, useDispatch } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import * as Actions from '../../store/actions';
import YorumItem from '../yorum/YorumItem';

const GorevYorumlar = props => {
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const gorev = useSelector(state => state.experto.gorev);
  const yorumlar = useSelector(state => state.experto.yorumlar);
  const [showYorumDlg, setShowYorumDlg] = useState(false);
  const [form, setForm] = useState({});

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getYorumlar(gorev._id));
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Yorumlar"
          avatar={<ChatBubbleOutlineIcon />}
          action={
            <IconButton
              onClick={e => {
                setShowYorumDlg(true);
              }}>
              <AddIcon />
            </IconButton>
          }
        />
        <Divider />
        <CardContent>
          <PerfectScrollbar>
            <List className="pt-0">
              {yorumlar &&
                yorumlar.map(y => {
                  return (
                    <ListItem>
                      <YorumItem yorum={y} />
                    </ListItem>
                  );
                })}
            </List>
          </PerfectScrollbar>
        </CardContent>
      </Card>
      {showYorumDlg && (
        <Dialog open={showYorumDlg} fullWidth maxWidth="lg">
          <DialogTitle>Yeni Yorum</DialogTitle>
          <Divider />
          <DialogContent className="p-4">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  label="Yorum"
                  fullWidth
                  multiline
                  autoFocus
                  variant="outlined"
                  rows={8}
                  value={form.yorum}
                  onChange={e => {
                    var f = { ...form };
                    f.yorum = e.target.value;
                    setForm(f);
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <Divider />
          <DialogActions>
            <Button
              onClick={() => {
                setForm({});
                setShowYorumDlg(false);
              }}
              color="primary">
              İptal
            </Button>
            <Button
              onClick={() => {
                dispatch(
                  Actions.addYorum(gorev._id, {
                    yorum: form.yorum,
                    olusturanMsID: me.id
                  })
                );
                setForm({});
                setShowYorumDlg(false);
              }}
              color="primary">
              Tamam
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </Fragment>
  );
};

export default GorevYorumlar;
