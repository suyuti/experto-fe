import React, { Fragment, useState, useEffect } from 'react';
import AutoComplete, {
  createFilterOptions
} from '@material-ui/lab/Autocomplete';
import {
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Grid,
  Avatar,
  Typography,
  Chip
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions';
import { SET_GOREV } from '../../store/actions';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
const GorevSorumlu = props => {
  const dispatch = useDispatch();
  const calisabildigimPersoneller = useSelector(
    state => state.experto.calisabildigimPersoneller
  );
  const tumDepartmanlar = useSelector(state => state.departman.departmanlar);
  const _personeller = useSelector(state => state.graph.personeller);
  const gorev = useSelector(state => state.experto.gorev);
  const [atanan, setAtanan] = useState(null);
  const error = useSelector(state => state.app.error);
  const [ceoyaBagliDepartmanlar, setCeoyaBagliDepartmanlar] = useState([])

  const [personeller, setPersoneller] = useState(
    _personeller.filter(p => calisabildigimPersoneller.indexOf(p.id) != -1)
  );
  const [departmanlar, setDepartmanlar] = useState([]);

  useEffect(() => {
    dispatch(Actions.getDepartmanlarAsList());
  }, [dispatch]);

  useEffect(() => {
    if (tumDepartmanlar) {
      let ceo = tumDepartmanlar.find(d => d.adi === 'CEO');
      let ceoyaBagliDepartmanlar = tumDepartmanlar.filter(d =>
        d.path.includes(`#${ceo._id}#`)
      );
      setCeoyaBagliDepartmanlar(ceoyaBagliDepartmanlar)
      setDepartmanlar(
        ceoyaBagliDepartmanlar.map(d => {
          return { displayName: `${d.adi}`, type: 'departman', id: d.yoneticiMsId };
        })
      );
      //console.log(ceoyaBagliDepartmanlar)
      //let ceoyaBagliDepartmanlar = await ceo.getAllChildren()
    }
  }, [tumDepartmanlar]);

  useEffect(() => {
    //debugger
    if (gorev.atananMsId) {
      let p = personeller.find(p => p.id === gorev.atananMsId);
      if (!p) {
        // personellerde bulunamadi. Gorev departmana atanmis olabilir. Departman yoneticilerinden bulunacak.
        let r = ceoyaBagliDepartmanlar.find(d => d.yoneticiMsId === gorev.atananMsId)
        if (r) {
          setAtanan({displayName: r.adi})
        }
      }
      else {
        setAtanan(p);
      }
    } else {
      setAtanan(null);
    }
  }, [gorev.atananMsId, ceoyaBagliDepartmanlar]);

  return (
    <Fragment>
      <Card className="card-box">
        <CardHeader title="Sorumlu" avatar={<PersonPinIcon />} />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <AutoComplete
                //multiple
                options={[...personeller, ...departmanlar]}
                getOptionLabel={opt => opt.displayName}
                //filterSelectedOptions
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                value={atanan}
                renderOption={option => {
                  if (option.type === 'departman') {
                    return (
                      <Fragment>
                        <div className="d-flex justify-items-between">
                          <BookmarkBorderIcon />
                          {option.displayName}
                        </div>
                      </Fragment>
                    );
                  } else {
                    return <Fragment>{option.displayName}</Fragment>;
                  }
                }}
                onChange={(event, newValue) => {
                  //const atananlar = newValue.map(a => a.id);
                  dispatch({
                    type: SET_GOREV,
                    gorev: { atananMsId: newValue ? newValue.id : null }
                  });
                  //}
                }}
                /*
                filterOptions={(options, params) => {
                const filtered = filter(options, params);
                if (params.inputValue !== '') {
                    filtered.push({
                    inputValue: params.inputValue,
                    title: `Ekle "${params.inputValue}"`
                    });
                }
                return filtered;
                }}
                */
                freeSolo
               /* renderTags={(tagValue, getTagProps) =>
                  tagValue.map((opt, index) => (
                    <Chip
                      avatar={<Avatar src={opt.photo} />}
                      label={opt.displayName}
                      variant="outlined"
                      {...getTagProps({ index })}
                    />
                  ))
                }*/
                renderInput={params => (
                  <TextField
                    error={error && error.atama}
                    helperText={error && error.atama}
                    fullWidth
                    {...params}
                    variant="outlined"
                    label="Personel"
                    placeholder="Personel"
                  />
                )}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default GorevSorumlu;
