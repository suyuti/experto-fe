import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Card,
  CardContent,
  CardActions,
  Button,
  CardHeader,
  Divider,
  IconButton,
  Avatar,
  Tooltip,
  LinearProgress,
  TextField,
  MenuItem
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import InfiniteScroll from 'react-infinite-scroll-component';
import { BarLoader } from 'react-spinners';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import RefreshIcon from '@material-ui/icons/Refresh';
import { withRouter } from 'react-router';
import moment from 'moment';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import MaterialTable from 'material-table';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const qs = require('query-string');

const GorevList = props => {
  const { filter, setCount } = props;
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const gorevler = useSelector(state => state.experto.gorevler);
  const [filteredGorevler, setFilteredGorevler] = useState([]);

  const myTasks = useSelector(state => state.experto.gorevler);
  const personeller = useSelector(state => state.graph.personeller);
  const [subtitle, setSubtitle] = useState('');
  const [gorevTipi, setGorevTipi] = useState(props.gorevTipi);
  const yoneticisiOldugumPersoneller = useSelector(
    state => state.experto.yoneticisiOldugumPersoneller
  );

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorevler());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    console.log(filter);
    var result = [...gorevler];
    if (filter) {
      if (filter.durum) {
        if (filter.durum !== 'hepsi') {
          result = result.filter(g => g.sonuc === filter.durum);
        }
      }

      if (filter.gecikme === 'gecikmis') {
        result = result.filter(g => {
          if (g.sonuc === 'acik') {
            return (
              new Date(g.sonTarih).setHours(0, 0, 0, 0) <
              new Date().setHours(0, 0, 0, 0)
            );
          } else {
            return (
              new Date(g.sonTarih).setHours(0, 0, 0, 0) <
              new Date(g.sonucTarihi).setHours(0, 0, 0, 0)
            );
          }
        });
      } else if (filter.gecikme === 'vaktinde') {
        result = result.filter(g => {
          if (g.sonuc === 'acik') {
            return (
              new Date(g.sonTarih).setHours(0, 0, 0, 0) >=
              new Date().setHours(0, 0, 0, 0)
            );
          } else {
            return (
              new Date(g.sonTarih).setHours(0, 0, 0, 0) >=
              new Date(g.sonucTarihi).setHours(0, 0, 0, 0)
            );
          }
        });
      }

      if (filter.personeller) {
        result = result.filter(g => filter.personeller.includes(g.atananMsId))
      }
    }
    setCount(result.length)
    setFilteredGorevler(result);
  }, [filter]);

  useEffect(() => {
    if (gorevler) {
      const type = qs.parse(props.location.search);
      if (type.t === 'acik') {
        setFilteredGorevler(
          gorevler.filter(g => g.atananMsId === me.id && g.sonuc === 'acik')
        );
      } else if (type.t === 'basarili') {
        setFilteredGorevler(
          gorevler.filter(g => g.atananMsId === me.id && g.sonuc === 'basarili')
        );
      } else if (type.t === 'basarisiz') {
        setFilteredGorevler(
          gorevler.filter(
            g => g.atananMsId === me.id && g.sonuc === 'basarisiz'
          )
        );
      } else if (type.t === 'olusturdugum') {
        setFilteredGorevler(gorevler.filter(g => g.createdByMsId === me.id));
      } else if (type.t === 'ekipacik') {
        debugger;
        setFilteredGorevler(
          gorevler.filter(
            g =>
              g.atananMsId !== me.id &&
              g.sonuc === 'acik' &&
              yoneticisiOldugumPersoneller.includes(g.atananMsId)
          )
        );
      } else if (type.t === 'ekipbasarili') {
        setFilteredGorevler(
          gorevler.filter(g => g.atananMsId !== me.id && g.sonuc === 'basarili')
        );
      } else if (type.t === 'ekipgeciken') {
        setFilteredGorevler(
          gorevler.filter(
            g =>
              g.atananMsId !== me.id &&
              g.sonuc === 'acik' &&
              new Date(g.sonTarih).setHours(0, 0, 0, 0) <
                new Date().setHours(0, 0, 0, 0)
          )
        );
      } else if (type.t === 'geciken') {
        setFilteredGorevler(
          gorevler.filter(
            g =>
              g.atananMsId === me.id &&
              g.sonuc === 'acik' &&
              new Date(g.sonTarih).setHours(0, 0, 0, 0) <
                new Date().setHours(0, 0, 0, 0)
          )
        );
      }
    }
  }, [gorevler, props.location.search]);

  return (
    <Fragment>
      {!myTasks && <LinearProgress />}

      <MaterialTable
        title="Görevlerim"
        data={filteredGorevler || []}
        columns={[
          { title: 'Görev', field: 'baslik' },
          {
            title: 'Son Tarih',
            field: 'sonTarih',
            customSort: (a, b) => {
              return moment(a.sonTarih) - moment(b.sonTarih);
            },
            render: (rowData, type) => {
              if (type === 'row') {
                return moment(rowData.sonTarih).format('DD.MM.yyyy');
              } else {
                return moment(rowData.sonTarih).format('DD.MM.yyyy');
              }
            }
          },
          { title: 'Müşteri', field: 'musteri.marka' },
          {
            title: 'Atama',
            field: 'atananMsId',
            render: (rowData, type) => {
              if (type === 'row') {
                let p = personeller.find(p => p.id == rowData.atananMsId);
                if (!p) {
                  return <></>;
                }
                return (
                  <div className="d-flex align-items-center">
                    <Avatar src={p.photo} />
                    <div className="ml-2">{p.displayName}</div>
                  </div>
                );
              } else {
                if (rowData) {
                  let p = personeller.find(p => p.id === rowData);
                  if (p) {
                    return (
                      <div className="d-flex align-items-center">
                        <Avatar src={p.photo} />
                        <div className="ml-2">{p.displayName}</div>
                      </div>
                    );
                  }
                }
              }
            }
          },
          { title: 'Öncelik', field: 'oncelik' },
          {
            title: 'Durum',
            field: 'sonuc',
            lookup: {'acik': 'Acik', 'basarili': 'Basarili', 'basarisiz': 'Basarisiz', 'iptal': 'Iptal'},
            render: (rowData, type) => {
              if (type === 'row') {
                if (rowData.sonuc === 'acik') {
                  const today = new Date();
                  const sonTarih = new Date(rowData.sonTarih);

                  //if (today > sonTarih) {
                  //  return (
                  //    <div className="h-auto py-0 px-3 badge badge-danger">
                  //      Gecikmiş
                  //    </div>
                  //  );
                  //} else {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-info">
                      Açık
                    </div>
                  );
                  //}
                } else if (rowData.sonuc === 'basarili') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-success">
                      Başarılı
                    </div>
                  );
                } else if (rowData.sonuc === 'basarisiz') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-first">
                      Başarısız
                    </div>
                  );
                } else if (rowData.sonuc === 'iptal') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-warning">
                      İptal
                    </div>
                  );
                }
                return <div></div>;
              } else {
                return rowData;
              }
            }
          },
          {
            title: 'Gecikme',
            //title: 'sonuc',
            //lookup: {1: 'Gecikmiş', 2: 'Vaktinde'},
            render: (rowData, type) => {
              if (type === 'row') {
                if (rowData.sonuc === 'acik') {
                  const today = new Date();
                  const sonTarih = new Date(rowData.sonTarih);
                  today.setHours(0, 0, 0, 0);
                  sonTarih.setHours(0, 0, 0, 0);
                  if (today > sonTarih) {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-danger">
                        Gecikmiş
                      </div>
                    );
                  } else {
                    let kacGun = moment(sonTarih).diff(today, 'days');
                    return <>{kacGun === 0 ? 'Bugün son' : `${kacGun} gün`}</>;
                  }
                } else {
                  if (!rowData.sonucTarihi) {
                    return <></>;
                  }
                  const bitirmeTarihi = new Date(rowData.sonucTarihi);
                  const sonTarih = new Date(rowData.sonTarih);
                  bitirmeTarihi.setHours(0, 0, 0, 0);
                  sonTarih.setHours(0, 0, 0, 0);
                  if (bitirmeTarihi > sonTarih) {
                    let kacGun = moment(bitirmeTarihi).diff(sonTarih, 'days');
                    return (
                      <div className="h-auto py-0 px-3 badge badge-danger">
                        {kacGun} gün gecikme
                      </div>
                    );
                  } else {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-info">
                        vaktinde
                      </div>
                    );
                  }
                }
              } else {
                return '';
              }
            }
          },
          {
            title: 'Ek',
            render: row => {
              if (row.attachments && row.attachments.length > 0) {
                return <AttachFileIcon />;
              } else {
                return <></>;
              }
            }
          }
        ]}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Detay',
            onClick: (event, rowData) =>
              props.history.push(`/gorev/${rowData._id}`) //alert('You saved ' + rowData.name)
          }
        ]}
        options={{
          //filtering: true,
          grouping: true,
          pageSizeOptions: [5, 10, 20, 50, 100],
          pageSize: 20,
          actionsColumnIndex: -1
        }}
      />
    </Fragment>
  );
};

export default withRouter(GorevList);
