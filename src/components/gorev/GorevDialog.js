import React, { Fragment, useState, useEffect } from 'react';
import {
  Dialog,
  DialogActions,
  Button,
  Divider,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  AppBar,
  Toolbar,
  Card,
  CardHeader,
  Typography,
  CardContent
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions';

const EXPERTO_PLAN_ID = process.env.REACT_APP_EXPERTO_PLAN;

const GorevDialog = props => {
  const dispatch = useDispatch();
  const show = useSelector(state => state.app.showGorevDlg);
  const initialTask = {
    //planId: EXPERTO_PLAN_ID,
    //title: '',
    //assignments: {}
    baslik: '',
    aciklamalar: ''
  };
  const [task, setTask] = useState(initialTask);

  useEffect(() => {
    setTask(initialTask);
  }, [show]);

  const createNewTask = () => {
    //dispatch(Actions.createTask(task));
    dispatch(Actions.createGorev(task));
  };

  return (
    <Fragment>
      <Dialog
        open={show}
        onClose={() => dispatch(Actions.hideGorevdialog())}
        aria-labelledby="form-dialog-title">
        <AppBar position="static" elevation={1}>
          <Toolbar className="flex w-full">
            <Typography variant="subtitle1" color="inherit">
              Yeni Görev
            </Typography>
          </Toolbar>
          <div className="flex flex-col items-center justify-center pb-24"></div>
        </AppBar>
        <DialogContent>
          <TextField
            variant="outlined"
            margin="dense"
            label="Görev"
            fullWidth
            autoFocus
            value={task.baslik}
            onChange={e => {
              var t = { ...task };
              t.baslik = e.target.value;
              setTask(t);
            }}
          />
          <TextField
            variant="outlined"
            margin="dense"
            label="Açıklama"
            value={task.aciklama}
            onChange={e => {
              var t = { ...task };
              t.aciklama = e.target.value;
              setTask(t);
            }}
            fullWidth
          />
          <TextField
            variant="outlined"
            margin="dense"
            label="Başlama Tarihi"
            fullWidth
          />
          <TextField
            variant="outlined"
            margin="dense"
            label="Son Tarih"
            fullWidth
          />
          <TextField
            variant="outlined"
            margin="dense"
            label="Not"
            fullWidth
            multiline
            rows={3}
          />
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button
            onClick={() => dispatch(Actions.hideGorevdialog())}
            variant="contained"
            color="primary">
            Kapat
          </Button>
          <Button variant="contained" color="primary" onClick={createNewTask}>
            Kaydet
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

export default GorevDialog;
