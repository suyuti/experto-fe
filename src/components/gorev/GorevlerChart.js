import React, { Fragment, useEffect, useState } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Grid, Paper, Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

const GorevlerChart = props => {
  const gorevler = useSelector(state => state.experto.gorevler);
    const [result, setResult] = useState({})
  useEffect(() => {
      var r = {}
    for(var g of gorevler) {
        if (r[g.atamaMsId]) {
            r[g.atamaMsId].count++
        }
        else {
            r[g.atamaMsId].count = 1
        }
    }
  }, [])

  const options = {
    chart: {
      width: 380,
      type: 'pie'
    },
    labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        }
      }
    ]
  };

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Paper className="p-4">
            <ReactApexChart
              options={options}
              series={[44, 25, 36]}
              type="pie"
            />
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className="p-4">
            <ReactApexChart
              options={options}
              series={[44, 25, 36]}
              type="pie"
            />
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className="p-4">
            <ReactApexChart
              options={options}
              series={[44, 25, 36]}
              type="pie"
            />
          </Paper>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default GorevlerChart;
