import React, { Fragment, useEffect, useState } from 'react';
import {
  TextField,
  Grid,
  Paper,
  MenuItem,
  Typography
} from '@material-ui/core';
import PersonelSecimAutoComplete from 'components/personel/PersonelSecimAutoComplete';
import DepartmanSecimAutoComplete from 'components/departman/DepartmanSecimAutoComplete';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from 'store/actions';

const GorevlerToolbar = props => {
  const { onFilterChange, count } = props;
  const dispatch = useDispatch();
  const yoneticisiOldugumPersoneller = useSelector(
    state => state.experto.yoneticisiOldugumPersoneller
  );

  const personeller = useSelector(state => state.graph.personeller);
  const departmanlar = useSelector(state => state.departman.departmanlar);
  const [filter, setFilter] = useState({});

  const [ekibimdekiPersoneller, setEkibimdekiPersoneller] = useState(
    personeller.filter(p => yoneticisiOldugumPersoneller.indexOf(p.id) != -1)
  );

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getDepartmanlarAsList());
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Paper className="p-2 m-2">
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <PersonelSecimAutoComplete
              personeller={ekibimdekiPersoneller}
              onSelected={val => {
                var _filter = { ...filter };
                _filter.personeller = val;
                setFilter(_filter);
                onFilterChange(_filter);
              }}
            />
          </Grid>
          {/*
          <Grid item xs={3}>
            <DepartmanSecimAutoComplete
              departmanlar={departmanlar}
              onSelected={val => {
                var _filter = { ...filter };
                _filter.departmanlar = val;
                setFilter(_filter);
                onFilterChange(_filter);
              }}
            />
          </Grid>
          */}
          <Grid item xs={2}>
            <TextField
              fullWidth
              variant="outlined"
              label="Durum"
              select
              value={filter.durum}
              onChange={e => {
                var _filter = { ...filter };
                _filter.durum = e.target.value;
                setFilter(_filter);
                onFilterChange(_filter);
              }}>
              <MenuItem key="hepsi" value="hepsi">
                Hepsi
              </MenuItem>
              <MenuItem key="acik" value="acik">
                Açık
              </MenuItem>
              <MenuItem key="basarili" value="basarili">
                Başarılı
              </MenuItem>
              <MenuItem key="basarisiz" value="basarisiz">
                Başarısız
              </MenuItem>
              <MenuItem key="iptal" value="iptal">
                İptal
              </MenuItem>
            </TextField>
          </Grid>
          <Grid item xs={2}>
            <TextField
              fullWidth
              variant="outlined"
              label="Gecikme"
              select
              value={filter.gecikme}
              onChange={e => {
                var _filter = { ...filter };
                _filter.gecikme = e.target.value;
                setFilter(_filter);
                onFilterChange(_filter);
              }}>
              <MenuItem key="hepsi" value="hepsi">
                Hepsi
              </MenuItem>
              <MenuItem key="gecikmis" value="gecikmis">
                Geçikmiş
              </MenuItem>
              <MenuItem key="vaktinde" value="vaktinde">
                Vaktinde
              </MenuItem>
            </TextField>
          </Grid>
          <Grid item xs={2}>
            <Typography variant="h5">{count} adet</Typography>
          </Grid>
        </Grid>
      </Paper>
    </Fragment>
  );
};
export default GorevlerToolbar;
