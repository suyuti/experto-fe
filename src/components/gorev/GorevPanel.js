import React, { Fragment, useState, useEffect } from 'react';
import {
  Grid,
  Divider,
  Card,
  CardHeader,
  CardContent,
  TextField,
  List,
  ListItem,
  MenuItem,
  Button,
  CardActions,
  Avatar,
  IconButton,
  Checkbox,
  Menu,
  Tooltip,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  FormControlLabel,
  Typography
} from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useSelector, useDispatch } from 'react-redux';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import Autocomplete from '@material-ui/lab/Autocomplete';
import * as Actions from '../../store/actions';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { SET_GOREV } from '../../store/actions';
import moment from 'moment';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import YorumItem from 'components/yorum/YorumItem';
import YorumForm from 'components/yorum/YorumForm';
import IOSSwitch from 'components/IosSwitch';
import GorevYorumlar from './GorevYorum';
import { withRouter } from 'react-router';
import GorevSorumlu from './GorevSorumlu';
import {
  toplantiKategorileri,
  gorevKategorileri
} from 'components/utils/constants';
import Timeline from 'components/timeline/Timeline';
import AttachPanel from 'components/attachment/AttachPanel'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1)
    }
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

const GorevBody = props => {
  const { onCheck } = props;
  const dispatch = useDispatch();
  const musteriler = useSelector(state => state.experto.musteriler);
  const gorev = useSelector(state => state.experto.gorev);
  const me = useSelector(state => state.graph.me);
  const error = useSelector(state => state.app.error);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getMusteriler());
    };
    load();
  }, [dispatch]);

  if (!gorev) {
    return <></>;
  }

  return (
    <Fragment>
      <Card>
        <CardHeader
          title={<Typography variant="h5">Görev</Typography>}
          avatar={<AssignmentTurnedInIcon />}
          action={
            <IconButton>
              <MoreVertIcon />
            </IconButton>
          }
        />
        <Divider />
        <CardContent>
          <Grid container spacing={1}>
            {gorev.ilgiliToplanti && (
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={e => {
                    props.history.push(`/toplanti/${gorev.ilgiliToplanti}`);
                  }}>
                  İlgili Toplantıya Git
                </Button>
              </Grid>
            )}
            <Grid item xs={10}>
              <TextField
                error={error && error.baslik}
                helperText={error && error.baslik}
                required
                label="Baslik"
                autoFocus
                fullWidth
                variant="outlined"
                value={gorev.baslik}
                onChange={e => {
                  dispatch({
                    type: SET_GOREV,
                    gorev: { baslik: e.target.value }
                  });
                }}
                InputProps={{
                  readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                }}
              />
            </Grid>
            <Grid item xs={2}>
              <TextField
                label="Öncelik"
                fullWidth
                variant="outlined"
                value={gorev.oncelik}
                InputProps={{
                  readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                }}
                onChange={e => {
                  dispatch({
                    type: SET_GOREV,
                    gorev: { oncelik: e.target.value }
                  });
                }}
                select>
                <MenuItem key="Acil" value="Acil">
                  Acil
                </MenuItem>
                <MenuItem key="Önemli" value="Önemli">
                  Önemli
                </MenuItem>
                <MenuItem key="Orta" value="Orta">
                  Orta
                </MenuItem>
                <MenuItem key="Düşük" value="Düşük">
                  Düşük
                </MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={error && error.aciklama}
                helperText={error && error.aciklama}
                required
                label="Acıklama"
                fullWidth
                variant="outlined"
                multiline
                rows={3}
                value={gorev.aciklama}
                InputProps={{
                  readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                }}
                onChange={e => {
                  dispatch({
                    type: SET_GOREV,
                    gorev: { aciklama: e.target.value }
                  });
                }}
              />
            </Grid>
            <Grid item xs={12} container spacing={3}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid item xs={6}>
                  <KeyboardDatePicker
                    //error={error && error.tarih}
                    //helperText={error && error.tarih}
                    disabled
                    autoOk
                    fullWidth
                    variant="inline"
                    inputVariant="outlined"
                    label="Başlama Tarihi"
                    format="dd/MM/yyyy"
                    value={gorev.baslamaTarihi}
                    InputAdornmentProps={{ position: 'start' }}
                    onChange={date =>
                      dispatch(Actions.setGorev({ baslamaTarihi: date }))
                    }
                  />
                </Grid>
                <Grid item xs={6}>
                  <KeyboardDatePicker
                    error={error && error.sonTarih}
                    helperText={error && error.sonTarih}
                    autoOk
                    fullWidth
                    variant="inline"
                    inputVariant="outlined"
                    label="Son Tarih"
                    format="dd/MM/yyyy"
                    value={gorev.sonTarih}
                    InputAdornmentProps={{ position: 'start' }}
                    onChange={date =>
                      dispatch(Actions.setGorev({ sonTarih: date }))
                    }
                  />
                </Grid>
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} container spacing={3}>
              <Grid item xs={2}>
                <FormControlLabel
                  control={
                    <IOSSwitch
                      InputProps={{
                        readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                      }}
                      checked={gorev.icGorevMi}
                      disabled={!gorev.yeni}
                      onChange={e => {
                        let checked = e.target.checked;
                        var g = { ...gorev };
                        g.icGorevMi = checked;
                        dispatch({ type: SET_GOREV, gorev: g });
                      }}
                      name="checkedB"
                    />
                  }
                  label="İç Görev mi?"
                />
              </Grid>
              <Grid item xs={10}>
                <Autocomplete
                  disabled={gorev.icGorevMi || !gorev.yeni}
                  options={musteriler}
                  getOptionLabel={option => {
                    return option.marka;
                  }}
                  value={gorev.musteri}
                  InputProps={{
                    readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                  }}
                  onChange={(e, v) => {
                    dispatch({
                      type: SET_GOREV,
                      gorev: { musteri: v }
                    });
                  }}
                  renderInput={params => (
                    <TextField
                      error={error && error.musteri && !gorev.icGorevMi}
                      helperText={error && !gorev.icGorevMi && error.musteri}
                      {...params}
                      label="İlgili Müşteri"
                      variant="outlined"
                      fullWidth
                      //InputProps={{
                      //  readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                      //}}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={error && error.kategori}
                helperText={error && error.kategori}
                required
                label="Görev kategorisi"
                fullWidth
                variant="outlined"
                value={gorev.gorevKategori}
                InputProps={{
                  readOnly: gorev.createdByMsId !== me.id || !gorev.yeni
                }}
                onChange={e => {
                  dispatch(Actions.setGorev({ gorevKategori: e.target.value }));
                }}
                select>
                {gorevKategorileri.map(k => {
                  return (
                    <MenuItem key={k} value={k}>
                      {k}
                    </MenuItem>
                  );
                })}
              </TextField>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

const GorevAtama = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const gorev = useSelector(state => state.experto.gorev);
  const personeller = useSelector(state => state.graph.personeller);
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const [atamaListesi, setAtamaListesi] = useState(
    personeller.map(p => {
      const secili = gorev.atananlar.includes(p.id);
      return { secili: secili, personel: p };
    })
  );

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };

  const handleAtamaSecimi = (e, p) => {
    var _ = [...atamaListesi];
    var x = _.filter(a => a.personel.id == p.personel.id);
    x[0].secili = e.target.checked;
    setAtamaListesi(_);

    var atananlar = [...gorev.atananlar];
    if (e.target.checked) {
      atananlar.push(p.personel.id);
    } else {
      atananlar = atananlar.filter(a => a !== p.personel.id);
    }

    dispatch({
      type: SET_GOREV,
      gorev: { atananlar: atananlar }
    });
  };

  const personelMenu = (
    <Menu
      anchorEl={anchorElMenu}
      keepMounted
      open={Boolean(anchorElMenu)}
      onClose={() => setAnchorElMenu(null)}
      getContentAnchorEl={null}
      classes={{ list: 'p-0' }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <div className="overflow-hidden dropdown-menu-xl p-0">
        <div className="bg-composed-wrapper bg-light mt-0">
          <div className="bg-composed-wrapper--content text-black px-4 py-3">
            <h5 className="mb-1">Atanacak Personeller</h5>
            <p className="mb-0 opacity-7"></p>
          </div>
        </div>
        <List>
          {atamaListesi.map(p => (
            <Fragment>
              <ListItem
                //button
                className="align-box-row"
                //onClick={() => handleTemsilci(p.id)}
              >
                <div className="d-flex align-items-center">
                  <Checkbox
                    checked={p.secili}
                    onChange={e => handleAtamaSecimi(e, p)}
                  />
                  <Avatar className="mr-4" src={p.personel.photo} />
                  {p.personel.displayName}
                  <span className="text-black">{p.personel.jobTitle}</span>
                </div>
              </ListItem>
              <Divider />
            </Fragment>
          ))}
        </List>
      </div>
    </Menu>
  );

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Sorumlu"
          avatar={<PersonPinIcon />}
          action={
            <IconButton onClick={handleClickMenu}>
              <AddIcon />
            </IconButton>
          }
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <div className="d-flex align-items-center justify-content-between">
                <AvatarGroup>
                  {atamaListesi
                    .filter(a => a.secili)
                    .map(p => (
                      <Tooltip title={p.personel.displayName}>
                        <Avatar
                          className={classes.avatar}
                          alt={p.personel.displayName}
                          src={p.personel.photo}
                        />
                      </Tooltip>
                    ))}
                </AvatarGroup>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      {personelMenu}
    </Fragment>
  );
};

const GorevTimeline = props => {
  const me = useSelector(state => state.graph.me);
  const gorev = useSelector(state => state.experto.gorev);
  const users = useSelector(state => state.graph.personeller);

  const olusturmaItem = h => {
    let user = users.find(u => u.id == h.msid);
    return (
      <div className="timeline-item--content">
        <div className="timeline-item--icon"></div>
        <h4 className="timeline-item--label mb-2 font-weight-bold">
          {h.eylem}
        </h4>
        <Tooltip title={user.displayName}>
          <Avatar src={user.photo} />
        </Tooltip>
        <p></p>
      </div>
    );
  };

  const timeLineItem = item => {
    //if (item.eylem == 'create')
    return olusturmaItem(item);
  };

  return (
    <Fragment>
      <Card className="card-box mb-4">
        <CardContent className="p-3">
          <div className="height-320">
            <PerfectScrollbar>
              <div className="timeline-list timeline-list-offset timeline-list-offset-dot">
                {gorev.history.map(h => {
                  return (
                    <div className="timeline-item">
                      <div className="timeline-item-offset">
                        {moment(h.tarih).format('HH.mm DD.MM.YYYY')}
                      </div>
                      {timeLineItem(h)}
                    </div>
                  );
                })}
              </div>
            </PerfectScrollbar>
          </div>
        </CardContent>
      </Card>
    </Fragment>
  );
};

const GorevPanel = props => {
  //const { gorev } = props;
  const dispatch = useDispatch();
  const gorev = useSelector(state => state.experto.gorev);
  //
  if (!gorev) {
    return <></>;
  }

  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={8} container justify="flex-start" spacing={2}>
          <Grid item xs={12}>
            <GorevBody gorev={gorev} {...props} />
          </Grid>
          {!gorev.yeni && (
            <Grid item xs={12}>
              <GorevYorumlar />
            </Grid>
          )}
        </Grid>
        <Grid item xs={4} container spacing={2}>
          {!gorev.yeni && (
            <Grid item xs={12}>
              <GorevAction {...props} />
            </Grid>
          )}
          <Grid item xs={12}>
            <GorevSorumlu />
          </Grid>
          <Grid xs ={12}>
            <AttachPanel
              files={gorev.attachments || []}
              onFileUploaded={fileId => {
                var g = {...gorev}
                g.attachments.push(fileId)
                dispatch(Actions.setGorev(g))
              }}
            />
          </Grid>
          {!gorev.yeni && (
            <Grid item xs={12}>
              <Timeline />
            </Grid>
          )}
        </Grid>
      </Grid>
    </Fragment>
  );
};

const GorevAction = props => {
  const [aciklama, setAciklama] = useState(false);
  const dispatch = useDispatch();
  const gorev = useSelector(state => state.experto.gorev);
  const me = useSelector(state => state.graph.me);
  const [enabled, setEnabled] = useState(gorev.sonuc === 'acik' && gorev.atananMsId === me.id);

  return (
    <Fragment>
      <Card className="card-box">
        <CardHeader title="Sonuçlandırma" />
        <Divider />
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                label="Sonuç"
                variant="outlined"
                disabled={!enabled}
                fullWidth
                value={gorev.sonuc}
                onChange={e => {
                  switch (e.target.value) {
                    case 'basarisiz': {
                      setAciklama(true);
                      dispatch({
                        type: SET_GOREV,
                        gorev: { sonuc: 'basarisiz', sonucTarihi: new Date() }
                      });
                      break;
                    }
                    case 'iptal': {
                      setAciklama(true);
                      dispatch({
                        type: SET_GOREV,
                        gorev: { sonuc: 'iptal', sonucTarihi: new Date() }
                      });
                      break;
                    }
                    case 'basarili': {
                      setAciklama(false);
                      dispatch({
                        type: SET_GOREV,
                        gorev: { sonuc: 'basarili', sonucTarihi: new Date() }
                      });
                      break;
                    }
                  }
                }}
                select>
                <MenuItem key="basarili" value="basarili">
                  Başarılı Tamamlandı
                </MenuItem>
                <MenuItem key="basarisiz" value="basarisiz">
                  Başarısız
                </MenuItem>
                <MenuItem key="iptal" value="iptal">
                  İptal
                </MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={12}>
              {aciklama && (
                <TextField
                  label="Açıklama"
                  variant="outlined"
                  multiline
                  rows={3}
                  fullWidth
                  onChange={e => {
                    dispatch({
                      type: SET_GOREV,
                      gorev: { sonucAciklama: e.target.value }
                    });
                  }}
                />
              )}
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button
            variant="contained"
            color="primary"
            disabled={!enabled}
            onClick={() => {
              dispatch(Actions.saveGorev(gorev));
              props.history.push('/gorevler?t=acik');
            }}>
            Görevi Tamamla
          </Button>
        </CardActions>
      </Card>
    </Fragment>
  );
};

export default withRouter(GorevPanel);
