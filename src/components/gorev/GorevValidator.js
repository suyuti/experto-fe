//import moment from 'moment'
const moment = require('moment')

exports.validate = (gorev) => {
    var error = null
    if (gorev.baslik === null || gorev.baslik.trim().length === 0) {
        error = error || {}
        error.baslik = 'Başlık yazmalısınız'
    }
    if (gorev.aciklama === null || gorev.aciklama.trim().length === 0) {
        error = error || {}
        error.aciklama = 'Açıklama yazmalısınız'
    }
    if (!moment(gorev.sonTarih).isValid()) {
        error = error || {}
        error.sonTarih = 'Son tarihi belirtmelisiniz'
    }
    if (!gorev.icGorevMi && !gorev.musteri) {
        error = error || {}
        error.musteri = 'Dış görev için müşteri seçmelisiniz'
    }
    if (!gorev.oncelik) {
        error = error || {}
        error.oncelik = 'Öncelik belirlemelisiniz'
    }
    if (!gorev.atananMsId) {
        error = error || {}
        error.atama = 'Sorumlu atamalısınız'
    }
    if (!gorev.gorevKategori) {
        error = error || {}
        error.kategori = 'Görev kategorisi seçmelisiniz'
    }

    return error
}