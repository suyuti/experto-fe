import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Grid,
  TextField,
  FormControlLabel,
  IconButton,
  TableHead,
  TableRow,
  Table,
  TableCell,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Tooltip,
  TableBody
} from '@material-ui/core';
import IOSSwitch from '../IosSwitch';
import AddIcon from '@material-ui/icons/Add';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import MuseumIcon from '@material-ui/icons/Museum';

const MusteriMaliBilgiler = props => {
  const dispatch = useDispatch();
  //const [editMode, setEditMode] = useState(true);
  const musteri = useSelector(state => state.experto.musteri);
  useEffect(() => {
    const load = async () => {};
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Card>
            <CardHeader
              title="Mali Bilgiler"
              avatar={<MuseumIcon  />}
              action={
                <IconButton>
                  <MoreVertIcon  />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <TextField
                    label="Vergi Dairesi"
                    fullWidth
                    variant='outlined'
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.firmaBilgi.vergiDairesi}
                    onChange={e => {
                      var m = {...musteri}
                      m.firmaBilgi.vergiDairesi = e.target.value
                      dispatch(Actions.setMusteri(m))
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Vergi No"
                    fullWidth
                    variant='outlined'
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.firmaBilgi.vergiNo}
                    onChange={e => {
                      var m = {...musteri}
                      m.firmaBilgi.vergiNo = e.target.value
                      dispatch(Actions.setMusteri(m))
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <IOSSwitch
                        checked={musteri.firmaBilgi.kobiMi}
                        //disabled={!editMode}
                        onChange={e => {
                          let checked = e.target.checked;
                          var m = { ...musteri };
                          m.firmaBilgi.kobiMi = checked;
                          dispatch({ type: Actions.SET_MUSTERI, musteri: m });
                        }}
                        name="checkedB"
                      />
                    }
                    label={musteri.firmaBilgi.kobiMi ? "Kobi" : "Sanayi"}
                  />
                </Grid>
                <Grid item xs={3}>
                  <FormControlLabel
                    control={
                      <IOSSwitch
                        checked={musteri.firmaBilgi.eFaturaMi}
                        //disabled={!editMode}
                        onChange={e => {
                          let checked = e.target.checked;
                          var m = { ...musteri };
                          m.firmaBilgi.eFaturaMi = checked;
                          dispatch({ type: Actions.SET_MUSTERI, musteri: m });
                        }}
                        name="checkedB"
                      />
                    }
                    label={musteri.firmaBilgi.eFaturaMi ? "e-Fatura" : "e-Arşiv"}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card>
            <CardHeader
              title="Ciro"
              avatar={<MonetizationOnIcon />}
              action={
                <Tooltip title="Ekle">
                  <IconButton>
                    <AddIcon />
                  </IconButton>
                </Tooltip>
              }
            />
            <Divider />
            <CardContent>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Yil</TableCell>
                    <TableCell>Ciro</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover>
                    <TableCell>2019</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriMaliBilgiler;
