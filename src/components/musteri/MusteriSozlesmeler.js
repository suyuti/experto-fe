import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  TableHead,
  TableRow,
  Table,
  TableCell,
  TableBody

} from '@material-ui/core';

const MusteriSozlesmeler = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    const load = async () => {};
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Sözleşmeler"
              avatar={<PlaylistAddCheckIcon fontSize="large" />}
              action={
                <IconButton>
                  <MoreVertIcon fontSize="large" />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Sözleşme No</TableCell>
                    <TableCell>Ürün</TableCell>
                    <TableCell>Sözleşme Tarihi</TableCell>
                    <TableCell>Teklif Veren</TableCell>
                    <TableCell>Başlangıç Tarihi</TableCell>
                    <TableCell>Bitiş Tarihi</TableCell>
                    <TableCell>Durum</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriSozlesmeler;
