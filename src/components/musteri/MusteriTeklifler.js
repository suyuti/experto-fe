import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  TableHead,
  TableRow,
  Table,
  TableCell,
  TableBody

} from '@material-ui/core';

const MusteriTeklifler = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    const load = async () => {};
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Teklifler"
              avatar={<LocalOfferIcon fontSize="large" />}
              action={
                <IconButton>
                  <MoreVertIcon fontSize="large" />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Teklif No</TableCell>
                    <TableCell>Ürünler</TableCell>
                    <TableCell>Teklif Tarihi</TableCell>
                    <TableCell>Teklif Veren</TableCell>
                    <TableCell>Durum</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriTeklifler;
