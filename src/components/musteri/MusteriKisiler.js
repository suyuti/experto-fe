import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import AddIcon from '@material-ui/icons/Add';
import {
  Grid,
  Avatar,
  Button,
  CardHeader,
  Divider,
  CardContent,
  Card,
  Tooltip,
  IconButton,
  ButtonBase,
  LinearProgress
} from '@material-ui/core';
import { withRouter } from 'react-router';
import MaterialTable from 'material-table';

const KisiKart = props => {
  const { kisi } = props;
  return (
    <Fragment>
      <ButtonBase
        onClick={() => {
          props.history.push(`/kisi/${kisi._id}`);
        }}>
        <div className="d-flex align-items-center">
          <Avatar alt="..." src="" className="mr-2" />
          <div>
            <a
              //href="#/"
              onClick={e => e.preventDefault()}
              className="font-weight-bold text-black"
              title="...">
              {`${kisi.adi} ${kisi.soyadi}`}
            </a>
            <span className="text-black-50 d-block">{kisi.unvani}</span>
          </div>
        </div>
      </ButtonBase>
    </Fragment>
  );
};

const MusteriKisiler = props => {
  const dispatch = useDispatch();
  const kisiler = useSelector(state => state.experto.kisiler);
  const musteri = useSelector(state => state.experto.musteri);

  useEffect(() => {
    const load = async () => {
      if (props.match.params.id === 'yeni') {
      } else {
        dispatch(Actions.getMusteriKisiler(musteri._id));
      }
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
            {!kisiler && <LinearProgress />}

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Firma Kontak Kişiler"
              avatar={<PersonPinIcon />}
              action={
                <Tooltip title="Yeni">
                  <IconButton onClick={() => props.history.push(`/kisi/yeni?musteri=${musteri._id}`)}>
                    <AddIcon />
                  </IconButton>
                </Tooltip>
              }
            />
            <Divider />
            <CardContent>
              <MaterialTable
                title='Müşteri Kontak Kişiler'
                columns={[
                  { title: 'Adi', field: 'adi' },
                  { title: 'Soyadi', field: 'soyadi' },
                  { title: 'Ünvanı', field: 'unvani' },
                  //{title: 'Firma', field: 'musteri.marka'},
                  { title: 'Cep Telefon', field: 'cepTelefon' },
                  { title: 'Mail', field: 'mail' },
                  {
                    title: 'Detay',
                    render: rowData => (
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                          props.history.push(`/kisi/${rowData._id}`);
                        }}>
                        Detay
                      </Button>
                    )
                  },
                  {
                    title: 'Durum',
                    render: rowData => {
                      if (rowData.aktif) {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-info">
                            Aktif
                          </div>
                        );
                      } else {
                        return (
                          <div className="h-auto py-0 px-3 badge badge-warning">
                            Pasif
                          </div>
                        );
                      }
                    }
                  }
                ]}
                data={kisiler || []}
                options={{
                  pageSizeOptions: [5, 10, 20, 50, 100],
                  pageSize: 20,
                  actionsColumnIndex: -1
                }}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(MusteriKisiler);
