import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import RefreshIcon from '@material-ui/icons/Refresh';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  TableHead,
  Paper,
  TableRow,
  Table,
  TableCell,
  TableBody,
  Tooltip,
  Typography
} from '@material-ui/core';
import { withRouter } from 'react-router';

const MusteriUrunler = props => {
  const dispatch = useDispatch();
  const musteriUrunleri = useSelector(state => state.experto.musteriUrunler);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getMusteriUrunler(props.match.params.id));
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Müşteri Ürünleri"
              avatar={<MailOutlineIcon  />}
              action={
                <Tooltip title="Yenile">
                  <IconButton>
                    <RefreshIcon  />
                  </IconButton>
                </Tooltip>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                {musteriUrunleri &&
                  musteriUrunleri.map(u => {
                    return (
                    <Grid item xs={3}>
                      <Paper className="p-4">
                        <Typography>{u.urun.adi}</Typography>
                      </Paper>
                    </Grid>)
                  })}
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(MusteriUrunler)
