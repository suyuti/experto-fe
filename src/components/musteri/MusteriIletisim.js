import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Grid,
  TextField,
  CardHeader,
  Card,
  CardContent,
  IconButton,
  Divider
} from '@material-ui/core';
import GoogleMapReact from 'google-map-react';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PhoneInput from 'components/PhoneInput';
import AutoComplete from '@material-ui/lab/Autocomplete';
import { iller, ilceler } from 'components/utils/constants';

const MusteriIletisim = props => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(true);
  const musteri = useSelector(state => state.experto.musteri);
  const [seciliIl, setSeciliIl] = useState(iller.find(il => il.adi === musteri.iletisim.il));
  const [seciliIlce, setSeciliIlce] = useState(ilceler.find(ilce => ilce.adi === musteri.iletisim.ilce));
  const center = {
    lat: 41.009753,
    lng: 28.898915
  };
  const zoom = 11;

  useEffect(() => {
    const load = async () => {};
    load();
  }, [dispatch]);

  useEffect(() => {
    if (musteri) {
      //let il = iller.find(il => il.adi === musteri.iletisim.il)
      //setSeciliIl(il)
    }
  }, [musteri])

  if (!musteri) {
    return <></>;
  }

  console.log(seciliIl)

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={6} container spacing={3}>
          <Grid item xs={12}>
            <Card>
              <CardHeader
                title="İletişim Bİlgileri"
                avatar={<></>}
                action={
                  <IconButton>
                    <MoreVertIcon />
                  </IconButton>
                }
              />
              <Divider />
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <TextField
                      label="Firma Adresi"
                      fullWidth
                      multiline
                      rows={3}
                      variant="outlined" //{editMode ? 'outlined' : 'standard'}
                      //InputProps={{ readOnly: !editMode }}
                      value={musteri.iletisim.adres}
                      onChange={e => {
                        var m = { ...musteri };
                        m.iletisim.adres = e.target.value;
                        dispatch(Actions.setMusteri(m));
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Fabrika Adresi"
                      fullWidth
                      multiline
                      rows={3}
                      variant="outlined" // {editMode ? 'outlined' : 'standard'}
                      //InputProps={{ readOnly: !editMode }}
                      value={musteri.iletisim.fabrikaAdresi}
                      onChange={e => {
                        var m = { ...musteri };
                        m.iletisim.fabrikaAdresi = e.target.value;
                        dispatch(Actions.setMusteri(m));
                      }}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Fatura Adresi"
                      fullWidth
                      multiline
                      rows={3}
                      variant="outlined" //{editMode ? 'outlined' : 'standard'}
                      //InputProps={{ readOnly: !editMode }}
                      value={musteri.iletisim.faturaAdresi}
                      onChange={e => {
                        var m = { ...musteri };
                        m.iletisim.faturaAdresi = e.target.value;
                        dispatch(Actions.setMusteri(m));
                      }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <AutoComplete
                      options={iller}
                      autoHighlight
                      getOptionLabel={opt => opt.adi}
                      renderInput={params => (
                        <TextField
                          //error={error}
                          //helperText={error && error.atama}
                          fullWidth
                          {...params}
                          variant="outlined"
                          label="İl"
                          placeholder="İl"
                        />
                      )}
                      value={seciliIl }
                      onChange={(event, newValue) => {
                        if (!newValue) {
                          var m = { ...musteri };
                          m.iletisim.il = null;
                          m.iletisim.ilce = null;
                          dispatch(Actions.setMusteri(m));
                          setSeciliIl(newValue);
                          setSeciliIlce(null)
                        }
                        else {
                          var m = { ...musteri };
                          m.iletisim.il = newValue.adi;
                          dispatch(Actions.setMusteri(m));
                          setSeciliIl(newValue);
                        }
                      }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <AutoComplete
                      options={seciliIl ? ilceler.filter(i => i.il === seciliIl.id) : []}
                      autoHighlight
                      getOptionLabel={opt => opt.adi}
                      renderInput={params => (
                        <TextField
                          //error={error}
                          //helperText={error && error.atama}
                          fullWidth
                          {...params}
                          variant="outlined"
                          label="İlçe"
                          placeholder="İlçe"
                        />
                      )}
                      value={seciliIlce}
                      //value={musteri.iletisim.ilce}
                      onChange={(event, newValue) => {
                        var m = { ...musteri };
                        m.iletisim.ilce = newValue ? newValue.adi : null;
                        dispatch(Actions.setMusteri(m));
                        setSeciliIlce(newValue);
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Grid item xs={6} container spacing={3}>
          <Grid item xs={12}>
            <Card>
              <CardHeader title="Konum" />
              <Divider />
              <CardContent>
                <div className="w-100" style={{ height: '350px' }}>
                  <GoogleMapReact
                    defaultCenter={center}
                    defaultZoom={zoom}></GoogleMapReact>
                </div>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Card>
            <CardHeader title="" />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <TextField
                    label="Telefon"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    InputProps={{
                      //readOnly: !editMode,
                      inputComponent: PhoneInput
                    }}
                    value={musteri.iletisim.telefon}
                    onChange={e => {
                      var m = { ...musteri };
                      m.iletisim.telefon = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Fax"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.iletisim.fax}
                    onChange={e => {
                      var m = { ...musteri };
                      m.iletisim.fax = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="OSB"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.iletisim.osb}
                    onChange={e => {
                      var m = { ...musteri };
                      m.iletisim.osb = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Web Sitesi"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.iletisim.webSitesi}
                    onChange={e => {
                      var m = { ...musteri };
                      m.iletisim.webSitesi = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Mail"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.iletisim.mail}
                    onChange={e => {
                      var m = { ...musteri };
                      m.iletisim.mail = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Domain"
                    fullWidth
                    variant="outlined" ///{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.domain}
                    onChange={e => {
                      var m = { ...musteri };
                      m.domain = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriIletisim;
