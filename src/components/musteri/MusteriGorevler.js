import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import RefreshIcon from '@material-ui/icons/Refresh';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  TableHead,
  Paper,
  TableRow,
  Table,
  TableCell,
  TableBody,
  Tooltip,
  Typography,
  Chip,
  Button,
  Avatar
} from '@material-ui/core';
import { withRouter } from 'react-router';
import MaterialTable from 'material-table';
import moment from 'moment';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const MusteriGorevler = props => {
  const dispatch = useDispatch();
  const gorevler = useSelector(state => state.experto.gorevler);
  const personeller = useSelector(state => state.graph.personeller);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorevler({ musteri: props.match.params.id }));
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <MaterialTable
        title="Müşteri ile ilgili görevler"
        data={gorevler || []}
        columns={[
          { title: 'Konu', field: 'baslik' },
          {
            title: 'Son Tarih',
            field: 'sonTarih',
            customSort: (a, b) => {
              return moment(a.sonTarih) - moment(b.sonTarih);
            },
            render: (rowData, type) => {
              if (type === 'row') {
                return moment(rowData.sonTarih).format('DD.MM.yyyy');
              } else {
                return moment(rowData.sonTarih).format('DD.MM.yyyy');
              }
            }
          },
          {
            title: 'Sorumlu',
            render: row => {
              let personel = personeller.find(
                personel => personel.id === row.atananMsId
              );
              return (
                <Fragment>
                  <Chip
                    className="m-1"
                    avatar={<Avatar src={personel.photo} />}
                    label={`${personel.displayName}`}
                    variant="outlined"
                  />
                </Fragment>
              );
            }
          },
          { title: 'Kategori', field: 'gorevKategori' },
          { title: 'Öncelik', field: 'oncelik' },
          {
            title: 'Durum',
            field: 'sonuc',
            render: (rowData, type) => {
              if (type === 'row') {
                if (rowData.sonuc === 'acik') {
                  const today = new Date();
                  const sonTarih = new Date(rowData.sonTarih);

                  //if (today > sonTarih) {
                  //  return (
                  //    <div className="h-auto py-0 px-3 badge badge-danger">
                  //      Gecikmiş
                  //    </div>
                  //  );
                  //} else {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-info">
                      Açık
                    </div>
                  );
                  //}
                } else if (rowData.sonuc === 'basarili') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-success">
                      Başarılı
                    </div>
                  );
                } else if (rowData.sonuc === 'basarisiz') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-first">
                      Başarısız
                    </div>
                  );
                } else if (rowData.sonuc === 'iptal') {
                  return (
                    <div className="h-auto py-0 px-3 badge badge-warning">
                      İptal
                    </div>
                  );
                }
                return <div></div>;
              } else {
                return rowData;
              }
            }
          },
          {
            title: 'Gecikme',
            title: 'sonuc',
            render: (rowData, type) => {
              if (type === 'row') {
                if (rowData.sonuc === 'acik') {
                  const today = new Date();
                  const sonTarih = new Date(rowData.sonTarih);
                  today.setHours(0, 0, 0, 0);
                  sonTarih.setHours(0, 0, 0, 0);
                  if (today > sonTarih) {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-danger">
                        Gecikmiş
                      </div>
                    );
                  } else {
                    let kacGun = moment(sonTarih).diff(today, 'days');
                    return <>{kacGun === 0 ? 'Bugün son' : `${kacGun} gün`}</>;
                  }
                } else {
                  if (!rowData.sonucTarihi) {
                    return <></>;
                  }
                  const bitirmeTarihi = new Date(rowData.sonucTarihi);
                  const sonTarih = new Date(rowData.sonTarih);
                  bitirmeTarihi.setHours(0, 0, 0, 0);
                  sonTarih.setHours(0, 0, 0, 0);
                  if (bitirmeTarihi > sonTarih) {
                    let kacGun = moment(bitirmeTarihi).diff(sonTarih, 'days');
                    return (
                      <div className="h-auto py-0 px-3 badge badge-danger">
                        {kacGun} gün gecikme
                      </div>
                    );
                  } else {
                    return (
                      <div className="h-auto py-0 px-3 badge badge-info">
                        vaktinde
                      </div>
                    );
                  }
                }
              } else {
                return '';
              }
            }
          },
          {
            title: 'Ek',
            render: row => {
              if (row.attachments && row.attachments.length > 0) {
                return <AttachFileIcon />
              }
              else {
                return <></>
              }
            }
          }
        ]}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Detail',
            onClick: (event, rowData) =>
              props.history.push(`/gorev/${rowData._id}`),
            disabled: false // TODO Yetkisi yoksa goremeyecek
          }
        ]}
        options={{
          grouping: true,
          actionsColumnIndex: -1,
          pageSize: 20
        }}
      />
    </Fragment>
  );
};

export default withRouter(MusteriGorevler);
