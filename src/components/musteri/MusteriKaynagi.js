import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Grid,
  Card,
  CardHeader,
  IconButton,
  Divider,
  CardContent,
  TextField,
  MenuItem,
  Button
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import PersonelCard from 'components/personel/PersonelCard';

const Reklam = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField label="Reklam Alanı" variant="outlined" fullWidth />
          <Grid item xs={12}>
            <TextField label="Reklam Zamanı" variant="outlined" fullWidth />
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};

const Mail = props => {
  return (
    <Fragment>
      <Grid container spacing={3}></Grid>
    </Fragment>
  );
};

const TemsilciReferansi = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <PersonelCard user={{}} />
        </Grid>
      </Grid>
    </Fragment>
  );
};

const PersonelYonlendirme = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField label="Personel" variant="outlined" fullWidth />
        </Grid>
      </Grid>
    </Fragment>
  );
};

const PartnerYonlendirme = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={10}>
          <TextField label="Partner" variant="outlined" fullWidth />
        </Grid>
        <Grid item xs={2}>
          <Button variant="contained">Partner Ekle</Button>
        </Grid>
      </Grid>
    </Fragment>
  );
};

const Referans = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField label="Referans Müşteri" variant="outlined" fullWidth />
        </Grid>
      </Grid>
    </Fragment>
  );
};

const Web = props => {
  return (
    <Fragment>
      <Grid container spacing={3}></Grid>
    </Fragment>
  );
};

const Fuar = props => {
  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField label="Hangi Fuar" variant="outlined" fullWidth />
        </Grid>
      </Grid>
    </Fragment>
  );
};

const MusteriKaynagi = props => {
  const dispatch = useDispatch();
  const musteri = useSelector(state => state.experto.musteri);
  const [editMode, setEditMode] = useState(true);
  const [selectedType, setSelectedType] = useState(null);

  useEffect(() => {
    const load = async () => {};
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              title="Müşteri Kaynağı"
              avatar={<SwapHorizIcon fontSize="large" />}
              action={
                <IconButton>
                  <MoreVertIcon fontSize="large" />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <TextField
                    label="Müşteri Kaynağı"
                    fullWidth
                    variant={editMode ? 'outlined' : 'standard'}
                    InputProps={{ readOnly: !editMode }}
                    value={musteri.firmaBilgi.vergiDairesi}
                    onChange={e => {
                      setSelectedType(e.target.value);
                      var m = { ...musteri };
                      //m.marka = e.target.value;
                      //dispatch({ type: SET_MUSTERI, musteri: m });
                    }}
                    select>
                    <MenuItem key="reklam" value="reklam">
                      Reklam
                    </MenuItem>
                    <MenuItem key="mail" value="mail">
                      Mailing
                    </MenuItem>
                    <MenuItem key="temsilciref" value="temsilciref">
                      Müşteri Temsilci Referansı
                    </MenuItem>
                    <MenuItem key="partner" value="partner">
                      Partner Yönlendirmesi
                    </MenuItem>
                    <MenuItem key="personelref" value="personelref">
                      İç Personel Yönlendirme
                    </MenuItem>
                    <MenuItem key="ref" value="ref">
                      Referans
                    </MenuItem>
                    <MenuItem key="web" value="web">
                      Web Sitesi
                    </MenuItem>
                    <MenuItem key="fuar" value="fuar">
                      Fuar
                    </MenuItem>
                  </TextField>
                </Grid>
                <Grid item xs={12}>
                  {selectedType === 'reklam' && <Reklam />}
                  {selectedType === 'mail' && <Mail />}
                  {selectedType === 'temsilciref' && <TemsilciReferansi />}
                  {selectedType === 'partner' && <PartnerYonlendirme />}
                  {selectedType === 'personelref' && <PersonelYonlendirme />}
                  {selectedType === 'ref' && <Referans />}
                  {selectedType === 'web' && <Web />}
                  {selectedType === 'fuar' && <Fuar />}
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default MusteriKaynagi;
