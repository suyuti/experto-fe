import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import IOSSwitch from '../IosSwitch';
import {
  Grid,
  TextField,
  Chip,
  Card,
  CardHeader,
  Divider,
  Tooltip,
  CardContent,
  IconButton,
  Avatar,
  Menu,
  FormControlLabel,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  Checkbox,
  MenuItem,
  FormGroup,
  Typography
} from '@material-ui/core';
import AutoComplete from '@material-ui/lab/Autocomplete';
//import IOSSwitch from 'components/IOSSwitch';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import CloseIcon from '@material-ui/icons/Close';
import PersonelCard from 'components/personel/PersonelCard';
import PersonelList from 'components/personel/PersonelList';

const MusteriInfo = props => {
  const dispatch = useDispatch();
  const musteri = useSelector(state => state.experto.musteri);
  const personeller = useSelector(state => state.graph.personeller);
  const sektorler = useSelector(state => state.experto.sektorler);
  const [editMode, setEditMode] = useState(false);
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const [temsilci, setTemsilci] = useState(null);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getSektorler());
    };
    load();
  }, [dispatch]);

  useEffect(() => {
    if (musteri) {
      if (musteri.musteriTemsilcisi && personeller) {
        let p = personeller.find(p => p.id === musteri.musteriTemsilcisi);
        if (p) {
          setTemsilci(p);
        }
      }
    }
  }, [musteri]);

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorElMenu(null);
  };

  const onSorumluPersonelSelected = id => {
    var m = { ...musteri };
    m.musteriTemsilcisi = id;
    dispatch(Actions.setMusteri(m));
    handleCloseMenu();
  };

  const personelMenu = (
    <Menu
      anchorEl={anchorElMenu}
      keepMounted
      open={Boolean(anchorElMenu)}
      onClose={handleCloseMenu}
      getContentAnchorEl={null}
      classes={{ list: 'p-0' }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <PersonelList
        title="Atanacak Personel"
        onSelect={id => {
          onSorumluPersonelSelected(id);
        }}
        users={personeller}
      />
    </Menu>
  );

  if (!musteri) {
    return <></>;
  }
  if (!personeller) {
    return <></>;
  }
  if (!sektorler) {
    return <></>;
  }


  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Card>
            <CardHeader
              title="Genel Bilgiler"
              avatar={<AccountBalanceIcon />}
              action={
                <div className="d-flex flex-row">
                  <IconButton>
                    <MoreVertIcon />
                  </IconButton>
                </div>
              }
            />
            <Divider />
            <CardContent>
              <Grid container spacing={3}>
                {musteri.firmaKapandi && (
                  <Grid item xs={12} className="text-center">
                    <Typography variant="h2" className="p-4 border">
                      Firma Kapandı
                    </Typography>
                  </Grid>
                )}
                <Grid item xs={12}>
                  <TextField
                    label="Marka"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.marka}
                    onChange={e => {
                      var m = { ...musteri };
                      m.marka = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="Firma Ünvanı"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.unvan}
                    onChange={e => {
                      var m = { ...musteri };
                      m.unvan = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="KEP Adresi"
                    fullWidth
                    variant="outlined" //{editMode ? 'outlined' : 'standard'}
                    //InputProps={{ readOnly: !editMode }}
                    value={musteri.KepAdresi}
                    onChange={e => {
                      var m = { ...musteri };
                      m.KepAdresi = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <AutoComplete
                    options={sektorler}
                    getOptionLabel={opt => opt.adi}
                    selectOnFocus
                    clearOnBlur
                    handleHomeEndKeys
                    value={musteri.sektor}
                    onChange={(event, newValue) => {
                      dispatch(
                        Actions.setMusteri({
                          sektor: newValue ? newValue : null
                        })
                      );
                    }}
                    freeSolo
                    renderTags={(tagValue, getTagProps) =>
                      tagValue.map((opt, index) => (
                        <Chip
                          avatar={<Avatar src={opt.photo} />}
                          label={opt.displayName}
                          variant="outlined"
                          {...getTagProps({ index })}
                        />
                      ))
                    }
                    renderInput={params => (
                      <TextField
                        //error={error}
                        //helperText={error && error.atama}
                        fullWidth
                        {...params}
                        variant="outlined"
                        label="Sektörü"
                        placeholder="Sektörü"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        //disabled={!editMode}
                        checked={musteri.argeMerkezi}
                        onChange={e => {
                          dispatch(
                            Actions.setMusteri({
                              argeMerkezi: e.target.checked
                            })
                          );
                        }}
                        name="argeMerkeziChk"
                        color="primary"
                      />
                    }
                    label="Ar-Ge Merkezi"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        //disabled={!editMode}
                        checked={musteri.tasarimMerkezi}
                        onChange={e => {
                          dispatch(
                            Actions.setMusteri({
                              tasarimMerkezi: e.target.checked
                            })
                          );
                        }}
                        name="tasarimMerkeziChk"
                        color="primary"
                      />
                    }
                    label="Tasarım Merkezi"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        //disabled={!editMode}
                        checked={musteri.teknoparkFirmasi}
                        onChange={e => {
                          dispatch(
                            Actions.setMusteri({
                              teknoparkFirmasi: e.target.checked
                            })
                          );
                        }}
                        name="teknoparkFirmasiChk"
                        color="primary"
                      />
                    }
                    label="Teknopark Firması"
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControl component="fieldset" className="mt-2">
                    <FormLabel>Hangi firmamızın müşterisi</FormLabel>
                    <FormGroup row>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={
                              musteri.hangiFirmamizdan &&
                              musteri.hangiFirmamizdan.experto
                                ? musteri.hangiFirmamizdan.experto
                                : false
                            }
                            onChange={e => {
                              var m = { ...musteri };
                              if (!m.hangiFirmamizdan) {
                                m.hangiFirmamizdan = {};
                              }
                              m.hangiFirmamizdan.experto = e.target.checked;
                              dispatch(Actions.setMusteri(m));
                            }}
                            name="expertoChk"
                            color="primary"
                          />
                        }
                        label="Experto"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={
                              musteri.hangiFirmamizdan &&
                              musteri.hangiFirmamizdan.expertoOsgb
                                ? musteri.hangiFirmamizdan.expertoOsgb
                                : false
                            }
                            onChange={e => {
                              var m = { ...musteri };
                              if (!m.hangiFirmamizdan) {
                                m.hangiFirmamizdan = {};
                              }
                              m.hangiFirmamizdan.expertoOsgb = e.target.checked;
                              dispatch(Actions.setMusteri(m));
                            }}
                            name="expertoOsgbChk"
                            color="primary"
                          />
                        }
                        label="Experto Osgb"
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={
                              musteri.hangiFirmamizdan &&
                              musteri.hangiFirmamizdan.expertoSgk
                                ? musteri.hangiFirmamizdan.expertoSgk
                                : false
                            }
                            onChange={e => {
                              var m = { ...musteri };
                              if (!m.hangiFirmamizdan) {
                                m.hangiFirmamizdan = {};
                              }
                              m.hangiFirmamizdan.expertoSgk = e.target.checked;
                              dispatch(Actions.setMusteri(m));
                            }}
                            name="expertoSgkChk"
                            color="primary"
                          />
                        }
                        label="Experto SGK"
                      />
                    </FormGroup>
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    label="Durum"
                    variant="outlined"
                    fullWidth
                    select
                    onChange={e => {
                      var m = { ...musteri };
                      m.durum = e.target.value;
                      dispatch(Actions.setMusteri(m));
                    }}
                    value={musteri.durum}>
                    <MenuItem key="aktif" value="aktif">
                      Aktif
                    </MenuItem>
                    <MenuItem key="pasif" value="pasif">
                      Pasif
                    </MenuItem>
                    <MenuItem key="potansiyel" value="potansiyel">
                      Potansiyel
                    </MenuItem>
                    <MenuItem key="sorunlu" value="sorunlu">
                      Sorunlu
                    </MenuItem>
                  </TextField>
                </Grid>
                <Grid item xs={6}>
                  <FormControlLabel
                    control={
                      <IOSSwitch
                        checked={musteri.firmaKapandi}
                        onChange={e => {
                          let checked = e.target.checked;
                          var m = { ...musteri };
                          m.firmaKapandi = checked;
                          if (checked) {
                            m.durum = 'pasif'
                          }
                          dispatch(Actions.setMusteri(m));
                        }}
                        name="checkedB"
                      />
                    }
                    label={
                      musteri.firmaKapandi ? 'Firma kapandı' : 'Firma aktif'
                    }
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card>
            <CardHeader
              title="Müşteri Sorumlusu"
              avatar={<PersonOutlineIcon fontSize="large" />}
              action={
                <IconButton onClick={handleClickMenu}>
                  <MoreVertIcon fontSize="large" />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              {temsilci && <PersonelCard user={temsilci} />}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      {personelMenu}
    </Fragment>
  );
};

export default MusteriInfo;
