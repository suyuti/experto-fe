import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import RefreshIcon from '@material-ui/icons/Refresh';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  TableHead,
  Paper,
  TableRow,
  Table,
  TableCell,
  TableBody,
  Tooltip,
  Typography,
  Avatar,
  Button,
  Chip
} from '@material-ui/core';
import { withRouter } from 'react-router';
import MaterialTable from 'material-table';
import moment from 'moment';
import AvatarGroup from '@material-ui/lab/AvatarGroup';

const MusteriToplantilar = props => {
  const dispatch = useDispatch();
  const toplantilar = useSelector(state => state.expertoToplanti.toplantilar);
  const personeller = useSelector(state => state.graph.personeller);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getToplantilar({ firma: props.match.params.id }));
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <MaterialTable
        title="Müşteri ile yapılan toplantılar"
        data={toplantilar || []}
        columns={[
          { title: 'Konu', field: 'baslik' },
          {
            title: 'Tarih',
            render: row => moment(row.tarih).format('DD.MM.yyyy')
          },
          {
            title: 'Experto Katılımcılar',
            render: row => {
              return (
                <Fragment>
                  {row.expertoKatilimcilar.map(p => {
                    let personel = personeller.find(
                      personel => personel.id === p
                    );
                    if (personel) {
                      return (
                        <Chip
                          className="m-1"
                          avatar={<Avatar src={personel.photo} />}
                          label={`${personel.displayName}`}
                          variant="outlined"
                        />
                      );
                    }
                    return <></>;
                  })}
                </Fragment>
              );
            }
          },
          {
            title: 'Firma Katılımcılar',
            render: row => (
              <Fragment>
                {row.firmaKatilimcilar.map(k => (
                  <Chip
                    className="m-1"
                    avatar={<Avatar />}
                    label={`${k.adi} ${k.soyadi} [${k.unvani}]`}
                    variant="outlined"
                  />
                ))}
              </Fragment>
            )
          },
          {title: 'Kategori', field: 'kategori'},

          {
            title: 'Kararlar',
            render: rowData => {
              const today = new Date();
              const topTar = new Date(rowData.tarih);

              return (
                <Button
                  disabled={today < topTar}
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    dispatch(Actions.getToplanti(rowData._id));
                    props.history.push(`/toplanti/${rowData._id}/karar`);
                  }}>
                  Kararlar
                </Button>
              );
            }
          },


        ]}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Detail',
            onClick: (event, rowData) =>
              props.history.push(`/toplanti/${rowData._id}`),
            disabled: false // TODO Yetkisi yoksa goremeyecek
          }
        ]}

        options={{
          grouping: true,
          actionsColumnIndex: -1,
          pageSize: 20
        }}
      />
    </Fragment>
  );
};

export default withRouter(MusteriToplantilar);
