import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import RefreshIcon from '@material-ui/icons/Refresh';
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  Avatar,
  TableHead,
  TableRow,
  Table,
  TableCell,
  TableBody,
  Tooltip
} from '@material-ui/core';
import MaterialTable from 'material-table';
import { withRouter } from 'react-router';
import moment from 'moment'

const MusteriYazismalar = props => {
  const dispatch = useDispatch();
  const gorusmeler = useSelector(state => state.gorusme.gorusmeler)
  const personeller = useSelector(state => state.graph.personeller);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getGorusmeler({musteri: props.match.params.id}))
    };
    load();
  }, [dispatch]);

  return (
    <Fragment>
      <MaterialTable
        title="Müşteri ile yapılan görüşmeler"
        data={gorusmeler || []}
        columns={[
          { title: 'Konu', field: 'konu' },
          { title: 'Zaman', render: rowData => `${moment(rowData.tarih).format('DD.mm.yyyy')} ${rowData.saat}` },
          { title: 'Kanal', field: 'kanal' },
          {
            title: 'Görüşme yapan',
            render: rowData => {
              if (rowData.gorusmeYapanMsId) {
                let p = personeller.find(
                  p => p.id === rowData.gorusmeYapanMsId
                );
                if (p) {
                  return (
                    <div className="d-flex align-items-center">
                      <Avatar src={p.photo} />
                      <div className="ml-2">{p.displayName}</div>
                    </div>
                  );
                }
              }
            }
          },
      {
            title: 'Görüşülen Kişi',
            render: rowData => (
              <div className="">
                <div className="text-500">{`${rowData.kisi.adi} ${rowData.kisi.soyadi}`}</div>
                <div className="">{`${rowData.kisi.unvani}`}</div>
              </div>
            )
          },
    ]}
      />
    </Fragment>
  );
};

export default withRouter(MusteriYazismalar);
