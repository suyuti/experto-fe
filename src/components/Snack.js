import React from 'react';
import { Snackbar, IconButton, Button } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import CloseIcon from '@material-ui/icons/Close'
import * as Actions from '../store/actions'

const Snack = props => {
    const open = useSelector(state => state.app.showMessage);
    const message = useSelector(state => state.app.message);
    const dispatch = useDispatch();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch(Actions.hideMessage());
  };
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left'
      }}
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
      message={message}
      action={
        <React.Fragment>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </React.Fragment>
      }
    />
  );
};
export default Snack;
