import React, { Fragment } from 'react';
import MaskedInput from 'react-text-mask';

const PhoneInput = props => {
  const { inputRef, ...other } = props;
  return (
    <Fragment>
      <MaskedInput
        {...other}
        ref={ref => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={[
          '(',
          /[1-9]/,
          /\d/,
          /\d/,
          ')',
          ' ',
          /\d/,
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/,
          ' ',
          /\d/,
          /\d/
        ]}
        placeholderChar={'\u2000'}
        showMask
      />
    </Fragment>
  );
};

export default PhoneInput;
