import React, { Fragment, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import moment from 'moment';
import {
  Grid,
  Divider,
  Card,
  CardHeader,
  CardContent,
  TextField,
  List,
  ListItem,
  MenuItem,
  Button,
  CardActions,
  Avatar,
  IconButton,
  Checkbox,
  Menu,
  Tooltip,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  FormControlLabel,
  Typography,
  makeStyles
} from '@material-ui/core';
import clsx from 'clsx';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';

const useStyles = makeStyles(theme => ({
  small: {
    width: theme.spacing(4),
    height: theme.spacing(4)
  },
  displayName: {
    fontSize: '0.7rem'
  },
  tarih: {
    fontSize: '0.6rem'
  },
  atamaItem: {
    display: 'flex',
    justifyContent: 'between'
  },
  timeline: {
    height: '280px'
  }
}));

const Timeline = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useSelector(state => state.experto.history);
  const personeller = useSelector(state => state.graph.personeller);

  const atamaItem = item => {
    let p1 = personeller.find(p => p.id === item.oncekiDurum.message);
    let p2 = personeller.find(p => p.id === item.sonrakiDurum.message);
    return (
      <div className="">
        Atama
        <div className={classes.atamaItem}>
          <Tooltip title={p1.displayName}>
            <Avatar src={p1.photo} className={classes.small} />
          </Tooltip>
          <ArrowForwardIcon />
          <Tooltip title={p2.displayName}>
            <Avatar src={p2.photo} className={classes.small} />
          </Tooltip>
        </div>
      </div>
    );
  };

  const olusturmaItem = item => {
    return (
      <div className="">
        Oluşturuldu
      </div>
    );
  };

  return (
    <Fragment>
      <Card className="card-box mb-4">
        <CardHeader title="Geçmiş işlemler" avatar={<HourglassEmptyIcon />} />
        <Divider />
        <CardContent className="p-3">
          <div className={classes.timeline}>
            <PerfectScrollbar>
              <div className="timeline-list timeline-list-offset timeline-list-offset-dot">
                {history &&
                  history.map(h => {
                    let user = personeller.find(u => u.id == h.islemYapanMsId);
                    var item = <></>;
                    if (h.message === 'Atama') {
                      item = atamaItem(h);
                    }
                    else if (h.message === 'Olusturma') {
                      item = olusturmaItem(h);
                    }
                    return (
                      <div className="timeline-item">
                        <div
                          className={clsx(
                            classes.tarih,
                            'timeline-item-offset'
                          )}>
                          {moment(h.tarih).format('HH.mm DD.MM.YYYY')}
                        </div>

                        <div className="timeline-item--content">
                          <div className="timeline-item--icon"></div>
                          <div className="d-flex justify-content-between align-items-center">
                            <h4 className="timeline-item--label mb-2 flex-grow-1">
                              {item}
                            </h4>
                            <div>
                              <Tooltip title={user.displayName}>
                                <Avatar
                                  src={user.photo}
                                  className={classes.small}
                                />
                              </Tooltip>
                              <div className={classes.displayName}>
                                {user.displayName}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </PerfectScrollbar>
          </div>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default Timeline;
