import React, { Fragment } from 'react';
import MaskedInput from 'react-text-mask';
import NumberFormat from 'react-number-format';

const MoneyInput = props => {
  const { inputRef, onChange, ...other } = props;
  return (
    <Fragment>
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={values => {
          onChange({
            target: {
              value: values.value
            }
          });
        }}
        thousandSeparator
        isNumericString
        suffix=" TL"
      />
    </Fragment>
  );
};

export default MoneyInput;
