import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Card,
  CardContent,
  CardActions,
  Button,
  CardHeader,
  Divider,
  IconButton

} from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroll-component';
import { BarLoader } from 'react-spinners';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import RefreshIcon from '@material-ui/icons/Refresh';

const MailList = props => {
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const myMails = useSelector(state => state.graph.myMails);

  useEffect(() => {
    const load = async () => {
      dispatch(Actions.getUserMails(me.id));
    };
    load();
  }, [dispatch]);

  if (!myMails) {
    return <></>;
  }
  const getMoreMails = () => {};

  return (
    <Fragment>
      <div>
        <Card>
          <CardHeader
            title={<Typography>Mesajlar</Typography>}
            subheader="Maillerim"
            avatar={<MailOutlineIcon fontSize='large'/>}
            action={
                <IconButton>
                    <RefreshIcon fontSize='large' />
                </IconButton>
            }
          />
          <Divider />
          <CardContent>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell>Mail</TableCell>
                  <TableCell>Kimden</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <InfiniteScroll
                  dataLength={myMails.length}
                  next={getMoreMails}
                  loader={
                    <div className="d-flex align-items-center justify-content-center py-3">
                      <BarLoader color={'var(--primary)'} loading={true} />
                    </div>
                  }
                  height={380}>
                  {myMails &&
                    myMails.map(m => {
                      return (
                        <TableRow>
                          <TableCell>{m.subject}</TableCell>
                        </TableRow>
                      );
                    })}
                </InfiniteScroll>
              </TableBody>
            </Table>
          </CardContent>
          <CardActions>
            <Button variant="outlined" onClick={getMoreMails}>
              Daha fazla
            </Button>
          </CardActions>
        </Card>
      </div>
    </Fragment>
  );
};

export default MailList;
