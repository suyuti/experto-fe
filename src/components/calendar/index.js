import React, { useEffect, useState } from 'react';
import {
  Dialog,
  Card,
  Button,
  Tabs,
  Tab,
  IconButton,
  CardContent,
  CardActions,
  Input,
  InputLabel,
  FormControlLabel,
  InputAdornment,
  LinearProgress,
  FormControl,
  Checkbox,
  Grid,
  Tooltip,
  CardHeader,
  Divider,
  Typography,
  TextField,
  Avatar
} from '@material-ui/core';
import EventIcon from '@material-ui/icons/Event';
import TabPanel from '../TabPanel';
import { Calendar, Views, momentLocalizer } from 'react-big-calendar';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import MailOutlineTwoToneIcon from '@material-ui/icons/MailOutlineTwoTone';
import LockTwoToneIcon from '@material-ui/icons/LockTwoTone';
import * as Actions from '../../store/actions';
import EventDialog from './eventDialog';

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: '#eaf6ff'
    }
  });

const localizer = momentLocalizer(moment);
let allViews = Object.keys(Views).map(k => Views[k]);

const MyCalendar = props => {
  const dispatch = useDispatch();
  const toplantilar = useSelector(state => state.expertoToplanti.toplantilar);
  const gorevler = useSelector(state => state.experto.gorevler);
  const personeller = useSelector(state => state.graph.personeller);
  const [showEventDlg, setShowEventDlg] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState(null);


  const eventPropGetter = (event, start, end, isSelected) => {
    console.log(event);
    var style = {};
    if (event.type === 'gorev') {
      style = {
        backgroundColor: `#EFB4C4`,
        borderRadius: '5px',
        opacity: 1,
        fontSize: `1rem`,
        color: 'black',
        border: '0px',
        display: 'block'
      };
    } else if (event.type === 'toplanti') {
      style = {
        backgroundColor: `#AFCEF1`,
        borderRadius: '0px',
        opacity: 1,
        fontSize: `1rem`,
        color: 'black',
        border: '0px',
        display: 'block'
      };
    }
    return {
      style: style
    };
  };

  if (!toplantilar || !gorevler) {
    return <></>;
  }

  return (
    <div>
      <Calendar
        localizer={localizer}
        selectable
        views={allViews}
        step={60}
        showMultiDayTimes
        defaultDate={new Date()}
        components={{
          timeSlotWrapper: ColoredDateCellWrapper
        }}
        events={[
          ...toplantilar.map(t => {
            return {
              id: t._id,
              title: t.baslik,
              start: t.tarih,
              end: moment(t.tarih).add(1, 'hour'),
              type: 'toplanti',
              konu: t.baslik,
              aciklama: t.aciklama,
              tarih: moment(t.tarih).format('DD.MM.yyyy'),
              firma: !t.firma ? 'İç Toplantı' : t.firma.marka || '',
              kategori: t.kategori,
              id: t._id
            };
          }),
          ...gorevler.map(g => {
            return {
              id: g._id,
              title: g.baslik,
              start: g.sonTarih,
              end: moment(g.sonTarih).add(1, 'hour'),
              type: 'gorev',
              konu: g.baslik,
              aciklama: g.aciklama,
              tarih: moment(g.sonTarih).format('DD.MM.yyyy'),
              firma: g.icGorevMi ? 'İç Görev' : (g.musteri ? g.musteri.marka : ''),
              kategori: g.gorevKategori,
              id: g._id
            };
          })
        ]}
        startAccessor="start"
        endAccessor="end"
        style={{ minHeight: 650 }}
        onSelectEvent={event => {
          setSelectedEvent(event);
          setShowEventDlg(true);
        }}
        eventPropGetter={eventPropGetter}
      />
      {Boolean(selectedEvent) && (
        <Dialog
          fullWidth
          scroll="body"
          maxWidth="lg"
          open={showEventDlg /*&& Boolean(selectedToplanti)*/}
          onClose={() => {
            setSelectedEvent(null);
            setShowEventDlg(false);
          }}>
          <EventDialog
            title={selectedEvent.type === 'gorev' ? 'Görev' : 'Toplantı'}
            konu={selectedEvent.konu}
            aciklama={selectedEvent.aciklama}
            tarih={selectedEvent.tarih}
            firma={selectedEvent.firma}
            kategori={selectedEvent.kategori}
            id={selectedEvent.id}
            type={selectedEvent.type}
          />
        </Dialog>
      )}
    </div>
  );
};

export default MyCalendar;
