import React, { Fragment } from 'react';
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  CardActions,
  Grid,
  TextField,
  Button
} from '@material-ui/core';
import {withRouter} from 'react-router'

const EventDialog = props => {
  const { title, konu, aciklama, tarih, firma, kategori, id, type } = props;

  return (
    <Fragment>
      <Card>
        <CardHeader title={title} />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                label="Konu"
                fullWidth
                variant="outlined"
                InputProps={{
                  readOnly: true
                }}
                value={konu}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Açıklama"
                fullWidth
                variant="outlined"
                multiline
                rows={3}
                InputProps={{
                  readOnly: true
                }}
                value={aciklama}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Tarih"
                fullWidth
                variant="outlined"
                InputProps={{
                  readOnly: true
                }}
                value={tarih}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Firma"
                fullWidth
                variant="outlined"
                InputProps={{
                  readOnly: true
                }}
                value={firma}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Kategori"
                fullWidth
                variant="outlined"
                InputProps={{
                  readOnly: true
                }}
                value={kategori}
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button variant="contained" color='primary' >Kapat</Button>
          <Button variant="contained" color='primary' onClick={(e) => {
              if (type === 'gorev') {
                props.history.push(`/gorev/${id}`)
            }
            else if (type === 'toplanti') {
                props.history.push(`/toplanti/${id}`)
            }
      }}>Detay</Button>
        </CardActions>
      </Card>
    </Fragment>
  );
};

export default withRouter(EventDialog);
