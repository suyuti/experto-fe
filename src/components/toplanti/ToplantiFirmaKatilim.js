import React, { Fragment, useState, useEffect } from 'react';
import AutoComplete, {
  createFilterOptions
} from '@material-ui/lab/Autocomplete';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  DialogContentText,
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Grid,
  IconButton,
  Avatar,
  Typography,
  Chip
} from '@material-ui/core';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import { useSelector, useDispatch } from 'react-redux';
import { SET_TOPLANTI } from 'store/actions';
import * as Actions from 'store/actions';
import AddIcon from '@material-ui/icons/Add';
import PhoneInput from 'components/PhoneInput';

const filter = createFilterOptions();

const ToplantiFirmaKatilim = props => {
  const dispatch = useDispatch();
  //const musteri = useSelector(state => state.experto.musteri);
  const kisiler = useSelector(state => state.experto.kisiler);
  const error = useSelector(state => state.app.error);
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const [atananlar, setAtananlar] = useState([]);
  const [showYeniKisi, setShowYeniKisi] = useState(false);
  const [form, setForm] = useState({});

  useEffect(() => {
    if (toplanti.firma) {
      var f = { ...form };
      f.musteri = toplanti.firma._id;
      setForm(f);
    }
  }, [toplanti.firma]);

  useEffect(() => {
    if (!kisiler) {
      return
    }
    var _atananlar = [];
    for (let k of toplanti.firmaKatilimcilar) {
      let p = kisiler.find(p => p._id === k);
      if (p) {
        _atananlar.push(p);
      }
    }
    setAtananlar(_atananlar);
  }, [toplanti.firmaKatilimcilar, kisiler]);

  const yeniKisiEkle = () => {
    dispatch(Actions.saveKisi(form));
    setShowYeniKisi(false);
  };

  return (
    <Fragment>
      <Card className="card-box">
        <CardHeader
          title="Firma Katılımcıları"
          avatar={<PersonPinIcon />}
          action={
            <IconButton
              disabled={!toplanti.firma}
              onClick={() => {
                setShowYeniKisi(true);
              }}>
              <AddIcon />
            </IconButton>
          }
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <AutoComplete
                disabled={!toplanti.firma}
                multiple
                options={kisiler || []}
                getOptionLabel={option => {
                  return `${option.adi} ${option.soyadi} [${option.unvani}]`;
                }}
                filterSelectedOptions
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                value={atananlar}
                //freeSolo
                onChange={(event, newValue) => {
                  const atananlar = newValue.map(a => a._id);
                  dispatch({
                    type: SET_TOPLANTI,
                    toplanti: { firmaKatilimcilar: atananlar }
                  });
                  //}
                }}
                //filterOptions={(options, params) => {
                //  const filtered = filter(options, params);
                //  if (params.inputValue !== '') {
                //    filtered.push({
                //      inputValue: params.inputValue,
                //      title: `Yeni Firma Personeli Ekle "${params.inputValue}"`
                //    });
                //  }
                //  return filtered;
                //}}
                renderTags={(tagValue, getTagProps) =>
                  tagValue.map((opt, index) => (
                    <Chip
                      avatar={<Avatar src={opt.photo} />}
                      label={`${opt.adi} ${opt.soyadi} [${opt.unvani}]`}
                      variant="outlined"
                      {...getTagProps({ index })}
                    />
                  ))
                }
                renderInput={params => (
                  <TextField
                    error={error && error.firmaKatilimcilar}
                    helperText={error && error.firmaKatilimcilar}
                    fullWidth
                    {...params}
                    variant="outlined"
                    label="Firma personeli"
                    placeholder="Firma personeli"
                  />
                )}
              />
            </Grid>
            <Grid item xs={1}></Grid>
          </Grid>
        </CardContent>
      </Card>
      {showYeniKisi && (
        <Dialog
          open={showYeniKisi}
          onClose={() => {
            setShowYeniKisi(false);
          }}
          aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
            Firmaya Yeni Kişi Ekle
          </DialogTitle>
          <Divider />
          <DialogContent>
            <DialogContentText></DialogContentText>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  autoFocus
                  label="Adı"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.adi = e.target.value;
                    setForm(f);
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  label="Soyadı"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.soyadi = e.target.value;
                    setForm(f);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Ünvanı"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.unvani = e.target.value;
                    setForm(f);
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  label="Cep Telefonu"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.cepTelefon = e.target.value;
                    setForm(f);
                  }}
                  InputProps={{
                    inputComponent: PhoneInput
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  label="Sabit Telefonu"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.isTelefon = e.target.value;
                    setForm(f);
                  }}
                  InputProps={{
                    inputComponent: PhoneInput
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label="Mail Adresi"
                  variant="outlined"
                  onChange={e => {
                    var f = { ...form };
                    f.mail = e.target.value;
                    setForm(f);
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <Divider />
          <DialogActions>
            <Button
              onClick={() => {
                setShowYeniKisi(false);
              }}
              variant="contained"
              color="primary">
              İptal
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                yeniKisiEkle();
              }}>
              Ekle
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </Fragment>
  );
};

export default ToplantiFirmaKatilim;
