import React, { Fragment } from 'react';
import { Grid } from '@material-ui/core';
import MyCalendar from '../calendar';

const ToplantiTakvim = props => {
  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <MyCalendar />
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default ToplantiTakvim;
