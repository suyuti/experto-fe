//import moment from 'moment'
const moment = require('moment')

exports.validate = (toplanti) => {
    debugger
    var error = null
    if (toplanti.baslik === null || toplanti.baslik.trim().length === 0) {
        error = error || {}
        error.baslik = 'Başlık yazmalısınız'
    }
    if (toplanti.aciklama === null || toplanti.aciklama.trim().length === 0) {
        error = error || {}
        error.aciklama = 'Açıklama yazmalısınız'
    }
    if (!moment(toplanti.tarih).isValid()) {
        error = error || {}
        error.tarih = 'Toplantı tarihi belirtmelisiniz'
    }
    if (!toplanti.icToplantiMi && !toplanti.firma) {
        error = error || {}
        error.musteri = 'Toplantı için müşteri seçmelisiniz'
    }
    if (toplanti.expertoKatilimcilar.length === 0) {
        error = error || {}
        error.expertoKatilimcilar = 'Katılımcı seçmelisiniz'
    }
    if (!toplanti.icToplantiMi && toplanti.firmaKatilimcilar.length === 0) {
        error = error || {}
        error.firmaKatilimcilar = 'Firma Katılımcısı seçmelisiniz'
    }
    //if (!toplanti.atananMsId) {
    //    error = error || {}
    //    error.atama = 'Sorumlu atamalısınız'
    //}
    if (!toplanti.saat) {
        error = error || {}
        error.saat = 'Toplantı saati belirtmelisiniz'
    }
    if (!toplanti.sure) {
        error = error || {}
        error.sure = 'Toplantı süresi belirtmelisiniz'
    }
    if (!toplanti.tur) {
        error = error || {}
        error.tur = 'Toplantı türü belirtmelisiniz'
    }
    if (!toplanti.yer) {
        error = error || {}
        error.yer = 'Toplantı konumu belirtmelisiniz'
    }
    if (!toplanti.kategori) {
        error = error || {}
        error.kategori = 'Toplantı kategorisi belirtmelisiniz'
    }

    return error
}