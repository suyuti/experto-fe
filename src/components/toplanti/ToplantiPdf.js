import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';

import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  PDFDownloadLink
} from '@react-pdf/renderer';
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});

const ToplantiPdf = props => {
  const dispatch = useDispatch();
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const pdfData = useSelector(state => state.expertoToplanti.pdfData);

  useEffect(() => {
    dispatch(Actions.getToplantiPdf(toplanti));
  }, [dispatch]);

  if (!pdfData) {
    return <></>;
  }

  return (
    <Fragment>
      <PDFDownloadLink
        document={
          <PDFViewer>
            <Document>
              <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                  <Text>Section #1</Text>
                </View>
                <View style={styles.section}>
                  <Text>Section #2</Text>
                </View>
              </Page>
            </Document>
          </PDFViewer>
        }
        fileName="somename.pdf"></PDFDownloadLink>
    </Fragment>
  );
};

export default ToplantiPdf;
