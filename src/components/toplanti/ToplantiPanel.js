import React, { Fragment, useEffect, useState } from 'react';
import {
  Grid,
  FormControlLabel,
  Button,
  ButtonBase,
  CardHeader,
  CardContent,
  Card,
  Divider,
  TextField,
  Menu,
  List,
  ListItem,
  Checkbox,
  Paper,
  Avatar,
  IconButton,
  Tooltip,
  MenuItem
} from '@material-ui/core';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker
} from '@material-ui/pickers';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import AddIcon from '@material-ui/icons/Add';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import IOSSwitch from '../IosSwitch';
import ToplantiExpertoKatilim from 'components/toplanti/ToplantiExpertoKatilim';
import ToplantiFirmaKatilim from 'components/toplanti/ToplantiFirmaKatilim';
import {
  hours,
  sureler,
  toplantiKategorileri
} from 'components/utils/constants';
import { List as ListIcon } from 'react-feather';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw, convertToRaw } from 'draft-js';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import AttachPanel from 'components/attachment/AttachPanel'

const moment = require('moment');

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1)
    }
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

const ToplantiBody = props => {
  const dispatch = useDispatch();
  const musteriler = useSelector(state => state.experto.musteriler);
  const kisiler = useSelector(state => state.kisi.kisiler);
  const kisi = useSelector(state => state.kisi.kisi);
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const error = useSelector(state => state.app.error);
  useEffect(() => {
    const load = async () => {
      if (!musteriler) {
        dispatch(Actions.getMusteriler());
      }
    };
    load();
  }, [dispatch]);

  //useEffect(() => {
  //  console.log(kisiler)
  //}, [kisiler]);

  useEffect(() => {
    // Toplanti firma katilimcisi yeni eklenmisse SaveKisi cagirilir. Donuste store.kisi.kisi doldurur.
    // Oradaki bir degisiklikte musterinin kisileri tekrar cagirilir ve Autocomplete doldurulur
    if (toplanti && toplanti.firma) {
      dispatch(Actions.getMusteriKisiler(toplanti.firma._id));
    }
  }, [kisi]);

  if (!musteriler) {
    return <></>;
  }

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Toplantı"
          avatar={<InsertInvitationIcon fontSize="large" />}
        />
        <Divider />
        <CardContent>
          <Grid container spacing={1}>
            <Grid item xs={2}>
              <FormControlLabel
                control={
                  <IOSSwitch
                    checked={toplanti.icToplantiMi}
                    //disabled={!editMode}
                    onChange={e => {
                      let checked = e.target.checked;
                      var t = { ...toplanti };
                      t.icToplantiMi = checked;
                      dispatch({ type: Actions.SET_TOPLANTI, toplanti: t });
                    }}
                    name="checkedB"
                  />
                }
                label="İç Toplantı mı?"
              />
            </Grid>
            <Grid item xs={10}>
              <Autocomplete
                disabled={toplanti.icToplantiMi}
                options={musteriler}
                getOptionLabel={option => option.marka}
                value={toplanti.firma}
                onChange={(e, v, r) => {
                  if (v) {
                    dispatch(Actions.getMusteriKisiler(v._id));
                    dispatch({
                      type: Actions.SET_TOPLANTI,
                      toplanti: { firma: v }
                    });
                  }
                }}
                //InputProps={{
                //  readOnly: gorev.createdByMsId != me.id
                //}}
                //onChange={(e, v) => {
                //  dispatch({
                //    type: Actıons.SET_GOREV,
                //    gorev: { musteri: v }
                //  });
                //}}
                renderInput={params => (
                  <TextField
                    {...params}
                    label="İlgili Müşteri"
                    variant="outlined"
                    fullWidth
                    error={error && error.musteri}
                    helperText={error && error.musteri}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={error && error.baslik}
                helperText={error && error.baslik}
                label="Başlık"
                autoFocus
                fullWidth
                variant="outlined"
                value={toplanti.baslik}
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { baslik: e.target.value }
                  });
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Açıklama"
                error={error && error.aciklama}
                helperText={error && error.aciklama}
                fullWidth
                multiline
                rows={3}
                variant="outlined"
                value={toplanti.aciklama}
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { aciklama: e.target.value }
                  });
                }}
              />
            </Grid>
            <Grid item xs={4}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  error={error && error.tarih}
                  helperText={error && error.tarih}
                  autoOk
                  fullWidth
                  variant="inline"
                  inputVariant="outlined"
                  label="Toplantı Tarihi"
                  format="dd/MM/yyyy"
                  value={toplanti.tarih}
                  InputAdornmentProps={{ position: 'start' }}
                  onChange={date =>
                    dispatch(Actions.setToplanti({ tarih: date }))
                  }
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={8}>
              <TextField
                error={error && error.yer}
                helperText={error && error.yer}
                label="Konum"
                fullWidth
                variant="outlined"
                value={toplanti.yer}
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { yer: e.target.value }
                  });
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                error={error && error.saat}
                helperText={error && error.saat}
                label="Saat"
                fullWidth
                select
                variant="outlined"
                value={toplanti.saat}
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { saat: e.target.value }
                  });
                }}>
                {hours.map(h => (
                  <MenuItem key={h} value={h}>
                    {h}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={6}>
              <TextField
                error={error && error.sure}
                helperText={error && error.sure}
                label="Süre"
                fullWidth
                select
                variant="outlined"
                value={toplanti.sure}
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { sure: e.target.value }
                  });
                }}>
                {sureler.map(s => (
                  <MenuItem key={s} value={s}>
                    {s}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={6}>
              <TextField
                error={error && error.tur}
                helperText={error && error.tur}
                label="Toplantı Türü"
                fullWidth
                variant="outlined"
                value={toplanti.tur}
                select
                onChange={e => {
                  dispatch({
                    type: Actions.SET_TOPLANTI,
                    toplanti: { tur: e.target.value }
                  });
                }}>
                <MenuItem key="yuzyuze" value="yuzyuze">
                  Yüzyüze
                </MenuItem>
                <MenuItem key="telekonferans" value="telekonferans">
                  Telekonferans
                </MenuItem>
                <MenuItem key="videokonferans" value="videokonferans">
                  Video Konferans
                </MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={6}>
              <TextField
                error={error && error.kategori}
                helperText={error && error.kategori}
                label="Toplantı Kategorisi"
                fullWidth
                variant="outlined"
                value={toplanti.kategori}
                select
                onChange={e => {
                  var t = { ...toplanti };
                  t.kategori = e.target.value;
                  dispatch(Actions.setToplanti(t));
                }}>
                {toplantiKategorileri.map(tk => {
                  return (
                    <MenuItem key={tk} value={tk}>
                      {tk}
                    </MenuItem>
                  );
                })}
              </TextField>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card className="mt-2">
        <CardHeader title="Toplantı Gündemi" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Toplantı Gündemi"
                variant="outlined"
                multiline
                rows={5}
                value={toplanti.gundem}
                onChange={e => {
                  var t = { ...toplanti };
                  t.gundem = e.target.value;
                  dispatch(Actions.setToplanti(t));
                }}
              />
              {/*
              <Editor 
              editorState={convertFromRaw(toplanti.gundem)}
              onEditorStateChange={(e) => {
                var t = {...toplanti}
                t.gundem = convertToRaw(e)
                dispatch(Actions.setToplanti(t))
              }} />
               */}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

const ToplantiAction = props => {
  const dispatch = useDispatch();
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);

  const toplantiKararAcik = () => {
    const today = new Date().setHours(0, 0, 0, 0);
    const topTar2 = new Date(toplanti.tarih).setHours(0, 0, 0, 0);
    return !toplanti.yeni && today >= topTar2; // moment(toplanti.tarih, 'DD.mm.yyyy') > new moment()
  };

  return (
    <Fragment>
      <Paper className="rounded p-4">
        <Button
          variant="contained"
          color="primary"
          className="m-2 p-2"
          disabled={!toplantiKararAcik()}
          onClick={() => {
            props.history.push(`/toplanti/${toplanti._id}/karar`);
          }}>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['fa', 'list']} />
          </span>
          <span className="btn-wrapper--label">Toplantı Kararları</span>
        </Button>
        <Button
          variant="contained"
          color="primary"
          className="m-2 p-2"
          disabled={!toplantiKararAcik()}
          onClick={() => {
            dispatch(Actions.getToplantiPdf(toplanti));
          }}>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['far', 'file-pdf']} />
          </span>
          <span className="btn-wrapper--label">Toplantı Raporu</span>
        </Button>
        <Button
          variant="contained"
          color="primary"
          className="m-2 p-2"
          disabled={toplanti.tamamlandi || !toplantiKararAcik()}
          onClick={() => {
            confirmAlert({
              title: 'Toplantı Tamamlama',
              message:
                'Toplantıyı tamamlamak üzeresiniz. Toplantı tamamlandıktan sonra yeni kararlar girilemez. Toplantıyı Tamamlamak için emin misin?',
              buttons: [
                {
                  label: 'Evet',
                  onClick: () => {
                    dispatch(Actions.toplantiTamamla(toplanti));
                  }
                },
                { label: 'Hayır', onClick: () => {} }
              ]
            });
            //props.history.push(`/dashboardsatis`)
          }}>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['far', 'file-pdf']} />
          </span>
          <span className="btn-wrapper--label">Toplantı Tamamlandı</span>
        </Button>
      </Paper>
    </Fragment>
  );
};

const _ToplantiExpertoKatilim = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const personeller = useSelector(state => state.graph.personeller);
  const [anchorElMenu, setAnchorElMenu] = useState(null);

  const [atamaListesi, setAtamaListesi] = useState(
    personeller.map(p => {
      const secili = toplanti.expertoKatilimcilar.includes(p.id);
      return { secili: secili, personel: p };
    })
  );

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };

  const handleAtamaSecimi = (e, p) => {
    var _ = [...atamaListesi];
    var x = _.filter(a => a.personel.id == p.personel.id);
    x[0].secili = e.target.checked;
    setAtamaListesi(_);

    var atananlar = [...toplanti.expertoKatilimcilar];
    if (e.target.checked) {
      atananlar.push(p.personel.id);
    } else {
      atananlar = atananlar.filter(a => a != p.personel.id);
    }

    dispatch({
      type: Actions.SET_TOPLANTI,
      toplanti: { expertoKatilimcilar: atananlar }
    });
  };

  const personelMenu = (
    <Menu
      anchorEl={anchorElMenu}
      keepMounted
      open={Boolean(anchorElMenu)}
      onClose={() => setAnchorElMenu(null)}
      getContentAnchorEl={null}
      classes={{ list: 'p-0' }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <div className="overflow-hidden dropdown-menu-xl p-0">
        <div className="bg-composed-wrapper bg-light mt-0">
          <div className="bg-composed-wrapper--content text-black px-4 py-3">
            <h5 className="mb-1">Atanacak Personeller</h5>
            <p className="mb-0 opacity-7"></p>
          </div>
        </div>
        <List>
          {atamaListesi.map(p => (
            <Fragment>
              <ListItem
                //button
                className="align-box-row"
                //onClick={() => handleTemsilci(p.id)}
              >
                <div className="d-flex align-items-center">
                  <Checkbox
                    checked={p.secili}
                    onChange={e => handleAtamaSecimi(e, p)}
                  />
                  <Avatar className="mr-4" src={p.personel.photo} />
                  {p.personel.displayName}
                  <span className="text-black">{p.personel.jobTitle}</span>
                </div>
              </ListItem>
              <Divider />
            </Fragment>
          ))}
        </List>
      </div>
    </Menu>
  );

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Experto Katılımcılar"
          action={
            <Tooltip title="Katılımcı ekle">
              <IconButton onClick={handleClickMenu}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          }
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <div className="d-flex align-items-center justify-content-between">
                <AvatarGroup>
                  {atamaListesi
                    .filter(a => a.secili)
                    .map(p => (
                      <Tooltip title={p.personel.displayName}>
                        <Avatar
                          className={classes.avatar}
                          alt={p.personel.displayName}
                          src={p.personel.photo}
                        />
                      </Tooltip>
                    ))}
                </AvatarGroup>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      {personelMenu}
    </Fragment>
  );
};

const _ToplantiFirmaKatilim = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const me = useSelector(state => state.graph.me);
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const personeller = useSelector(state => state.graph.personeller);
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const [atamaListesi, setAtamaListesi] = useState(
    []
    //personeller.map(p => {
    //  const secili = toplanti.expertoKatilimcilar.includes(p.id);
    //  return { secili: secili, personel: p };
    //})
  );

  const handleClickMenu = event => {
    setAnchorElMenu(event.currentTarget);
  };

  const handleAtamaSecimi = (e, p) => {
    var _ = [...atamaListesi];
    var x = _.filter(a => a.personel.id == p.personel.id);
    x[0].secili = e.target.checked;
    setAtamaListesi(_);

    var atananlar = [...toplanti.expertoKatilimcilar];
    if (e.target.checked) {
      atananlar.push(p.personel.id);
    } else {
      atananlar = atananlar.filter(a => a != p.personel.id);
    }

    //dispatch({
    //  type: SET_GOREV,
    //  gorev: { atananlar: atananlar }
    //});
  };

  const personelMenu = (
    <Menu
      anchorEl={anchorElMenu}
      keepMounted
      open={Boolean(anchorElMenu)}
      onClose={() => setAnchorElMenu(null)}
      getContentAnchorEl={null}
      classes={{ list: 'p-0' }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center'
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}>
      <div className="overflow-hidden dropdown-menu-xl p-0">
        <div className="bg-composed-wrapper bg-light mt-0">
          <div className="bg-composed-wrapper--content text-black px-4 py-3">
            <h5 className="mb-1">Atanacak Personeller</h5>
            <p className="mb-0 opacity-7"></p>
          </div>
        </div>
        <List>
          {atamaListesi.map(p => (
            <Fragment>
              <ListItem
                //button
                className="align-box-row"
                //onClick={() => handleTemsilci(p.id)}
              >
                <div className="d-flex align-items-center">
                  <Checkbox
                    checked={p.secili}
                    onChange={e => handleAtamaSecimi(e, p)}
                  />
                  <Avatar className="mr-4" src={p.personel.photo} />
                  {p.personel.displayName}
                  <span className="text-black">{p.personel.jobTitle}</span>
                </div>
              </ListItem>
              <Divider />
            </Fragment>
          ))}
        </List>
      </div>
    </Menu>
  );

  return (
    <Fragment>
      <Card>
        <CardHeader
          title="Firma Katılımcılar"
          action={
            <Tooltip title="Katılımcı ekle">
              <IconButton disabled onClick={handleClickMenu}>
                <AddIcon />
              </IconButton>
            </Tooltip>
          }
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <div className="d-flex align-items-center justify-content-between">
                <AvatarGroup>
                  {atamaListesi
                    .filter(a => a.secili)
                    .map(p => (
                      <Tooltip title={p.personel.displayName}>
                        <Avatar
                          className={classes.avatar}
                          alt={p.personel.displayName}
                          src={p.personel.photo}
                        />
                      </Tooltip>
                    ))}
                </AvatarGroup>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      {personelMenu}
    </Fragment>
  );
};

const ToplantiYorumlar = props => {
  return <Fragment></Fragment>;
};

const ToplantiEkler = props => {
  return <Fragment></Fragment>;
};

const ToplantiKararlar = props => {
  return <Fragment></Fragment>;
};

const ToplantiPanel = props => {
  const dispatch = useDispatch();
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);

  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12} container spacing={2}>
          <Grid item xs={8}>
            <ToplantiBody />
          </Grid>
          <Grid item xs={4} container spacing={2}>
            <Grid item xs={12}>
              <ToplantiAction {...props} />
            </Grid>
            <Grid item xs={12}>
              <ToplantiExpertoKatilim />
            </Grid>
            <Grid item xs={12}>
              <ToplantiFirmaKatilim />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <ToplantiKararlar />
        </Grid>
        <Grid item xs={12}>
          <ToplantiYorumlar />
        </Grid>
        <Grid item xs={8}>
            <AttachPanel
              files={toplanti.attachments || []}
              onFileUploaded={fileId => {
                var t = { ...toplanti };
                t.attachments.push(fileId);
                dispatch(Actions.setToplanti(t));
              }}
            />
          </Grid>
      </Grid>
    </Fragment>
  );
};

export default withRouter(ToplantiPanel);
