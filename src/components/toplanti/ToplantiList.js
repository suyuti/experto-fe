import React, { Fragment } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  Avatar,
  IconButton,
  Button,
  LinearProgress
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import moment from 'moment';
import { withRouter } from 'react-router';
import EditIcon from '@material-ui/icons/Edit';
import MaterialTable from 'material-table';
import * as Actions from 'store/actions';

const ToplantiList = props => {
  const dispatch = useDispatch();
  const toplantilar = useSelector(state => state.expertoToplanti.toplantilar);
  const personeller = useSelector(state => state.graph.personeller);

  return (
    <Fragment>
      <MaterialTable
        title="Toplantılar"
        data={toplantilar || []}
        columns={[
          { title: 'Konu', field: 'baslik' },
          { title: 'Firma', field: 'firma.marka' },
          {
            title: 'Zaman',
            render: rowData => moment(rowData.tarih).format('DD.MM.yyyy')
          },
          { title: 'Yer', field: 'yer' },
          { title: 'Kategori', field: 'kategori' },
          {
            title: 'Kararlar',
            render: rowData => {
              const today = new Date();
              const topTar = new Date(rowData.tarih);

              return (
                <Button
                  disabled={today < topTar}
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    dispatch(Actions.getToplanti(rowData._id));
                    props.history.push(`/toplanti/${rowData._id}/karar`);
                  }}>
                  Kararlar
                </Button>
              );
            }
          },
          {
            title: 'Katılımcılar',
            render: rowData => {
              return (
                <AvatarGroup>
                  {rowData.expertoKatilimcilar.map(ek => {
                    let p = personeller.find(p => p.id == ek);
                    if (!p) {
                      return <></>;
                    }
                    return (
                      <Tooltip title={p.displayName}>
                        <Avatar src={p.photo} />
                      </Tooltip>
                    );
                  })}
                </AvatarGroup>
              );
            }
          },
          {
            title: 'Durum',
            render: row => {
              if (row.tamamlandi) {
                return (
                  <div className="h-auto py-0 px-3 badge badge-success">
                    Tamamlandı
                  </div>
                );
              }
              else {
                return (
                  <div className="h-auto py-0 px-3 badge badge-info">
                    Tamamlanmadı
                  </div>
                );
              }
            }
          }
        ]}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Detail',
            onClick: (event, rowData) =>
              props.history.push(`/toplanti/${rowData._id}`),
            disabled: false // TODO Yetkisi yoksa goremeyecek
          }
        ]}
        options={{
          actionsColumnIndex: -1,
          pageSize: 20
        }}
      />
    </Fragment>
  );
};

export default withRouter(ToplantiList);
