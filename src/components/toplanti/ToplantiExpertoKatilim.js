import React, { Fragment, useState, useEffect } from 'react';
import AutoComplete, {
  createFilterOptions
} from '@material-ui/lab/Autocomplete';
import {
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Grid,
  Avatar,
  Typography,
  Chip
} from '@material-ui/core';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import { useSelector, useDispatch } from 'react-redux';
import { SET_TOPLANTI } from 'store/actions';

const ToplantiExpertoKatilim = props => {
  const dispatch = useDispatch();
  const personeller = useSelector(state => state.graph.personeller);
  const error = useSelector(state => state.app.error);
  const toplanti = useSelector(state => state.expertoToplanti.toplanti);
  const [atananlar, setAtananlar] = useState([]);
  const [atananlarDisplayName, setAtananlarDisplayName] = useState([]);

  useEffect(() => {
    var _atananlar = [];
    var _atananlarDisplayName = [];
    for (let k of toplanti.expertoKatilimcilar) {
      let p = personeller.find(p => p.id === k);
      if (p) {
        _atananlar.push(p);
        _atananlarDisplayName.push(p.displayName)
      }
    }
    setAtananlar(_atananlar);
    setAtananlarDisplayName(_atananlarDisplayName);
  }, [toplanti.expertoKatilimcilar]);

  return (
    <Fragment>
      <Card className="card-box">
        <CardHeader title="Experto Katılımcıları" avatar={<PersonPinIcon />} />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <AutoComplete
                multiple
                options={personeller}
                getOptionLabel={opt => opt.displayName}
                filterSelectedOptions
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                value={atananlar}
                onChange={(event, newValue) => {
                  const atananlar = newValue.map(a => a.id);
                  const atananlarDisplayName = newValue.map(a => a.displayName);
                  dispatch({
                    type: SET_TOPLANTI,
                    toplanti: { expertoKatilimcilar: atananlar, expertoKatilimcilarDisplayName: atananlarDisplayName }
                  });
                  //}
                }}
                /*
                filterOptions={(options, params) => {
                const filtered = filter(options, params);
                if (params.inputValue !== '') {
                    filtered.push({
                    inputValue: params.inputValue,
                    title: `Ekle "${params.inputValue}"`
                    });
                }
                return filtered;
                }}
                */
                freeSolo
                renderTags={(tagValue, getTagProps) =>
                  tagValue.map((opt, index) => (
                    <Chip
                      avatar={<Avatar src={opt.photo} />}
                      label={opt.displayName}
                      variant="outlined"
                      {...getTagProps({ index })}
                    />
                  ))
                }
                renderInput={params => (
                  <TextField
                    error={error && error.expertoKatilimcilar}
                    helperText={error && error.expertoKatilimcilar}
                    fullWidth
                    {...params}
                    variant="outlined"
                    label="Personel"
                    placeholder="Personel"
                  />
                )}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Fragment>
  );
};

export default ToplantiExpertoKatilim;
