import React, { lazy, Suspense, Fragment } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from 'framer-motion';
import { useSelector } from 'react-redux';
import { ThemeProvider } from '@material-ui/styles';

import { ClimbingBoxLoader } from 'react-spinners';

import MuiTheme from './theme';

// Layout Blueprints

import {
  LeftSidebar,
  MinimalLayout,
  PresentationLayout
} from './layout-blueprints';

// Example Pages

import PagesLogin from './example-pages/PagesLogin';
import PagesRegister from './example-pages/PagesRegister';
import PagesRecoverPassword from './example-pages/PagesRecoverPassword';
import PagesError404 from './example-pages/PagesError404';
import Buttons from './example-pages/Buttons';
import RegularTables1 from './example-pages/RegularTables1';
import DashboardSatis from 'pages/dashboard/DashboardSatis';
import NewUserPage from 'pages/home/newuser';

const DashboardDefault = lazy(() => import('./example-pages/DashboardDefault'));
const LandingPage = lazy(() => import('./pages/landing'));
const GorevPage = lazy(() => import('./pages/gorev/Gorev'));
const GorevlerPage = lazy(() => import('./pages/gorev/Gorevler'));
const ToplantiPage = lazy(() => import('./pages/toplanti/Toplanti'));
const ToplantilarPage = lazy(() => import('./pages/toplanti/Toplantilar'));
const ToplantiKararPage = lazy(() => import('./pages/toplanti/ToplantiKarar'));
const ToplantiPdfPage = lazy(() => import('./components/toplanti/ToplantiPdf'));
const MusterilerPage = lazy(() => import('./pages/musteri/Musteriler'));
const MusteriPage = lazy(() => import('./pages/musteri/Musteri'));
const UrunlerPage = lazy(() => import('./pages/urun/Urunler'));
const UrunPage = lazy(() => import('./pages/urun/Urun'));
const KisilerPage = lazy(() => import('./pages/kisi/Kisiler'));
const KisiPage = lazy(() => import('./pages/kisi/Kisi'));
const ProfilePage = lazy(() => import('./pages/profile/Profile'));
const PersonelPage = lazy(() => import('./pages/personel/Personel'));
const PersonellerPage = lazy(() => import('./pages/personel/Personeller'));
const PartnerPage = lazy(() => import('./pages/partner/Partner'));
const PartnerlerPage = lazy(() => import('./pages/partner/Partnerler'));
const TedarikciPage = lazy(() => import('./pages/tedarikci/Tedarikci'));
const TedarikcilerPage = lazy(() => import('./pages/tedarikci/Tedarikciler'));
const DepartmanlarPage = lazy(() =>
  import('./pages/admin/departman/Departmanlar')
);
const DepartmanPage = lazy(() => import('./pages/admin/departman/Departman'));
const YetkiPage = lazy(() => import('./pages/admin/Yetkilendirme/Yetki'));
const GorusmelerPage = lazy(() => import('./pages/gorusme/Gorusmeler'));
const GorusmePage = lazy(() => import('./pages/gorusme/Gorusme'));

const PrivateRoute = ({ component: Component, layout: Layout, ...rest }) => {
  const me = useSelector(state => state.graph.me);
  //debugger
  return (
    <Route
      {...rest}
      render={props =>
        me ? (
          <Layout>
            <Component {...props} {...rest} />
          </Layout>
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

const Routes = () => {
  const location = useLocation();

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99
    },
    in: {
      opacity: 1,
      scale: 1
    },
    out: {
      opacity: 0,
      scale: 1.01
    }
  };

  const pageTransition = {
    type: 'tween',
    ease: 'anticipate',
    duration: 0.4
  };

  const SuspenseLoading = () => {
    return (
      <Fragment>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <ClimbingBoxLoader color={'#5383ff'} loading={true} />
          </div>
          <div className="text-muted font-size-xl text-center pt-3">
            Please wait while we load the live preview examples
            <span className="font-size-lg d-block text-dark">
              This live preview instance can be slower than a real production
              build!
            </span>
          </div>
        </div>
      </Fragment>
    );
  };
  return (
    <ThemeProvider theme={MuiTheme}>
      <AnimatePresence>
        <Suspense fallback={<SuspenseLoading />}>
          <Switch>
            <Redirect exact from="/" to="/welcome" />
            <Route path={['/welcome']}>
              <PresentationLayout>
                <Switch location={location} key={location.pathname}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <Route path="/welcome" component={LandingPage} />
                  </motion.div>
                </Switch>
              </PresentationLayout>
            </Route>

            <Route
              path={[
                '/PagesLogin',
                '/PagesRegister',
                '/PagesRecoverPassword',
                '/PagesError404'
              ]}>
              <MinimalLayout>
                <Switch location={location} key={location.pathname}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <Route path="/PagesLogin" component={PagesLogin} />
                    <Route path="/PagesRegister" component={PagesRegister} />
                    <Route
                      path="/PagesRecoverPassword"
                      component={PagesRecoverPassword}
                    />
                    <Route path="/PagesError404" component={PagesError404} />
                  </motion.div>
                </Switch>
              </MinimalLayout>
            </Route>

            <PrivateRoute
              path="/dashboardsatis"
              component={DashboardSatis}
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/dashboardsatis"
              component={DashboardSatis}
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorevler"
              component={GorevlerPage}
              gorevTipi="hepsi"
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorevler?t=basarili"
              component={GorevlerPage}
              gorevTipi="basarili"
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorevler?t=basarisiz"
              component={GorevlerPage}
              gorevTipi="basarisiz"
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorevler?t=acik"
              component={GorevlerPage}
              gorevTipi="acik"
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorevler?t=olusturdugum"
              component={GorevlerPage}
              gorevTipi="olusturdugum"
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/gorev/:id"
              component={GorevPage}
              layout={LeftSidebar}
            />

            <PrivateRoute
              path="/basariligorevler"
              component={GorevlerPage}
              gorevTipi="basarili"
              layout={LeftSidebar}
            />

            <PrivateRoute
              path="/toplantilar"
              component={ToplantilarPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/toplanti/:id/karar"
              component={ToplantiKararPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/toplanti/:id/pdf"
              component={ToplantiPdfPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              path="/toplanti/:id"
              component={ToplantiPage}
              layout={LeftSidebar}
            />

            <PrivateRoute
              exact
              path="/musteriler/:id"
              component={MusteriPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/musteriler"
              component={MusterilerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/urunler"
              component={UrunlerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/urun/:id"
              component={UrunPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/kisiler"
              component={KisilerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/kisi/:id"
              component={KisiPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/profile"
              component={ProfilePage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/personeller"
              component={PersonellerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/personel/:id"
              component={PersonelPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/partnerler"
              component={PartnerlerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/partner/:id"
              component={PartnerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/tedarikciler"
              component={TedarikcilerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/tedarikci/:id"
              component={TedarikciPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/departmanlar"
              component={DepartmanlarPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/departmanlar/:id"
              component={DepartmanPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/yetki"
              component={YetkiPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/gorusmeler"
              component={GorusmelerPage}
              layout={LeftSidebar}
            />
            <PrivateRoute
              exact
              path="/gorusme/:id"
              component={GorusmePage}
              layout={LeftSidebar}
            />

            <Route
              exact
              path="/newuser"
              component={NewUserPage}
              layout={MinimalLayout}
            />

            {/*
            <PrivateRoute
            //path={[
            //'/dashboardsatis',
            //'/gorevler',
            //'/acikgorevler',
            //'/basariligorevler',
            //'/basarisizgorevler',
            //'/olusturdugumgorevler',
            //'/gorev/:id'
            //]}
            >
              <LeftSidebar>
                <Switch location={location} key={location.pathname}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <PrivateRoute
                      path="/dashboardsatis"
                      component={DashboardSatis}
                    />
                    <PrivateRoute
                      path="/basariligorevler"
                      component={GorevlerPage}
                      gorevTipi="basarili"
                    />
                    <PrivateRoute
                      path="/basarisizgorevler"
                      component={GorevlerPage}
                      gorevTipi="basarisiz"
                    />
                    <PrivateRoute
                      path="/acikgorevler"
                      component={GorevlerPage}
                      gorevTipi="acik"
                    />
                    <PrivateRoute
                      path="/olusturdugumgorevler"
                      component={GorevlerPage}
                      gorevTipi="olusturdugum"
                    />
                    <PrivateRoute path="/gorev/:id" component={GorevPage} />
                  </motion.div>
                </Switch>
              </LeftSidebar>
            </PrivateRoute>
*/}
          </Switch>
        </Suspense>
      </AnimatePresence>
    </ThemeProvider>
  );
};

export default Routes;
