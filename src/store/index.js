
/*import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import createReducer from './reducers';
import thunk from 'redux-thunk';
import app from './reducers/app.reducer';
import ThemeOptions from '../reducers/ThemeOptions';

const rootReducer = combineReducers({
  app,
  ThemeOptions
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
*/

import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import ThemeOptions from '../reducers/ThemeOptions';
import app from './reducers/app.reducer';
import graph from './reducers/graph.reducer';
import experto from './reducers/experto.reducer';
import expertoToplanti from './reducers/experto.toplanti.reducer';
import urun from './reducers/experto.urun.reducer';
import kisi from './reducers/experto.kisi.reducer';
import departman from './reducers/experto.departman.reducer';
import yetki from './reducers/experto.yetki.reducer';
import gorusme from './reducers/experto.gorusme.reducer';

export default function configureStore() {
    return createStore(
    combineReducers({
      ThemeOptions,
      app,
      graph,
      experto,
      expertoToplanti,
      urun,
      kisi,
      departman,
      yetki,
      gorusme
    }),
    applyMiddleware(thunk)
  );
}
