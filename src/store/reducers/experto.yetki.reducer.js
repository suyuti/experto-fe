import * as Actions from '../actions';

const initialState = {
  yetkiler: null,
};

const expertoYetkiReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_YETKILER_REQ: {
      return { ...state, yetkiler: null };
    }
    case Actions.GET_YETKILER_SUC: {
      debugger
      return { ...state, yetkiler: [...action.yetkiler] };
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoYetkiReducer;
