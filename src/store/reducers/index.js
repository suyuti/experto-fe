import {combineReducers} from 'redux'
import app from './app.reducer'
import graph from './graph.reducer'
import experto from './experto.reducer'

const createReducer = (asyncReducers) => {
    combineReducers({
        ...asyncReducers,
        app,
        graph, 
        experto
    })
}

export default createReducer;