import * as Actions from '../actions';

const initialState = {
  expertoToken: null,
  redirectUrl: null,
  yetkiler: null,
  gorevler: null,
  musteriler: null,
  musteri: null,
  gorev: null,
  gorevStats: null,
  sektorler: null,
  kisiler: null,
  yorumlar: null,
  musteriUrunler: null,
  uyeOldugumDepartmanlar: null,
  calisabildigimPersoneller: null,
  expertoPersoneller: null,
  history: null,
  yoneticisiOldugumDepartmanlar: null,
  yoneticisiOldugumPersoneller: null
};

const expertoReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.EXPERTO_LOGIN_ERR:
    case Actions.EXPERTO_LOGIN_REQ: {
      return {
        ...state,
        expertoToken: null,
        redirectUrl: null,
        yetkiler: null,
        uyeOldugumDepartmanlar    : null,
        calisabildigimPersoneller : null,
        yoneticisiOldugumDepartmanlar: null,
        yoneticisiOldugumPersoneller: null
      };
    }
    case Actions.EXPERTO_LOGIN_SUC: {
      return {
        ...state,
        expertoToken              : action.payload.expertoToken,
        redirectUrl               : action.payload.redirectUrl,
        yetkiler                  : { ...action.payload.yetkiler },
        uyeOldugumDepartmanlar    : null,
        calisabildigimPersoneller : [...action.payload.personeller],
        yoneticisiOldugumDepartmanlar: [...action.payload.yoneticisiOldugumDepartmanlar],
        yoneticisiOldugumPersoneller  : [...action.payload.yoneticisiOldugumPersoneller]
      };
    }

    case Actions.GET_GOREVLER_ERR:
    case Actions.GET_GOREVLER_REQ: {
      return { ...state, gorevler: null };
    }
    case Actions.GET_GOREVLER_SUC: {
      return { ...state, gorevler: [...action.gorevler] };
    }

    case Actions.GET_GOREV_ERR:
    case Actions.GET_GOREV_REQ: {
      return { ...state, gorev: null };
    }
    case Actions.GET_GOREV_SUC: {
      return { ...state, gorev: { ...action.gorev } };
    }

    case Actions.GET_MUSTERILER_ERR:
    case Actions.GET_MUSTERILER_REQ: {
      return { ...state, musteriler: null };
    }
    case Actions.GET_MUSTERILER_SUC: {
      return { ...state, musteriler: [...action.musteriler] };
    }
    case Actions.SET_GOREV: {
      return { ...state, gorev: { ...state.gorev, ...action.gorev } };
    }
    case Actions.YENI_GOREV: {
      return { ...state, gorev: { ...action.gorev } };
    }
    case Actions.SAVE_GOREV_REQ: {
      return { ...state, gorev: { ...state.gorev, kayit: false } };
    }
    case Actions.SAVE_GOREV_SUC: {
      return { ...state, gorev: { ...state.gorev, kayit: true } };
    }
    case Actions.GET_GOREV_COUNT_SUC: {
      return { ...state, gorevStats: { ...action.gorevStats } };
    }
    case Actions.GET_MUSTERI_REQ: {
      return { ...state, musteri: null };
    }
    case Actions.GET_MUSTERI_SUC: {
      return { ...state, musteri: { ...action.musteri } };
    }
    case Actions.GET_SEKTORLER_REQ: {
      return { ...state, sektorler: null };
    }
    case Actions.GET_SEKTORLER_SUC: {
      return { ...state, sektorler: [ ...action.sektorler ] };
    }
    case Actions.GET_MUSTERI_KISILER_REQ: {
      return { ...state, kisiler: null };
    }
    case Actions.GET_MUSTERI_KISILER_SUC: {
      return { ...state, kisiler: [...action.kisiler] };
    }
    case Actions.GET_YORUMLAR_REQ: {
      return { ...state, yorumlar: null };
    }
    case Actions.GET_YORUMLAR_SUC: {
      return { ...state, yorumlar: [...action.yorumlar] };
    }

    case Actions.GET_MUSTERI_URUNLER_REQ: {
      return { ...state, musteriUrunler: null };
    }
    case Actions.GET_MUSTERI_URUNLER_SUC: {
      return { ...state, musteriUrunler: [...action.urunler] };
    }
    case Actions.YENI_MUSTERI: {
      return { ...state, musteri: {...action.musteri} };
    }
    case Actions.ADD_YORUM_SUC : {
      var y = [...state.yorumlar]
      y.push(action.yorum)
      return {...state, yorumlar: [...y] }
    }
    case Actions.SET_MUSTERI: {
      return { ...state, musteri: { ...state.musteri, ...action.musteri } };
    }
    case Actions.GET_EXPERTO_PERSONELLER_REQ: {
      return {...state, expertoPersoneller: null}
    }
    case Actions.GET_EXPERTO_PERSONELLER_SUC: {
      return {...state, expertoPersoneller: [...action.personeller]}
    }
    case Actions.GET_HISTORY_REQ: {
      return {...state, history: null}
    }
    case Actions.GET_HISTORY_SUC: {
      return {...state, history: [...action.history]}
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoReducer;
