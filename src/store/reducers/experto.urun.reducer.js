import * as Actions from '../actions';

const initialState = {
  urunler: null,
  urun: null
};

const expertoUrunReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_URUNLER_REQ: {
      return { ...state, urunler: null };
    }
    case Actions.GET_URUNLER_SUC: {
      return { ...state, urunler: [...action.urunler] };
    }
    case Actions.GET_URUN_REQ: {
      return { ...state, urunler: null };
    }
    case Actions.GET_URUN_SUC: {
      return { ...state, urun: { ...action.urun } };
    }
    case Actions.SET_URUN: {
      return { ...state, urun: { ...state.urun, ...action.urun } };
    }
    case Actions.YENI_URUN: {
      return { ...state, urun: { ...action.urun } };
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoUrunReducer;
