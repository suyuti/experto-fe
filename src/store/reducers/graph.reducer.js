import * as Actions from '../actions';

const initialState = {
  me: null,
  uniqueId: null,
  accessToken: null,
  myTasks: null, // TODO Expertoda takip ediliyor. Kullanilmayacak
  myMails: null,
  personeller: null,
  msLoginOk: false,
  loginwait: false,
  waitAuthCheck: true
};

const graphReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GRAPH_LOGIN_ERR:
    case Actions.GRAPH_LOGIN_REQ: {
      return {
        ...state,
        uniqueId: null,
        accessToken: null,
        me: null,
        loginwait: true,
        waitAuthCheck: true
      };
    }
    case Actions.GRAPH_LOGIN_SUC: {
      return {
        ...state,
        uniqueId: action.payload.uniqueId,
        me: { ...action.payload.me, photo: action.payload.mePhoto },
        loginwait: false,
        accessToken: action.payload.token,
        waitAuthCheck: false,
        msLoginOk: true
      };
    }
    case Actions.GET_USER_TASKS_ERR:
    case Actions.GET_USER_TASKS_REQ: {
      return { ...state, myTasks: null };
    }
    case Actions.GET_USER_TASKS_SUC: {
      return { ...state, myTasks: [...action.payload] };
    }
    case Actions.GET_USER_MAILS_ERR:
    case Actions.GET_USER_MAILS_REQ: {
      return { ...state, myMails: null };
    }
    case Actions.GET_USER_MAILS_SUC: {
      return { ...state, myMails: [...action.payload] };
    }
    case Actions.GRAPH_LOGOUT_SUC: {
      return {
        ...state,
        me: null,
        uniqueId: null,
        accessToken: null,
        myTasks: null,
        myMails: null,
        msLoginOk: false
      };
    }

    case Actions.CREATE_TASK_SUC: {
      return {
        ...state
      };
    }
    case Actions.UPDATE_TASK_SUC: {
      ;
      return {
        ...state
      };
    }

    case Actions.GET_PERSONELLER_ERR:
    case Actions.GET_PERSONELLER_REQ: {
      return { ...state, personeller: null };
    }
    case Actions.GET_PERSONELLER_SUC: {
      return { ...state, personeller: [...action.personeller] };
    }

    default: {
      return { ...state };
    }
  }
};

export default graphReducer;
