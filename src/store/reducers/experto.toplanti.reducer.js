import * as Actions from '../actions';

const initialState = {
  toplantilar: null,
  toplanti: null,
  yeni: false,
  kararlar: null,
  karar: null,
  pdfData: null,
  toplantiStats: null
};

const expertoToplantiReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.YENI_TOPLANTI: {
      return { ...state, toplanti: { ...action.toplanti }, yeni: true };
    }
    case Actions.GET_TOPLANTILAR_REQ: {
      return { ...state, toplantilar: null };
    }
    case Actions.GET_TOPLANTILAR_ERR: {
      return { ...state, toplantilar: null };
    }
    case Actions.GET_TOPLANTILAR_SUC: {
      return { ...state, toplantilar: [...action.toplantilar] };
    }
    case Actions.GET_TOPLANTI_REQ: {
      return { ...state, toplanti: null, yeni: false };
    }
    case Actions.GET_TOPLANTI_ERR: {
      return { ...state, toplanti: null, yeni: false };
    }
    case Actions.GET_TOPLANTI_SUC: {
      return { ...state, toplanti: { ...action.toplanti }, yeni: false };
    }
    case Actions.UPDATE_TOPLANTI_REQ: {
      return { ...state, toplanti: null };
    }
    case Actions.UPDATE_TOPLANTI_ERR: {
      return { ...state, toplanti: null };
    }
    case Actions.TOPLANTI_TAMAMLAR_SUC:
    case Actions.UPDATE_TOPLANTI_SUC: {
      return { ...state, toplanti: { ...action.toplanti } };
    }
    case Actions.SAVE_TOPLANTI_REQ: {
      return { ...state };
    }
    case Actions.SAVE_TOPLANTI_ERR: {
      return { ...state };
    }
    case Actions.SAVE_TOPLANTI_SUC: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_REQ: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_ERR: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_SUC: {
      return { ...state };
    }
    case Actions.ADD_TOPLANTI_KARAR_REQ: {
      return { ...state };
    }
    case Actions.ADD_TOPLANTI_KARAR_ERR: {
      return { ...state };
    }
    case Actions.ADD_TOPLANTI_KARAR_SUC: {
      return { ...state };
    }
    case Actions.UPDATE_TOPLANTI_KARAR_REQ: {
      return { ...state };
    }
    case Actions.UPDATE_TOPLANTI_KARAR_ERR: {
      return { ...state };
    }
    case Actions.UPDATE_TOPLANTI_KARAR_SUC: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_KARAR_REQ: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_KARAR_ERR: {
      return { ...state };
    }
    case Actions.REMOVE_TOPLANTI_KARAR_SUC: {
      return { ...state };
    }
    case Actions.CLEAR_TOPLANTI: {
      return { ...state, kararlar: null };
    }
    case Actions.GET_TOPLANTI_KARARLARI_REQ:
    case Actions.GET_TOPLANTI_KARARLARI_ERR: {
      return { ...state, kararlar: null };
    }
    case Actions.GET_TOPLANTI_KARARLARI_SUC: {
      return { ...state, kararlar: [...action.kararlar] };
    }
    case Actions.SET_TOPLANTI: {
      return { ...state, toplanti: { ...state.toplanti, ...action.toplanti } };
    }
    case Actions.GET_TOPLANTI_KARAR_REQ:
    case Actions.GET_TOPLANTI_KARAR_ERR: {
      return { ...state, karar: null };
    }
    case Actions.GET_TOPLANTI_KARAR_SUC: {
      return { ...state, karar: {...action.karar} };
    }
    case Actions.GET_TOPLANTI_PDF_REQ: {
      return { ...state, pdfData: null };
    }
    case Actions.GET_TOPLANTI_PDF_SUC: {
      return { ...state, pdfData: action.pdfData };
    }
    case Actions.GET_TOPLANTI_STATS_SUC: {
      return { ...state, toplantiStats: {...action.stats} };
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoToplantiReducer;
