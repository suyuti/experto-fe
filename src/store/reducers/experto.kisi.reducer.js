import * as Actions from '../actions';

const initialState = {
  kisiler: null,
  kisi: null,
  notlar: null
};

const expertoKisiReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_MUSTERI_KISILER_REQ:
    case Actions.GET_KISILER_REQ: {
      return { ...state, kisiler: null };
    }
    case Actions.GET_MUSTERI_KISILER_SUC:
    case Actions.GET_KISILER_SUC: {
      return { ...state, kisiler: [...action.kisiler] };
    }
    case Actions.GET_KISI_REQ: {
      return { ...state, kisiler: null };
    }
    case Actions.GET_KISI_SUC: {
      return { ...state, kisi: { ...action.kisi } };
    }
    //case Actions.SAVE_KISI_REQ: {
    //  return { ...state, kisi: null };
    //}
    case Actions.SAVE_KISI_SUC: {
      return { ...state, kisi: { ...action.kisi } };
    }
    case Actions.YENI_KISI: {
      return { ...state, kisi: { ...action.kisi } };
    }
    case Actions.GET_KISI_NOTLAR_REQ: {
      return { ...state, notlar: null };
    }
    case Actions.GET_KISI_NOTLAR_SUC: {
      return { ...state, notlar: [...action.notlar] };
    }
    case Actions.SET_KISI: {
      return { ...state, kisi: { ...state.kisi, ...action.kisi } };
    }


    default: {
      return { ...state };
    }
  }
};

export default expertoKisiReducer;
