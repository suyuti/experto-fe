import * as Actions from '../actions';

const initialState = {
  showGorevDlg: false,
  showMessage: false,
  message: false,
  error: null,
  enableKaydetBtn: true
};

const appReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.SHOW_GOREV_DIALOG: {
      return { ...state, showGorevDlg: true };
    }
    case Actions.HIDE_GOREV_DIALOG: {
      return { ...state, showGorevDlg: false };
    }
    case Actions.SHOW_MESSAGE: {
      return { ...state, message: action.message, showMessage: true };
    }
    case Actions.HIDE_MESSAGE: {
      return { ...state, message: null, showMessage: false };
    }
    case Actions.SET_ERROR: {
      return { ...state, error: {...action.error} };
    }
    case Actions.RESET_ERROR: {
      return { ...state, error: null };
    }
    case Actions.DISABLE_KAYDET_BTN: {
      return {...state, enableKaydetBtn: false }
    }
    case Actions.ENABLE_KAYDET_BTN: {
      return {...state, enableKaydetBtn: true }
    }
    default: {
      return { ...state };
    }
  }
};

export default appReducer