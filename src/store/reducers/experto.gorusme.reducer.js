import * as Actions from '../actions';

const initialState = {
  gorusmeler: null,
  gorusme: null
};

const expertoGorusmeReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_GORUSMELER_REQ: {
      return { ...state, gorusmeler: null };
    }
    case Actions.GET_GORUSMELER_SUC: {
      return { ...state, gorusmeler: [...action.gorusmeler] };
    }
    case Actions.GET_GORUSME_REQ: {
      return { ...state, gorusme: null };
    }
    case Actions.GET_GORUSME_SUC: {
      return { ...state, gorusme: { ...action.gorusme } };
    }
    case Actions.SET_GORUSME: {
      return { ...state, gorusme: { ...state.gorusme, ...action.gorusme } };
    }
    case Actions.YENI_GORUSME: {
      return { ...state, gorusme: { ...action.gorusme } };
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoGorusmeReducer;
