import * as Actions from '../actions';

const initialState = {
  departmanlar: null,
  departman: null
};

const expertoDepartmanReducer = function(state = initialState, action) {
  switch (action.type) {
    case Actions.GET_DEPARTMANLAR_REQ: {
      return { ...state, departmanlar: null };
    }
    case Actions.GET_DEPARTMANLAR_ERR: {
      return { ...state };
    }
    case Actions.GET_DEPARTMANLAR_SUC: {
      return { ...state, departmanlar: [...action.departmanlar] };
    }
    case Actions.GET_DEPARTMAN_REQ: {
      return { ...state, departman: null };
    }
    case Actions.GET_DEPARTMAN_ERR: {
      return { ...state };
    }
    case Actions.GET_DEPARTMAN_SUC: {
      return { ...state, departman: { ...action.departman } };
    }
    case Actions.SAVE_DEPARTMAN_REQ: {
      return { ...state };
    }
    case Actions.SAVE_DEPARTMAN_ERR: {
      return { ...state };
    }
    case Actions.SAVE_DEPARTMAN_SUC: {
      return { ...state, departman: { ...action.departman } };
    }
    case Actions.REMOVE_DEPARTMAN_REQ: {
      return { ...state };
    }
    case Actions.REMOVE_DEPARTMAN_ERR: {
      return { ...state };
    }
    case Actions.REMOVE_DEPARTMAN_SUC: {
      return { ...state };
    }
    case Actions.YENI_DEPARTMAN: {
      return { ...state, departman: { ...action.departman } };
    }
    case Actions.SET_DEPARTMAN: {
      return {
        ...state,
        departman: { ...state.departman, ...action.departman }
      };
    }

    default: {
      return { ...state };
    }
  }
};

export default expertoDepartmanReducer;
