import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';

export const GET_YETKILER_REQ = 'GET YETKILER REQ';
export const GET_YETKILER_ERR = 'GET YETKILER ERR';
export const GET_YETKILER_SUC = 'GET YETKILER SUC';

export const SAVE_YETKILER_REQ = 'SAVE YETKILER REQ';
export const SAVE_YETKILER_ERR = 'SAVE YETKILER ERR';
export const SAVE_YETKILER_SUC = 'SAVE YETKILER SUC';

export const getYetkiler = filter => {
  const client = axiosService.get(`/yetki`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_YETKILER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_YETKILER_ERR });
      }
      else {
        dispatch({ type: GET_YETKILER_SUC, yetkiler: resp.data });
      }
    });
  };
};

export const saveYetkiler = (departmanId, yetkiler) => {
  const client = axiosService.put(`/yetki/${departmanId}`, yetkiler);
  return dispatch => {
    dispatch({ type: SAVE_YETKILER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: SAVE_YETKILER_ERR });
      }
      else {
        dispatch({ type: SAVE_YETKILER_SUC, yetkiler: resp.data });
      }
    });
  };
/*  var client = null;
  if (kisi.yeni) {
    client = axiosService.post(`/kisi`, kisi);
  } else {
    client = axiosService.patch(`/kisi/${kisi._id}`, kisi);
  }

  return dispatch => {
    dispatch({ type: SAVE_KISI_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_KISI_SUC, kisi: resp });
      dispatch({ type: SHOW_MESSAGE, message: 'Kişi kaydedildi' });
    });
  };
  */
};
