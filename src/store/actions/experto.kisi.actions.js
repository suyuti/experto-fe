import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';

export const GET_KISILER_REQ = 'GET KISILER REQ';
export const GET_KISILER_ERR = 'GET KISILER ERR';
export const GET_KISILER_SUC = 'GET KISILER SUC';

export const GET_KISI_REQ = 'GET KISI REQ';
export const GET_KISI_ERR = 'GET KISI ERR';
export const GET_KISI_SUC = 'GET KISI SUC';

export const SAVE_KISI_REQ = 'SAVE KISI REQ';
export const SAVE_KISI_ERR = 'SAVE KISI ERR';
export const SAVE_KISI_SUC = 'SAVE KISI SUC';

export const GET_KISI_NOTLAR_REQ = 'GET KISI NOTLAR REQ';
export const GET_KISI_NOTLAR_ERR = 'GET KISI NOTLAR ERR';
export const GET_KISI_NOTLAR_SUC = 'GET KISI NOTLAR SUC';

export const ADD_KISI_NOT_REQ = 'ADD KISI NOT REQ';
export const ADD_KISI_NOT_ERR = 'ADD KISI NOT ERR';
export const ADD_KISI_NOT_SUC = 'ADD KISI NOT SUC';

export const YENI_KISI = 'YENI KISI';
export const SET_KISI = 'SET KISI';

export const getKisiler = filter => {
  const client = axiosService.get(`/kisi`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_KISILER_REQ });
    client.then(resp => {
      dispatch({ type: GET_KISILER_SUC, kisiler: resp });
    });
  };
};

export const getKisi = id => {
  const client = axiosService.get(`/kisi/${id}`);
  return dispatch => {
    dispatch({ type: GET_KISI_REQ });
    client.then(resp => {
      dispatch({ type: GET_KISI_SUC, kisi: resp });
    });
  };
};

export const saveKisi = kisi => {
  var client = null;
  if (kisi.yeni) {
    client = axiosService.post(`/kisi`, kisi);
  } else {
    client = axiosService.patch(`/kisi/${kisi._id}`, kisi);
  }

  return dispatch => {
    dispatch({ type: SAVE_KISI_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_KISI_SUC, kisi: resp });
      dispatch({ type: SHOW_MESSAGE, message: 'Kişi kaydedildi' });
    });
  };
};

export const yeniKisi = () => {
  return dispatch => {
    dispatch({
      type: YENI_KISI,
      kisi: {
        yeni: true,
        aktif: true,
        adi: '',
        soyadi: '',
        musteri: null,
        attachments: []
      }
    });
  };
};

export const getKisiNotlar = id => {
  const client = axiosService.get(`/kisi/${id}/not`);
  return dispatch => {
    dispatch({ type: GET_KISI_NOTLAR_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_KISI_NOTLAR_ERR });
      } else {
        dispatch({ type: GET_KISI_NOTLAR_SUC, notlar: resp.data });
      }
    });
  };
};

export const addKisiNot = (id, not) => {
  const client = axiosService.post(`/kisi/${id}/not`, not);
  return dispatch => {
    dispatch({ type: ADD_KISI_NOT_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: ADD_KISI_NOT_ERR });
      } else {
        dispatch({ type: ADD_KISI_NOT_SUC, not: resp.data });
      }
    });
  };
};

export const setKisi = kisi => {
  return dispatch => {
    dispatch({ type: SET_KISI, kisi: kisi });
  };
};
