//import api from '../../services/api';

export const SHOW_GOREV_DIALOG = "SHOW GOREV DIALOG";
export const HIDE_GOREV_DIALOG = "HIDE GOREV DIALOG";

export const SHOW_MESSAGE = "SHOW MESSAGE";
export const HIDE_MESSAGE = "HIDE MESSAGE";

export const SET_ERROR = "SET ERROR";
export const RESET_ERROR = "RESET ERROR";

export const DISABLE_KAYDET_BTN = 'DISABLE KAYDET BTN'
export const ENABLE_KAYDET_BTN  = 'ENABLE KAYDET BTN'

export function showGorevDialog() {
  return {
    type: SHOW_GOREV_DIALOG,
  };
}

export function hideGorevdialog() {
  return {
    type: HIDE_GOREV_DIALOG,
  };
}

export function showMessage(message) {
  return {
    type: SHOW_MESSAGE,
    message: message
  };
}

export function hideMessage() {
  return {
    type: HIDE_MESSAGE
  };
}

export function setError(error) {
  return {
    type: SET_ERROR,
    error: error
  };
}

export function resetError() {
  return {
    type: RESET_ERROR,
  };
}

export function disableKaydetBtn() {
  return {
    type: DISABLE_KAYDET_BTN,
  };
}

export function enableKaydetBtn() {
  return {
    type: ENABLE_KAYDET_BTN,
  };
}
