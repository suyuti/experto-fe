import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';

export const GET_GORUSMELER_REQ = 'GET GORUSMELER REQ';
export const GET_GORUSMELER_ERR = 'GET GORUSMELER ERR';
export const GET_GORUSMELER_SUC = 'GET GORUSMELER SUC';

export const GET_GORUSME_REQ = 'GET GORUSME REQ';
export const GET_GORUSME_ERR = 'GET GORUSME ERR';
export const GET_GORUSME_SUC = 'GET GORUSME SUC';

export const SAVE_GORUSME_REQ = 'SAVE GORUSME REQ';
export const SAVE_GORUSME_ERR = 'SAVE GORUSME ERR';
export const SAVE_GORUSME_SUC = 'SAVE GORUSME SUC';

export const SET_GORUSME = 'SET GORUSME';
export const YENI_GORUSME = 'YENI GORUSME';

export const getGorusmeler = filter => {
  const client = axiosService.get(`/gorusme`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_GORUSMELER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_GORUSMELER_ERR });
      } else {
        dispatch({ type: GET_GORUSMELER_SUC, gorusmeler: resp.data });
      }
    });
  };
};

export const getGorusme = id => {
  const client = axiosService.get(`/gorusme/${id}`);
  return dispatch => {
    dispatch({ type: GET_GORUSME_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_GORUSME_ERR });
      } else {
        dispatch({ type: GET_GORUSME_SUC, gorusme: resp.data });
      }
    });
  };
};

export const setGorusme = gorusme => {
  return dispatch => {
    dispatch({ type: SET_GORUSME, gorusme: gorusme });
  };
};

export const saveGorusme = gorusme => {
  var client = null;
  if (gorusme.yeni) {
    client = axiosService.post(`/gorusme`, gorusme);
  } else {
    client = axiosService.patch(`/gorusme/${gorusme._id}`, gorusme);
  }

  return dispatch => {
    dispatch({ type: SAVE_GORUSME_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: SAVE_GORUSME_ERR });
        dispatch({ type: SHOW_MESSAGE, message: 'Görüşme kaydedilemedi' });
      } else {
        dispatch({ type: SAVE_GORUSME_SUC, gorusme: resp });
        dispatch({ type: SHOW_MESSAGE, message: 'Görüşme kaydedildi' });
      }
    });
  };
};

export const yeniGorusme = (opts) => {
  return dispatch => {
    dispatch({
      type: YENI_GORUSME,
      gorusme: {
        yeni: true,
        konu:'',
        icGorusme: false,
        tarih: new Date(),
        ...opts
      }
    });
  };
};
