import graphService from '../../services/graph.service'
import * as ExpertoActions from './experto.actions'
import { HIDE_GOREV_DIALOG } from './app.actions'

export const GRAPH_LOGIN_REQ = "GRAPH LOGIN REQ"
export const GRAPH_LOGIN_ERR = "GRAPH LOGIN ERR"
export const GRAPH_LOGIN_SUC = "GRAPH LOGIN SUC"

export const GRAPH_LOGOUT_REQ = "GRAPH LOGOUT REQ"
export const GRAPH_LOGOUT_ERR = "GRAPH LOGOUT ERR"
export const GRAPH_LOGOUT_SUC = "GRAPH LOGOUT SUC"

export const GET_USER_TASKS_REQ = "GET USER TASKS REQ"
export const GET_USER_TASKS_ERR = "GET USER TASKS ERR"
export const GET_USER_TASKS_SUC = "GET USER TASKS SUC"

export const GET_USER_MAILS_REQ = "GET USER MAILS REQ"
export const GET_USER_MAILS_ERR = "GET USER MAILS ERR"
export const GET_USER_MAILS_SUC = "GET USER MAILS SUC"

export const CREATE_TASK_REQ = "CREATE TASK REQ"
export const CREATE_TASK_ERR = "CREATE TASK ERR"
export const CREATE_TASK_SUC = "CREATE TASK SUC"

export const UPDATE_TASK_REQ = "UPDATE TASK REQ"
export const UPDATE_TASK_ERR = "UPDATE TASK ERR"
export const UPDATE_TASK_SUC = "UPDATE TASK SUC"

export const GET_PERSONELLER_REQ = "GET PERSONELLER REQ"
export const GET_PERSONELLER_ERR = "GET PERSONELLER ERR"
export const GET_PERSONELLER_SUC = "GET PERSONELLER SUC"

export const graphLogin = (redirect) => {
    const client = graphService.login()
    return dispatch => {
        dispatch({type: GRAPH_LOGIN_REQ})
        client.then(resp => {
            dispatch(ExpertoActions.expertoLogin(redirect, resp.uniqueId))
            dispatch(getPersoneller())
            dispatch({
                type: GRAPH_LOGIN_SUC,
                payload: resp
            })
        })
    }
}

export const login = (redirect) => {
    const client = graphService.login()
    return dispatch => {
        dispatch({type: GRAPH_LOGIN_REQ})
        client.then(resp => {
            //debugger
            dispatch(ExpertoActions.expertoLogin(redirect, resp.uniqueId))
            dispatch(getPersoneller())
            dispatch({
                type: GRAPH_LOGIN_SUC,
                payload: resp
            })
        })
    }
}

export const graphSilentLogin = (meId, redirect) => {
    //debugger
    const client = graphService.silentLogin(meId)
    return dispatch => {
        dispatch({type: GRAPH_LOGIN_REQ})
        client.then(resp => {
            //debugger
            dispatch(ExpertoActions.expertoLogin(redirect, resp.uniqueId))
            dispatch(getPersoneller())
            dispatch({
                type: GRAPH_LOGIN_SUC,
                payload: resp
            })
        })
    }
}


export const getUserTasks = (userId) => {
    const client = graphService.getUserTasks(userId)
    return dispatch => {
        dispatch({type: GET_USER_TASKS_REQ})
        client.then(resp => {
            dispatch({
                type: GET_USER_TASKS_SUC,
                payload: resp
            })
        })
    }
}

export const getUserMails = (userId) => {
    const client = graphService.getUserMails(userId)
    return dispatch => {
        dispatch({type: GET_USER_MAILS_REQ})
        client.then(resp => {
            dispatch({
                type: GET_USER_MAILS_SUC,
                payload: resp
            })
        })
    }
}

export const graphLogout = () => {
    const client = graphService.logout()
    return dispatch => {
        dispatch({type: GRAPH_LOGOUT_REQ})
        client.then(resp => {
            dispatch({
                type: GRAPH_LOGOUT_SUC
            })
        })
    }
}

export const createTask = (task) => {
    /*
    Task sadece title ile olusturulabiliyor.
    Detaylari için update yapmak gerekiyor
    */
    const client = graphService.createTask(task)
    return dispatch => {
        dispatch({type: CREATE_TASK_REQ})
        client.then(resp => {
            
            graphService.updateTask(resp.id, resp[`@odata.etag`], {description: 'Hello'}).then(r => {
                dispatch({type: UPDATE_TASK_SUC})
            })

            dispatch({
                type: CREATE_TASK_SUC,
                payload: resp
            })
            dispatch({type: HIDE_GOREV_DIALOG})
        })
    }
}

export const getPersoneller = () => {
    const client = graphService.getUsers()
    return dispatch => {
        dispatch({type: GET_PERSONELLER_REQ})
        client.then(resp => {
            for (var i = 0; i < resp.length; ++i) {
                graphService.getUserPhoto(resp[i]).then(photoResp => {
                    photoResp.user.photo = photoResp.photo})
            }
            dispatch({
                type: GET_PERSONELLER_SUC,
                personeller: resp
            })
        })
    }
}
