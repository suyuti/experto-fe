import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';
import { SET_ERROR } from './app.actions';
import { SET_GORUSME } from './experto.gorusme.actions';

export const EXPERTO_LOGIN_REQ = 'EXPERTO LOGIN REQ';
export const EXPERTO_LOGIN_ERR = 'EXPERTO LOGIN ERR';
export const EXPERTO_LOGIN_SUC = 'EXPERTO LOGIN SUC';

export const CREATE_GOREV_REQ = 'CREATE GOREV REQ';
export const CREATE_GOREV_ERR = 'CREATE GOREV ERR';
export const CREATE_GOREV_SUC = 'CREATE GOREV SUC';

export const GET_GOREVLER_REQ = 'GET GOREVLER REQ';
export const GET_GOREVLER_ERR = 'GET GOREVLER ERR';
export const GET_GOREVLER_SUC = 'GET GOREVLER SUC';

export const GET_GOREV_REQ = 'GET GOREV REQ';
export const GET_GOREV_ERR = 'GET GOREV ERR';
export const GET_GOREV_SUC = 'GET GOREV SUC';

export const GET_MUSTERILER_REQ = 'GET MUSTERILER REQ';
export const GET_MUSTERILER_ERR = 'GET MUSTERILER ERR';
export const GET_MUSTERILER_SUC = 'GET MUSTERILER SUC';

export const SET_GOREV = 'SET GOREV';
export const YENI_GOREV = 'YENI GOREV';
export const YENI_MUSTERI = 'YENI MUSTERI';
export const SET_MUSTERI = 'SET MUSTERI';

export const SAVE_GOREV_REQ = 'SAVE GOREV REQ';
export const SAVE_GOREV_ERR = 'SAVE GOREV ERR';
export const SAVE_GOREV_SUC = 'SAVE GOREV SUC';

export const GET_GOREV_COUNT_REQ = 'GET GOREV COUNT REQ';
export const GET_GOREV_COUNT_ERR = 'GET GOREV COUNT ERR';
export const GET_GOREV_COUNT_SUC = 'GET GOREV COUNT SUC';

export const GET_MUSTERI_REQ = 'GET MUSTERI REQ';
export const GET_MUSTERI_ERR = 'GET MUSTERI ERR';
export const GET_MUSTERI_SUC = 'GET MUSTERI SUC';

export const SAVE_MUSTERI_REQ = 'SAVE MUSTERI REQ';
export const SAVE_MUSTERI_ERR = 'SAVE MUSTERI ERR';
export const SAVE_MUSTERI_SUC = 'SAVE MUSTERI SUC';

export const GET_SEKTORLER_REQ = 'GET SEKTORLER REQ';
export const GET_SEKTORLER_ERR = 'GET SEKTORLER ERR';
export const GET_SEKTORLER_SUC = 'GET SEKTORLER SUC';

export const GET_MUSTERI_KISILER_REQ = 'GET MUSTERI KISI REQ';
export const GET_MUSTERI_KISILER_ERR = 'GET MUSTERI KISI ERR';
export const GET_MUSTERI_KISILER_SUC = 'GET MUSTERI KISI SUC';

export const GET_YORUMLAR_REQ = 'GET YORUMLAR REQ';
export const GET_YORUMLAR_ERR = 'GET YORUMLAR ERR';
export const GET_YORUMLAR_SUC = 'GET YORUMLAR SUC';

export const ADD_YORUM_REQ = 'ADD YORUM REQ';
export const ADD_YORUM_ERR = 'ADD YORUM ERR';
export const ADD_YORUM_SUC = 'ADD YORUM SUC';

export const GET_MUSTERI_URUNLER_REQ = 'GET MUSTERI URUNLER REQ';
export const GET_MUSTERI_URUNLER_ERR = 'GET MUSTERI URUNLER ERR';
export const GET_MUSTERI_URUNLER_SUC = 'GET MUSTERI URUNLER SUC';

export const ADD_MUSTERI_URUN_REQ = 'ADD MUSTERI URUN REQ';
export const ADD_MUSTERI_URUN_ERR = 'ADD MUSTERI URUN ERR';
export const ADD_MUSTERI_URUN_SUC = 'ADD MUSTERI URUN SUC';

export const GET_EXPERTO_PERSONELLER_REQ = 'GET EXPERTO PERSONELLER REQ';
export const GET_EXPERTO_PERSONELLER_ERR = 'GET EXPERTO PERSONELLER ERR';
export const GET_EXPERTO_PERSONELLER_SUC = 'GET EXPERTO PERSONELLER SUC';

export const GET_HISTORY_REQ = 'GET HISTORY REQ';
export const GET_HISTORY_ERR = 'GET HISTORY ERR';
export const GET_HISTORY_SUC = 'GET HISTORY SUC';

export const yeniGorev = me => {
  return dispatch => {
    dispatch({ type: RESET_ERROR });
    dispatch({
      type: YENI_GOREV,
      gorev: {
        baslik: '',
        musteri: null,
        oncelik: 'Orta',
        aciklama: '',
        atananlar: [],
        atanan: null,
        atananMsID: null,
        history: [],
        createdByMsId: me.id,
        kayit: false,
        yeni: true,
        sonuc: 'acik',
        canSubmit: false,
        icGorevMi: false,
        sonTarih: null, //new Date()
        attachments: []
      }
    });
  };
};

export const saveGorev = gorev => {
  var client = null;
  if (gorev.yeni) {
    client = axiosService.post(`/gorev`, gorev);
  } else {
    client = axiosService.patch(`/gorev/${gorev._id}`, gorev);
  }

  return dispatch => {
    dispatch({ type: SAVE_GOREV_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_GOREV_SUC });
      dispatch({ type: SHOW_MESSAGE, message: 'Görev kaydedildi' });
    });
  };
};

export const expertoLogin = (redirect, uid) => {
  const client = axiosService.get(`/auth/signin`, {
    params: { uniqueId: uid }
  });
  return dispatch => {
    dispatch({ type: EXPERTO_LOGIN_REQ });
    client.then(resp => {
      //debugger
      if (resp) {
        window.localStorage.setItem('experto_access_token', resp.expertoToken);
        dispatch({
          type: EXPERTO_LOGIN_SUC,
          payload: resp
        });
        redirect(resp.redirectUrl);
      } else {
        dispatch({
          type: EXPERTO_LOGIN_ERR
        });
      }
    });
  };
};

export const createGorev = gorev => {
  debugger
  let formData = new FormData();
  formData.append('file', gorev.file);
  formData.append('gorev', gorev);
  const client = axiosService.post(`/gorev`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });

  //const client = axiosService.post(`/gorev`, gorev);
  return dispatch => {
    dispatch({ type: CREATE_GOREV_REQ });
    client.then(resp => {
      dispatch({ type: CREATE_GOREV_SUC, gorev: resp });
    });
  };
};

export const getGorevler = filter => {
  const client = axiosService.get(`/gorev`, { params: { filter: filter } });
  return dispatch => {
    dispatch({ type: GET_GOREVLER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_GOREVLER_ERR });
      } else {
        dispatch({ type: GET_GOREVLER_SUC, gorevler: resp.data });
      }
    });
  };
};

export const getGorev = id => {
  const client = axiosService.get(`/gorev/${id}`);
  return dispatch => {
    dispatch({ type: GET_GOREV_REQ });
    client.then(resp => {
      dispatch({ type: GET_GOREV_SUC, gorev: resp });
    });
  };
};

export const getMusteriler = filter => {
  const client = axiosService.get(`/musteri`, { params: { filter: filter } });
  return dispatch => {
    dispatch({ type: GET_MUSTERILER_REQ });
    client.then(resp => {
      dispatch({ type: GET_MUSTERILER_SUC, musteriler: resp });
    });
  };
};

export const getGorevStats = filter => {
  const client = axiosService.get(`/statistics/gorev`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_GOREV_COUNT_REQ });
    client.then(resp => {
      dispatch({ type: GET_GOREV_COUNT_SUC, gorevStats: resp });
    });
  };
};

export const getMusteri = id => {
  const client = axiosService.get(`/musteri/${id}`);
  return dispatch => {
    dispatch({ type: GET_MUSTERI_REQ });
    client.then(resp => {
      dispatch({ type: GET_MUSTERI_SUC, musteri: resp });
    });
  };
};

export const getSektorler = filter => {
  const client = axiosService.get(`/sektor`, { params: { filter: filter } });
  return dispatch => {
    dispatch({ type: GET_SEKTORLER_REQ });
    client.then(resp => {
      dispatch({ type: GET_SEKTORLER_SUC, sektorler: resp });
    });
  };
};

export const getMusteriKisiler = id => {
  const client = axiosService.get(`/musteri/${id}/kisiler`);
  return dispatch => {
    dispatch({ type: GET_MUSTERI_KISILER_REQ });
    client.then(resp => {
      dispatch({ type: GET_MUSTERI_KISILER_SUC, kisiler: resp });
    });
  };
};

export const getYorumlar = refId => {
  const client = axiosService.get(`/yorum/${refId}`);
  return dispatch => {
    dispatch({ type: GET_YORUMLAR_REQ });
    client.then(resp => {
      dispatch({ type: GET_YORUMLAR_SUC, yorumlar: resp });
    });
  };
};

export const addYorum = (refId, yorum) => {
  const client = axiosService.post(`/yorum/${refId}`, yorum);
  return dispatch => {
    dispatch({ type: ADD_YORUM_REQ });
    client.then(resp => {
      dispatch({ type: ADD_YORUM_SUC, yorum: resp });
    });
  };
};

export const getMusteriUrunler = musteriId => {
  const client = axiosService.get(`/musteri/${musteriId}/urun`);
  return dispatch => {
    dispatch({ type: GET_MUSTERI_URUNLER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_MUSTERI_URUNLER_ERR });
      } else {
        dispatch({ type: GET_MUSTERI_URUNLER_SUC, urunler: resp.data });
      }
    });
  };
};

export const addMusteriUrun = (musteriId, urun) => {
  const client = axiosService.post(`/musteri/${musteriId}/urun`);
  return dispatch => {
    dispatch({ type: ADD_MUSTERI_URUN_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: ADD_MUSTERI_URUN_ERR });
      } else {
        dispatch({ type: ADD_MUSTERI_URUN_SUC, urunler: resp.data });
      }
    });
  };
};

export const yeniMusteri = () => {
  return dispatch => {
    dispatch({
      type: YENI_MUSTERI,
      musteri: {
        yeni: true,
        firmaBilgi: {},
        iletisim: {
          il: '',
          ilce: ''
        },
        hangiFirmamizdan: {}
      }
    });
  };
};

export const saveMusteri = musteri => {
  var client = null;
  if (musteri.yeni) {
    client = axiosService.post(`/musteri`, musteri);
  } else {
    client = axiosService.patch(`/musteri/${musteri._id}`, musteri);
  }

  return dispatch => {
    dispatch({ type: SAVE_MUSTERI_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_MUSTERI_SUC });
      dispatch({ type: SHOW_MESSAGE, message: 'Müşteri kaydedildi' });
    });
  };
};

export const setMusteri = musteri => {
  return dispatch => {
    dispatch({ type: SET_MUSTERI, musteri: musteri });
  };
};

export const getExpertoPersoneller = filter => {
  const client = axiosService.get(`/personel`, filter);
  return dispatch => {
    dispatch({ type: GET_EXPERTO_PERSONELLER_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_EXPERTO_PERSONELLER_ERR });
      } else {
        dispatch({ type: GET_EXPERTO_PERSONELLER_SUC, personeller: resp.data });
      }
    });
  };
};

export const setGorev = gorev => {
  return dispatch => {
    dispatch({ type: SET_GOREV, gorev: gorev });
  };
};

export const getHistory = refId => {
  const client = axiosService.get(`/history/${refId}`);
  return dispatch => {
    dispatch({ type: GET_HISTORY_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_HISTORY_ERR });
      } else {
        dispatch({ type: GET_HISTORY_SUC, history: resp.data });
      }
    });
  };
};
