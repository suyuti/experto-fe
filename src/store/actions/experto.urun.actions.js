import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';

export const GET_URUNLER_REQ = 'GET URUNLER REQ';
export const GET_URUNLER_ERR = 'GET URUNLER ERR';
export const GET_URUNLER_SUC = 'GET URUNLER SUC';

export const GET_URUN_REQ = 'GET URUN REQ';
export const GET_URUN_ERR = 'GET URUN ERR';
export const GET_URUN_SUC = 'GET URUN SUC';

export const SAVE_URUN_REQ = 'SAVE URUN REQ';
export const SAVE_URUN_ERR = 'SAVE URUN ERR';
export const SAVE_URUN_SUC = 'SAVE URUN SUC';

export const SET_URUN = 'SET URUN';
export const YENI_URUN = 'YENI URUN';

export const getUrunler = filter => {
  const client = axiosService.get(`/urun`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_URUNLER_REQ });
    client.then(resp => {
      dispatch({ type: GET_URUNLER_SUC, urunler: resp });
    });
  };
};

export const getUrun = id => {
  const client = axiosService.get(`/urun/${id}`);
  return dispatch => {
    dispatch({ type: GET_URUN_REQ });
    client.then(resp => {
      dispatch({ type: GET_URUN_SUC, urun: resp });
    });
  };
};

export const setUrun = urun => {
  return dispatch => {
    dispatch({ type: SET_URUN, urun: urun });
  };
};

export const saveUrun = urun => {
  var client = null;
  if (urun.yeni) {
    client = axiosService.post(`/urun`, urun);
  } else {
    client = axiosService.patch(`/urun/${urun._id}`, urun);
  }

  return dispatch => {
    dispatch({ type: SAVE_URUN_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_URUN_SUC, urun: resp });
      dispatch({ type: SHOW_MESSAGE, message: 'Ürün kaydedildi' });
    });
  };
};

export const yeniUrun = () => {
  return dispatch => {
    dispatch({
      type: YENI_URUN,
      urun: {
        yeni: true
      }
    });
  };
};
