import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR, showMessage } from './app.actions';

export const YENI_TOPLANTI = 'YENI TOPLANTI';
export const SET_TOPLANTI = 'SET TOPLANTI';
export const CLEAR_TOPLANTI = 'CLEAR TOPLANTI';

export const GET_TOPLANTILAR_REQ = 'GET TOPLANTILAR REQ';
export const GET_TOPLANTILAR_ERR = 'GET TOPLANTILAR ERR';
export const GET_TOPLANTILAR_SUC = 'GET TOPLANTILAR SUC';

export const GET_TOPLANTI_REQ = 'GET TOPLANTI REQ';
export const GET_TOPLANTI_ERR = 'GET TOPLANTI ERR';
export const GET_TOPLANTI_SUC = 'GET TOPLANTI SUC';

export const UPDATE_TOPLANTI_REQ = 'UPDATE TOPLANTI REQ';
export const UPDATE_TOPLANTI_ERR = 'UPDATE TOPLANTI ERR';
export const UPDATE_TOPLANTI_SUC = 'UPDATE TOPLANTI SUC';

export const SAVE_TOPLANTI_REQ = 'SAVE TOPLANTI REQ';
export const SAVE_TOPLANTI_ERR = 'SAVE TOPLANTI ERR';
export const SAVE_TOPLANTI_SUC = 'SAVE TOPLANTI SUC';

export const REMOVE_TOPLANTI_REQ = 'REMOVE TOPLANTI REQ';
export const REMOVE_TOPLANTI_ERR = 'REMOVE TOPLANTI ERR';
export const REMOVE_TOPLANTI_SUC = 'REMOVE TOPLANTI SUC';

export const GET_TOPLANTI_KARARLARI_REQ = 'GET TOPLANTI KARARLARI REQ';
export const GET_TOPLANTI_KARARLARI_ERR = 'GET TOPLANTI KARARLARI ERR';
export const GET_TOPLANTI_KARARLARI_SUC = 'GET TOPLANTI KARARLARI SUC';

export const ADD_TOPLANTI_KARAR_REQ = 'ADD TOPLANTI KARAR REQ';
export const ADD_TOPLANTI_KARAR_ERR = 'ADD TOPLANTI KARAR ERR';
export const ADD_TOPLANTI_KARAR_SUC = 'ADD TOPLANTI KARAR SUC';

export const UPDATE_TOPLANTI_KARAR_REQ = 'UPDATE TOPLANTI KARAR REQ';
export const UPDATE_TOPLANTI_KARAR_ERR = 'UPDATE TOPLANTI KARAR ERR';
export const UPDATE_TOPLANTI_KARAR_SUC = 'UPDATE TOPLANTI KARAR SUC';

export const REMOVE_TOPLANTI_KARAR_REQ = 'REMOVE TOPLANTI KARAR REQ';
export const REMOVE_TOPLANTI_KARAR_ERR = 'REMOVE TOPLANTI KARAR ERR';
export const REMOVE_TOPLANTI_KARAR_SUC = 'REMOVE TOPLANTI KARAR SUC';

export const SAVE_TOPLANTI_KARARLARI_REQ = 'SAVE TOPLANTI KARARLARI REQ';
export const SAVE_TOPLANTI_KARARLARI_ERR = 'SAVE TOPLANTI KARARLARI ERR';
export const SAVE_TOPLANTI_KARARLARI_SUC = 'SAVE TOPLANTI KARARLARI SUC';

export const GET_TOPLANTI_KARAR_REQ = 'GET TOPLANTI KARAR REQ';
export const GET_TOPLANTI_KARAR_ERR = 'GET TOPLANTI KARAR ERR';
export const GET_TOPLANTI_KARAR_SUC = 'GET TOPLANTI KARAR SUC';

export const GET_TOPLANTI_PDF_REQ = 'GET TOPLANTI PDF REQ';
export const GET_TOPLANTI_PDF_ERR = 'GET TOPLANTI PDF ERR';
export const GET_TOPLANTI_PDF_SUC = 'GET TOPLANTI PDF SUC';

export const GET_TOPLANTI_STATS_REQ = 'GET TOPLANTI STATS REQ';
export const GET_TOPLANTI_STATS_ERR = 'GET TOPLANTI STATS ERR';
export const GET_TOPLANTI_STATS_SUC = 'GET TOPLANTI STATS SUC';

export const TOPLANTI_TAMAMLAR_REQ = 'TOPLANTI TAMAMLA REQ';
export const TOPLANTI_TAMAMLAR_ERR = 'TOPLANTI TAMAMLA ERR';
export const TOPLANTI_TAMAMLAR_SUC = 'TOPLANTI TAMAMLA SUC';


export const yeniToplanti = me => {
  return dispatch => {
    dispatch({ type: RESET_ERROR });
    dispatch({
      type: YENI_TOPLANTI,
      toplanti: {
        toplantimsid: '',
        baslik: '',
        aciklama: '',
        tarih: null, //new Date(),
        saat: null,
        sure: null,
        yer: '',
        tur: null,
        firma: null,
        expertoKatilimcilar: [],
        firmaKatilimcilar: [],
        createdByMsId: me.id,
        createdAt: new Date(),
        updatedBy: null,
        updatedAt: null,
        sonuc: '',
        kararlar: [],
        yeni: true,
        kayit: false,
        gundem: '',
        attachments: []
      }
    });
  };
};

export const getToplantilar = filter => {
  const client = axiosService.get(`/toplanti`, { params: { filter: filter } });
  return dispatch => {
    dispatch({ type: GET_TOPLANTILAR_REQ });
    client.then(resp => {
      if (resp.error) {
        dispatch({ type: GET_TOPLANTILAR_ERR });
      }
      else {
        dispatch({ type: GET_TOPLANTILAR_SUC, toplantilar: resp.data });
      }
    });
  };
};

export const getToplanti = id => {
  const client = axiosService.get(`/toplanti/${id}`);
  return dispatch => {
    dispatch({ type: GET_TOPLANTI_REQ });
    client.then(resp => {
      if (resp.err) {
      } else {
        dispatch({ type: GET_TOPLANTI_SUC, toplanti: resp.data });
      }
    });
  };
};

export const updateToplanti = toplanti => {
  const client = axiosService.patch(`/toplanti/${toplanti._id}`, toplanti);
  return dispatch => {
    dispatch({ type: UPDATE_TOPLANTI_REQ });
    client.then(resp => {
      dispatch({ type: UPDATE_TOPLANTI_SUC, toplanti: resp });
    });
  };
};

export const saveToplanti = toplanti => {
  var client = null;
  if (toplanti.yeni) {
    client = axiosService.post(`/toplanti`, toplanti);
  } else {
    client = axiosService.patch(`/toplanti/${toplanti._id}`, toplanti);
  }
  return dispatch => {
    dispatch({ type: SAVE_TOPLANTI_REQ });
    client.then(resp => {
      dispatch({ type: SAVE_TOPLANTI_SUC, toplanti: resp });
      dispatch({ type: SHOW_MESSAGE, message: 'Toplantı kaydedildi' });
    });
  };
};

export const removeToplanti = toplanti => {
  const client = axiosService.delete(`/toplanti/${toplanti._id}`);
  return dispatch => {
    dispatch({ type: REMOVE_TOPLANTI_REQ });
    client.then(resp => {
      dispatch({ type: REMOVE_TOPLANTI_SUC });
    });
  };
};

export const addToplantiKarar = (toplanti, karar) => {
  const client = axiosService.post(`/toplanti/${toplanti._id}/karar`, karar);
  return dispatch => {
    dispatch({ type: ADD_TOPLANTI_KARAR_REQ });
    client.then(resp => {
      dispatch({ type: ADD_TOPLANTI_KARAR_SUC, toplanti: resp });
    });
  };
};

export const updateToplantiKarar = (toplanti, karar) => {
  const client = axiosService.patch(
    `/toplanti/${toplanti._id}/karar/${karar._id}`,
    karar
  );
  return dispatch => {
    dispatch({ type: UPDATE_TOPLANTI_KARAR_REQ });
    client.then(resp => {
      dispatch({ type: UPDATE_TOPLANTI_KARAR_SUC, toplanti: resp });
    });
  };
};

export const removeToplantiKarar = (toplanti, karar) => {
  const client = axiosService.delete(
    `/toplanti/${toplanti._id}/karar/${karar._id}`
  );
  return dispatch => {
    dispatch({ type: REMOVE_TOPLANTI_KARAR_REQ });
    client.then(resp => {
      dispatch({ type: REMOVE_TOPLANTI_KARAR_SUC });
    });
  };
};

export const getToplantiKararlari = toplantiId => {
  const client = axiosService.get(`/toplanti/${toplantiId}/karar`);
  return dispatch => {
    dispatch({ type: GET_TOPLANTI_KARARLARI_REQ });
    client.then(resp => {
      if (resp.err) {
      } else {
        dispatch({ type: GET_TOPLANTI_KARARLARI_SUC, kararlar: resp.data });
      }
    });
  };
};

export const getToplantiKarar = (toplanti, kararId) => {
  const client = axiosService.get(`/toplanti/${toplanti._id}/karar/${kararId}`);
  return dispatch => {
    dispatch({ type: GET_TOPLANTI_KARAR_REQ });
    client.then(resp => {
      if (resp.err) {
      } else {
        dispatch({ type: GET_TOPLANTI_KARAR_SUC, karar: resp.data });
      }
    });
  };
};

export const saveToplantiKararlari = (toplanti, kararlar) => {
  const client = axiosService.post(
    `/toplanti/${toplanti._id}/kararlar`,
    kararlar
  );
  return dispatch => {
    dispatch({ type: SAVE_TOPLANTI_KARARLARI_REQ });
    client.then(resp => {
      if (resp.err) {
      } else {
        dispatch({ type: SAVE_TOPLANTI_KARARLARI_SUC, kararlar: resp.data });
        dispatch(showMessage('Toplantı kararları kaydedildi'))
      }
    });
  };
};

export const getToplantiPdf = toplanti => {
  const client = axiosService.get(`/toplanti/${toplanti._id}/pdf`, {
    responseType: 'blob'
  });
  return dispatch => {
    dispatch({ type: GET_TOPLANTI_PDF_REQ });
    client.then(pdfdata => {
      const file = new Blob([pdfdata], { type: 'application/pdf' });
      const fileUrl = URL.createObjectURL(file);
      window.open(fileUrl);
    });
  };
};

export const getToplantiStats = filter => {
  const client = axiosService.get(`/statistics/toplanti`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_TOPLANTI_STATS_REQ });
    client.then(resp => {
      dispatch({ type: GET_TOPLANTI_STATS_SUC, stats: resp });
    });
  };
};


export const setToplanti = toplanti => {
  return dispatch => {
    dispatch({type: SET_TOPLANTI, toplanti: toplanti})
  }
}

export const toplantiTamamla = toplanti => {
  const client = axiosService.post(
    `/toplanti/${toplanti._id}/tamamla`
  );
  return dispatch => {
    dispatch({ type: TOPLANTI_TAMAMLAR_REQ });
    client.then(resp => {
      if (resp.err) {
      } else {
        dispatch({ type: TOPLANTI_TAMAMLAR_SUC, toplanti: resp.data });
        dispatch(showMessage('Toplantı Tamamlandı'))
      }
    });
  };
}