import axiosService from '../../services/axios.service';
import { SHOW_MESSAGE, RESET_ERROR } from './app.actions';

export const GET_DEPARTMANLAR_REQ = 'GET DEPARTMANLAR REQ';
export const GET_DEPARTMANLAR_ERR = 'GET DEPARTMANLAR ERR';
export const GET_DEPARTMANLAR_SUC = 'GET DEPARTMANLAR SUC';

export const GET_DEPARTMAN_REQ = 'GET DEPARTMAN REQ';
export const GET_DEPARTMAN_ERR = 'GET DEPARTMAN ERR';
export const GET_DEPARTMAN_SUC = 'GET DEPARTMAN SUC';

export const SAVE_DEPARTMAN_REQ = 'SAVE DEPARTMAN REQ';
export const SAVE_DEPARTMAN_ERR = 'SAVE DEPARTMAN ERR';
export const SAVE_DEPARTMAN_SUC = 'SAVE DEPARTMAN SUC';

export const REMOVE_DEPARTMAN_REQ = 'REMOVE DEPARTMAN REQ';
export const REMOVE_DEPARTMAN_ERR = 'REMOVE DEPARTMAN ERR';
export const REMOVE_DEPARTMAN_SUC = 'REMOVE DEPARTMAN SUC';

export const YENI_DEPARTMAN = 'YENI DEPARTMAN';
export const SET_DEPARTMAN = 'SET DEPARTMAN';

export const getDepartmanlar = filter => {
  const client = axiosService.get(`/departman?t=tree`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_DEPARTMANLAR_REQ });
    client.then(resp => {
      if (resp.err) {
        dispatch({ type: GET_DEPARTMANLAR_ERR, urunler: null });
      } else {
        dispatch({ type: GET_DEPARTMANLAR_SUC, departmanlar: resp.data });
      }
    });
  };
};

export const getDepartmanlarAsList = filter => {
  const client = axiosService.get(`/departman?t=list`, {
    params: { filter: filter }
  });
  return dispatch => {
    dispatch({ type: GET_DEPARTMANLAR_REQ });
    client.then(resp => {
      if (resp.err) {
        dispatch({ type: GET_DEPARTMANLAR_ERR, urunler: null });
      } else {
        dispatch({ type: GET_DEPARTMANLAR_SUC, departmanlar: resp.data });
      }
    });
  };
};

export const getDepartman = id => {
  const client = axiosService.get(`/departman/${id}`);
  return dispatch => {
    dispatch({ type: GET_DEPARTMAN_REQ });
    client.then(resp => {
      if (resp.err) {
        dispatch({ type: GET_DEPARTMAN_ERR, urunler: null });
      } else {
        dispatch({ type: GET_DEPARTMAN_SUC, departman: resp.data });
      }
    });
  };
};

export const saveDepartman = departman => {
  var client = null;
  if (departman.yeni) {
    client = axiosService.post(`/departman`, departman);
  } else {
    client = axiosService.patch(`/departman/${departman._id}`, departman);
  }

  return dispatch => {
    dispatch({ type: SAVE_DEPARTMAN_REQ });
    client.then(resp => {
      if (resp.err) {
        //dispatch({ type: SAVE_DEPARTMAN_ERR, departman: null });
      } else {
        dispatch({ type: SAVE_DEPARTMAN_SUC, departman: resp.data });
        dispatch({ type: SHOW_MESSAGE, message: 'Departman kaydedildi' });
      }
    });
  };
};

export const removeDepartman = departman => {
  const client = axiosService.delete(`/departman/${departman._id}`);
  return dispatch => {
    dispatch({ type: REMOVE_DEPARTMAN_REQ });
    client.then(resp => {
      dispatch({ type: REMOVE_DEPARTMAN_SUC });
    });
  };
};

export const yeniDepartman = () => {
  return dispatch => {
    dispatch({ type: RESET_ERROR });
    dispatch({
      type: YENI_DEPARTMAN,
      departman: {
        adi: '',
        personeller: [],
        yeni: true
      }
    });
  };
};

export const setDepartman = departman => {
  return dispatch => {
    dispatch({
      type: SET_DEPARTMAN,
      departman: departman
    });
  };
};
