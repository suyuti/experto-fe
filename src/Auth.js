import React, { Component, Fragment, useState, useEffect } from 'react';
import graphService from './services/graph.service';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from './store/actions';
import { withRouter } from 'react-router';

const AuthWrapper = props => {
  const dispatch = useDispatch();
  const msLoginOk = useSelector(state => state.graph.msLoginOk);
  const me = useSelector(state => state.graph.me);

  const redirect = url => {
    props.history.push(url);
  };

  useEffect(() => {
    const load = async () => {
      //debugger;
      dispatch(Actions.login(redirect));





//      let meId = window.localStorage.getItem('me');
//      if (meId) {
//        dispatch(Actions.graphSilentLogin(meId, redirect));
//      } else {
//        props.history.push('/welcome');
//        //dispatch(Actions.graphLogin(redirect));
//      }
    };
    load();
  }, []);

//  useEffect(() => {
//    //debugger
//    if (msLoginOk) {
//      //debugger
//      dispatch(Actions.expertoLogin(redirect, me.id))
//    }
//  }, [msLoginOk])

  return <Fragment>{props.children}</Fragment>;
};

export default withRouter(AuthWrapper);

//    <Fragment>{waitAuthCheck ? <>wait</> : <Fragment>{props.children }</Fragment>}</Fragment>
