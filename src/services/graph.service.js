import { UserAgentApplication } from 'msal';
import { config } from './graph.config';

var graph = require('@microsoft/microsoft-graph-client');
export const userAgentApplication = new UserAgentApplication({
  auth: {
    clientId: config.appId,
    redirectUri: config.redirectUri
  },
  cache: {
    cacheLocation: 'sessionStorage',
    //storeAuthStateInCookie: true
  }
});

var client = null;

const getAuthenticatedClient = accessToken => {
  client = graph.Client.init({
    authProvider: done => {
      done(null, accessToken);
    }
  });
  return client;
};
//var client = getAuthenticatedClient(window.localStorage.getItem('token'));

const silentLogin = meId => {
  try {
    //debugger;
    let promise = new Promise(resolve => {
      //debugger;
      userAgentApplication
        .acquireTokenSilent({ scopes: config.scopes })
        .then(token => {
          //debugger;
          console.log(token);
          window.localStorage.setItem('token', token.accessToken);
          getAuthenticatedClient(token.accessToken);
          getUser(meId).then(user => {
            //debugger;
            getUserPhoto(user).then(photoResp => {
              //debugger;
              resolve({
                uniqueId: meId,
                token: token.accessToken,
                me: user,
                mePhoto: photoResp.photo
              });
            });
          });
        })
        .catch(e => {
          //debugger;
          console.log(e);
        });
    });
    return promise;
  } catch (e) {
    //debugger;
    console.log(e);
  }
};

const __login = () => {
  try {
    let promise = new Promise(resolve => {
      userAgentApplication
        .loginPopup({
          scopes: config.scopes,
          prompt: 'select_account'
        })
        .then(resp => {
          userAgentApplication
            .acquireTokenSilent({ scopes: config.scopes })
            .then(token => {
              //debugger;
              window.localStorage.setItem('token', token.accessToken);
              window.localStorage.setItem('me', resp.uniqueId);
              getAuthenticatedClient(token.accessToken);
              getUser(resp.uniqueId).then(user =>
                getUserPhoto(user).then(photoResp => {
                  resolve({
                    uniqueId: resp.uniqueId,
                    token: token.accessToken,
                    me: user,
                    mePhoto: photoResp.photo
                  });
                })
              );
            });
        })
        .catch(e => {
          //debugger;
          console.log(e);
        });
    });
    return promise;
  } catch (e) {
    console.log(e);
  }
};

const logout = () => {
  let promise = new Promise(resolve => {
    userAgentApplication.logout();
    userAgentApplication.clearCache();
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('me');
    window.localStorage.removeItem('experto_access_token');
    resolve(null);
  });
};

const getMe = () => {
  try {
    let promise = new Promise(resolve => {
      client
        .api(`/me`)
        .get()
        .then(user => resolve(user));
    });
    return promise;
  } catch (e) {
    console.log(e);
  }
};
const getUser = uniqueId => {
  try {
    let promise = new Promise(resolve => {
      client
        .api(`/users/${uniqueId}`)
        .get()
        .then(user => resolve(user));
    });
    return promise;
  } catch (e) {
    console.log(e);
  }
};

const getUserPhoto = async user => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${user.id}/photo/$value`)
      .get()
      .then(photo => {
        resolve({ user: user, photo: window.URL.createObjectURL(photo) });
      })
      .catch(e => resolve({ user: user, photo: null }));
  });
};

const getUserTasks = async userId => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${userId}/planner/tasks`)
      .get()
      .then(resp => resolve(resp.value))
      .catch(e => resolve(null));
  });
};

const getUserMails = async userId => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${userId}/messages`)
      .get()
      .then(resp => resolve(resp.value))
      .catch(e => resolve(null));
  });
};

const createTask = async task => {
  const _task = {
    title: task.title,
    planId: task.planId
  };
  return new Promise((resolve, reject) => {
    client
      .api(`/planner/tasks`)
      .post(_task)
      .then(resp => {
        resolve(resp);
      })
      .catch(e => {
        resolve(null);
      });
  });
};

const updateTask = async (taskId, etag, details) => {
  /*const _task = {
        dueDateTime: task.dueDateTime,
        startDateTime: task.startDateTime
    }*/
  return new Promise((resolve, reject) => {
    client
      .api(`/planner/tasks/${taskId}`)
      .header('If-Match', etag)
      .header('Prefer', 'return=representation')
      .patch(details)
      .then(resp => {
        resolve(resp.value);
      })
      .catch(e => {
        resolve(null);
      });
  });
};

const createPlan = async plan => {
  return new Promise((resolve, reject) => {
    client
      .api(`/planner/plans`)
      .post(plan)
      .then(resp => {
        resolve(resp.value);
      })
      .catch(e => {
        resolve(null);
      });
  });
};

const getGroupPlans = async group => {
  return new Promise((resolve, reject) => {
    client
      .api(`/groups/${group.id}/planner/plans`)
      .get()
      .then(resp => {
        resolve(resp.value);
      })
      .catch(e => {
        resolve(null);
      });
  });
};

const getUsers = async => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users`)
      .get()
      .then(resp => {
        resolve(resp.value);
      })
      .catch(e => {
        resolve(null);
      });
  });
};
/*
const getUserPhoto = async user => {
  return new Promise((resolve, reject) => {
    client
      .api(`/users/${user.id}/photo/$value`)
      .get()
      .then(photo => {
        user.photo = window.URL.createObjectURL(photo);
        resolve(user);
      })
      .catch(e => {
        resolve(null);
      });
  });
};
*/

const login = async () => {
  try {
    //debugger
    let promise = new Promise(resolve => {
      userAgentApplication
        .acquireTokenSilent({ scopes: config.scopes })
        .then(accessTokenResponse => {
          //debugger
          getAuthenticatedClient(accessTokenResponse.accessToken);
          getMe().then(me => {
            //debugger
            getUserPhoto(me).then(photo => {
              //debugger
              resolve({
                uniqueId: me.id,
                token: accessTokenResponse.accessToken,
                me: me,
                mePhoto: photo.photo
              });
            });
          });
        })
        .catch(error => {
          userAgentApplication
            .loginPopup({ scopes: config.scopes })
            .then(accessTokenResponse => {
              //debugger
              getAuthenticatedClient(accessTokenResponse.accessToken);
              getMe().then(me => {
                //debugger
                getUserPhoto(me.id).then(photo => {
                  //debugger
                  resolve({
                    uniqueId: me,
                    token: accessTokenResponse.accessToken,
                    me: me,
                    mePhoto: photo.photo
                  });
                });
              });
            });
        });
    });
    return promise;
  } catch (e) {
    console.log(e);
  }
};

const graphService = {
  silentLogin,
  login,
  logout,
  getUserTasks,
  getUserMails,
  createTask,
  updateTask,
  createPlan,
  getGroupPlans,
  getUsers,
  getUserPhoto
};

export default graphService;
