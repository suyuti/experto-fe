import axiosService from './axios.service';

const upload = (file, onUploadProgress) => {
  let formData = new FormData();

  formData.append('file', file);

  return axiosService.post('/file/upload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    onUploadProgress
  });
};


const download = (fileId) => {
  return axiosService.get(`/file/${fileId}`, {
    responseType: "blob"
  });
};


export default {
  upload,
  download
};
